import { listAllJs } from './listAllJS'; 
import { readLinePromise } from './Exercise1'

const readCommand = async ()=>{
    while(true){ 
        // Exit by Ctrl+C
        const answer = await readLinePromise ("Please choose read the report(1) or run the benchmark(2):");
        const option = parseInt(answer,10);
        console.log(`Option ${answer} chosen.`);
        if(option == 1){
            await readTheReport();  
        }else if(option == 2){
            await runTheBenchmark(); 
        }else{
            console.log("Please input 1 or 2 only.");
        }
    }
}

readCommand();

let elapsed1: null | number = null;
let elapsed100: null | number = null;
let elapsed1000: null | number = null;

async function runTheBenchmark(){
    {
    console.log(`benchmark for 1 time`)
    const start = new Date()
    for (let i = 0; i <= 1; i++){
    await listAllJs (`./`)
    }
   elapsed1 = (new Date()).getTime()- start.getTime()
    }
    
    {
        console.log(`benchmark for 1 time`)
        const start = new Date()
        for (let i = 0; i <= 100; i++){
        await listAllJs (`./`)
        }
       elapsed100 = (new Date()).getTime()- start.getTime()
        }

        {
            console.log(`benchmark for 1 time`)
            const start = new Date()
            for (let i = 0; i <= 1000; i++){
            await listAllJs (`./`)
            }
           elapsed1000 = (new Date()).getTime()- start.getTime()
            }
}

async function readTheReport(){
    if (elapsed1 == null || elapsed100 == null || elapsed1000 == null){
        console.log("not yet benchmarked")
        return;
    } 
    console.table(
    [
        { times: '1次', seconds: `${elapsed1/1000}秒`},
        { times: '100次', seconds: `${elapsed100/1000}秒`},
        { times: '1000次', seconds: `${elapsed1000/1000}秒`},
    ]
    )}