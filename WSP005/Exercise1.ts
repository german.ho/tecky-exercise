import * as fs from 'fs'
import * as readline from 'readline';
import * as events from 'events'

function fsReadFilePromise(filePath:string):Promise<Buffer>{
    return new Promise(function(resolve,reject){
        fs.readFile (filePath, (err,data) =>{
            if (err){
                return reject (err)
            }
             resolve (data)
            })
        })
    }

async function test(){
    const data = await fsReadFilePromise ('package.json')
    console.log(data)
}

test()



function fsWriteFilePromise(filePath:string,data:any,options:fs.WriteFileOptions):Promise<{}>{
    return new Promise(function(resolve,reject){
        fs.writeFile(filePath, data, options, (err) =>{
            if (err){
                console.log(err)
                return reject (err)
            } 
            resolve ()
        })
    });
}


function createTimeoutPromise (millisecond: number){
    return new Promise<void> (function (resolve, reject){
        setTimeout(function(){
            resolve()
        },millisecond);
    })
}

async function testFunction(){
    console.log("1 second")
    await createTimeoutPromise (1000)

    console.log("2 second")
    await createTimeoutPromise (2000)

    console.log("answer is here")
    await createTimeoutPromise (3000)
}

testFunction()


const readLineInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

export function readLinePromise (answer: string){  
    return new Promise <string> ((resolve, reject) =>{
        readLineInterface.question("What is your name?",(answer:string)=>{
            resolve (answer);
            readLineInterface.close();
    })
}
)};



const emitter = new events.EventEmitter();

function createEventPromise (){
    return new Promise ((resolve, reject) =>{
    emitter.on('event',(data)=>{
     resolve (data)
     }
    )}
    )}


     