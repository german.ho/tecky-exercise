import * as fs from 'fs'
import * as path from 'path'

function fsStatPromise(fullPath: string) {
  return new Promise<{
    stat: fs.Stats,
    filename: string
  }>((resolve, reject) => {
    fs.stat(fullPath, (err, stat) => {
      if (err) {
        return reject(err);
      }

      resolve({
        stat: stat,
        filename: fullPath
      });
    })
  })
}

export function listAllJs(searchPath: string): Promise<string[]> {
  let result: string[] = []

  return fs.promises.readdir(searchPath).then(files => {
    const Stat之Promise合集 = [];
    for (const file of files) {
      const filePath = path.join(searchPath, file);

      Stat之Promise合集.push(fsStatPromise(filePath));
    }

    return Promise.all(Stat之Promise合集)
  }).then(stats => {
    const folder之folder之Promise合集 = [];
    for (const statResult of stats) {
      const ext = path.extname(statResult.filename);

      if (statResult.stat.isFile() && ext === '.js') {
        result.push(statResult.filename);
      }
      if (statResult.stat.isDirectory()) {
        folder之folder之Promise合集.push(
          listAllJs(statResult.filename)
        )
      }
    }

    return Promise.all(folder之folder之Promise合集);
  }).then(一堆folderPromise嘅答案s => {
    for (const folderPromise嘅答案s of 一堆folderPromise嘅答案s) {
      result = result.concat(folderPromise嘅答案s);
    }

    return result;
  })
}
