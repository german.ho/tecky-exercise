export const checkIsBodyValid = (data: object, rules: object) => {
	for (let key in rules) {
		if (typeof data[key] !== rules[key]) {
			return false;
		}
	}
	return true;
}