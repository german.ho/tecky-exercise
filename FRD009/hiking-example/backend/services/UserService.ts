import Knex from "knex";

export interface User {
  id: number;
  username: string;
  password: string;
}

export class UserService {
  constructor(private knex: Knex) {

  }

  public getUser = async (id: number): Promise<User> => {
    const result = await this.knex.raw(/* SQL */`SELECT * FROM users WHERE id = ?`, [id]);
    return result.rows[0]
  }

  public getUserByUsername = async (username: string): Promise<User> => {
    const result = await this.knex.raw(/* SQL */`SELECT * FROM users WHERE username = ?`, [username]);
    return result.rows[0]
  }

  public createUser = async (username: string): Promise<User> => {
    return await this.knex.insert({
      username,
      // 睇片抄 code 的您，請在呢度加埋 facebook id
    }).into("users").returning(['id', 'username']);
  }
}