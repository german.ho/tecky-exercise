import { UserService, User } from './services/UserService'
import Knex from 'knex'
import { AuthRouter } from './routers/AuthRouter';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { UserRouter } from './routers/UserRouter';
import { isLoggedIn } from './guard';

declare global {
  namespace Express {
    interface Request {
      user?: User
    }
  }
}

const knexConfig = require('./knexfile')
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development'])

const userService = new UserService(knex);
const authRouter = new AuthRouter(userService);
const userRouter = new UserRouter();

const isLoggedInGuard = isLoggedIn(userService)

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cors())

app.use('/auth', authRouter.router())
app.use('/user', isLoggedInGuard, userRouter.router())

const port = process.env.PORT || 8000;
app.listen(port, () => {
  console.log(`Listening on ${port}`)
})