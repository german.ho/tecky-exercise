import * as Knex from "knex";
import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<any> {
    await knex("users").del()

    await knex("users").insert([
        {username: "alex", password: await hashPassword('123456')},
        {username: "gordon", password: await hashPassword('123456')},
    ]).into("users")
};
