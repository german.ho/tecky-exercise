import { UserService } from "../services/UserService";
import express from "express"
import { checkPassword } from "../hash";
import jwt from "../jwt";
import jwtSimple from "jwt-simple"
import fetch from 'node-fetch'

export class AuthRouter {
  constructor(private userService: UserService) {

  }

  router() {
    const router = express.Router();
    router.post('/login', this.post);
    router.post('/loginFacebook', this.loginFacebook)
    return router;
  }

  private post = async (req: express.Request, res: express.Response) => {
    try {
      if (!req.body.username || !req.body.password) {
        res.status(401).json({ msg: "Wrong Username/Password" });
        return;
      }
      const { username, password } = req.body;
      const user = (await this.userService.getUserByUsername(username));
      if (!user || !(await checkPassword(password, user.password))) {
        res.status(401).json({ msg: "Wrong Username/Password" });
        return;
      }
      const payload = {
        id: user.id,
        username: user.username,
      };
      const token = jwtSimple.encode(payload, jwt.jwtSecret);
      res.json({
        token: token
      });
    } catch (e) {
      console.log(e)
      res.status(500).json({ msg: e.toString() })
    }
  }

  private loginFacebook = async (req: express.Request, res: express.Response) => {
    try {
      if (!req.body.accessToken) {
        res.status(401).json({ msg: "Wrong Access Token!" });
        return;
      }
      const { accessToken } = req.body;
      const fetchResponse = await fetch(`https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`);
      const result = await fetchResponse.json();
      if (result.error) {
        res.status(401).json({ msg: "Wrong Access Token!" });
        return;
      }
      let user = (await this.userService.getUserByUsername(result.email));

      // Create a new user if the user does not exist
      if (!user) {
        user = (await this.userService.createUser(result.email))[0];
      }
      const payload = {
        id: user.id,
        username: user.username
      };
      const token = jwtSimple.encode(payload, jwt.jwtSecret);
      res.json({
        token: token
      });
    } catch (e) {
      res.status(500).json({ msg: e.toString() })
    }
  }

}