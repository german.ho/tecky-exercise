import express from "express"

export class UserRouter {
  router() {
    const router = express.Router();
    router.get('/current', this.getCurrentUser);
    return router;
  }

  private getCurrentUser = (req: express.Request, res: express.Response) => {
    if (req.user == null) {
      return res.status(401).json("Unauthorized");
    }
    const { password, ...user } = req.user;
    return res.json(user);
  }
}