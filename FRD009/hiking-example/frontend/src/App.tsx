import React, { useEffect, useState } from 'react';
import './App.css';
import { LoginForm } from './LoginForm';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from './store';
import { HomePage } from './HomePage';
import { restoreLogin } from './redux/auth/thunk';
import { Switch, Route } from 'react-router';
import { PrivateRoute } from './PrivateRoute';
import { Link } from 'react-router-dom';
import GoogleMapReact, { ChildComponentProps } from 'google-map-react'

function Marker(props: ChildComponentProps & {id: number}) {
  return <div>Hi {props.id}</div>
}

function App() {
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated)
  const dispatch = useDispatch();
  const [center, setCenter] = useState({lat: 22, lng: 114});
  const [zoom, setZoom] = useState(13);

  const shops = useSelector((state: RootState) => state.shops);

  const [map, setMap] = useState<any>(null);
  const [maps, setMaps] = useState<any>(null);

  useEffect(() => {
    if (map == null || maps == null) {
      return;
    }

    var directionsService = new maps.DirectionsService();
    var directionsRenderer = new maps.DirectionsRenderer();
    directionsRenderer.setMap(map);
    directionsService.route(
    {
      origin: new maps.LatLng(shops[0].lat, shops[0].lng),
      destination: new maps.LatLng(shops[1].lat, shops[1].lng),
      travelMode: 'DRIVING'
    },
    function(response: any, status: any) {
      if (status === 'OK') {
        directionsRenderer.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });

    var flightPath = new maps.Polyline({
      path: shops,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });

    flightPath.setMap(map);

    return () => {
      // update/unmount

      flightPath.setMap(null);
      directionsRenderer.setMap(null);
    }
  }, [map, maps, shops]);

  useEffect(() => {
    dispatch(restoreLogin());
  }, [dispatch])

  return (
    <div className="App">
      <div>
        <Link to="/login">Login</Link>
        <Link to="/aboutus">About Us</Link>
      </div>
      <Switch>
        <Route path="/aboutus">關於我們。。完</Route>
        <Route path="/login"><LoginForm /></Route>
        <PrivateRoute path="/" exact component={HomePage}></PrivateRoute>
      </Switch>
      <div>
      { shops.map((shop, i) => <button onClick={() => {
        setCenter({lat: shop.lat, lng: shop.lng});
        setZoom(17);
      }} key={i} className={Math.abs(center.lat - shop.lat) < 0.001  && Math.abs(center.lng - shop.lng) < 0.001 ? "active" : ""}>Shop {i}</button> )}
      <button onClick={() => dispatch({
        type: "@@SHOP/CHANGE_SHOP"
      })}>大調換</button>
      </div>
      <div className="map">
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyCcU1avxrJsTweqU4BMcQYaoVum34Sas9U" }}
          center={center}
          zoom={zoom}
          distanceToMouse={()=>{return 0}}
          onChange={({center, zoom}) => {
            setCenter(center);
            setZoom(zoom)
          }}
          yesIWantToUseGoogleMapApiInternals
          onGoogleApiLoaded={({ map, maps }) => {
            setMap(map);
            setMaps(maps);
          }}
        >
          { shops.map((shop, i) => <Marker lat={shop.lat} lng={shop.lng} id={i} key={i} /> )}
          
        </GoogleMapReact>
      </div>
    </div>
  );
}

export default App;
