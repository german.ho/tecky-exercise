import { AuthActions } from "./action";

export interface AuthState {
  token: string | null;
  user: User | null;
  isAuthenticated: boolean | null;
  errorMessage: string | null;
}

export interface User {
  id: number;
  username: string;
}

const initialState = {
  token: localStorage.getItem('token'),
  user: null,
  isAuthenticated: null,
  errorMessage: null,
}

export const authReducer = (state: AuthState = initialState, action: AuthActions): AuthState => {
  switch (action.type) {
    case "@@AUTH/LOGIN_SUCCESS":
      return {
        ...state,
        token: action.token,
        isAuthenticated: true,
        errorMessage: null,
      }
    case "@@AUTH/LOGIN_FAILED":
      return {
        ...state,
        token: null,
        user: null,
        isAuthenticated: false,
        errorMessage: action.message
      }
    case "@@AUTH/LOGIN_RESET":
      return {
        ...state,
        errorMessage: null
      }
    case "@@AUTH/GET_USER": 
      return {
        ...state,
        user: action.user
      }
    case "@@AUTH/LOGOUT":
      return {
        ...state,
        token: null,
        user: null,
        isAuthenticated: false,
        errorMessage: null
      }
  }

  return state;
}