import { ThunkDispatch, RootState } from "../../store";
import { loginSuccess, logout, getUser, loginFailed } from "./action";
import { push, replace } from "connected-react-router";

export function login(username: string, password: string) {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/auth/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username, password
      })
    });

    const json = await res.json();

    if (json.token != null) {
      localStorage.setItem('token', json.token);
      dispatch(loginSuccess(json.token))
      dispatch(restoreLogin())
      dispatch(push('/'))
    } else {
      dispatch(loginFailed('錯密碼啊'))
    }
  }
}

export function loginFacebook(accessToken: string) {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/auth/loginFacebook`, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify({ accessToken })
    })
    const result = await res.json();

    if (res.status !== 200) {
      dispatch(loginFailed('FB 同你八字唔夾'))
    } else {
      localStorage.setItem('token', result.token);
      dispatch(loginSuccess(result.token))
      dispatch(restoreLogin())
      dispatch(push("/"));
    }
  }
}


export function restoreLogin() {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    const token = getState().auth.token;

    if (token == null) {
      dispatch(logout()) // isAuthenticated => false
      return;
    }

    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/user/current`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    });
    const json = await res.json();

    if (res.status != 200) {
      dispatch(loginFailed('憑證過時，好耐冇見喇老友'))
    } else {
      dispatch(loginSuccess(token))
      dispatch(getUser(json))
    }
  }
}