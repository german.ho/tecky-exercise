export type ShopsState = {lat: number, lng: number}[]

const initialState = [
  {lat: 22.3724789, lng: 114.105524},
  {lat: 22.3726274, lng: 114.1246268},
  {lat: 22.3617532, lng: 114.1276845},
];

export function shopsReducer (state: ShopsState = initialState, action: any): ShopsState {
  if (action.type == "@@SHOP/CHANGE_SHOP") {
    return [
      {lat: 22.3724789, lng: 114.105524},
      {lat: 22.3617532, lng: 114.1276845},
      {lat: 22.3726274, lng: 114.1246268},
    ];
  }
  return state;
}