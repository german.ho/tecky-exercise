import React, { useEffect } from 'react';
import { useFormState } from 'react-use-form-state'
import { useDispatch, useSelector } from 'react-redux';
import { login, loginFacebook } from './redux/auth/thunk';
import ReactFacebookLogin, { ReactFacebookLoginInfo } from 'react-facebook-login';
import { RootState } from './store';
import { resetMessage } from './redux/auth/action';

export function LoginForm() {
  const [formState, { text, password }] = useFormState();
  const message = useSelector((state: RootState) => state.auth.errorMessage)
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(resetMessage())
  }, [])

  const fBOnCLick = () => {
    return null;
  }

  const fBCallback = (userInfo: ReactFacebookLoginInfo & { accessToken: string }) => {
    if (userInfo.accessToken) {
      dispatch(loginFacebook(userInfo.accessToken));
    }
    return null;
  }

  return (
    <div>
      <form onSubmit={event => {
        event.preventDefault();
        dispatch(login(formState.values.username, formState.values.password));
      }}>
        <label>Username: <input {...text('username')} /></label>
        <label>Password: <input {...password('password')} /></label>
        <input type="submit" />
        {message}
      </form>
      <ReactFacebookLogin
        appId={process.env.REACT_APP_FACEBOOK_APP_ID || ''}
        autoLoad={false}
        fields="name,email,picture"
        onClick={fBOnCLick}
        callback={fBCallback}
      />
    </div>
  )
}