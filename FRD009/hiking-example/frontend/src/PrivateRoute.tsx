import { RouteProps, Redirect, Route } from "react-router";
import { useSelector } from "react-redux";
import React from "react";
import { RootState } from "./store";

export function PrivateRoute({ component, ...rest }: RouteProps) {
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
  const Component = component;
  if (Component == null) {
    return null;
  }
  let render: (props: any) => JSX.Element
  if (isAuthenticated) {
    render = (props: any) => (
      <Component {...props} />
    )
  } else if (isAuthenticated === false) {
    render = (props: any) => (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }} />
    )
  } else {
    render = (props: any) => (
      <div>Loading</div>
    )
  }
  return <Route {...rest} render={render} />
};
