import React, { useState, useCallback, useEffect } from 'react'
import { useSelector } from 'react-redux';
import { RootState } from './store';

export function HomePage(props: {
  color: string
}) {
  const name = useSelector((state: RootState) => state.auth.user?.username)

  return (
    <div>你好 {name}</div>
  )
}