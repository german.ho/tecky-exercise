import express from 'express'
import bodyParser from 'body-parser'
import path from 'path'
import multer from 'multer'
import jsonfile from 'jsonfile'

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, 'uploads'));
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
})

const upload = multer({storage: storage})

const app = express()

// 增強 express 的功能
// Middleware (未講)
app.use(/* 放加強器 */ express.static(path.join(__dirname, 'public')) )
app.use(bodyParser.urlencoded({ extended: false }))

// a=123&b=456
// req.body = {a: '123', b: '456'}

app.get('/memos', async (req, res) => {
  // 嘔 json file 俾 res
  try { // 搵 runtime error
    const memos = await jsonfile.readFile('./memos.json')
  
    res.json(memos);
  } catch (e) {
    console.error(e);
    res.status(500).json({result: false});
  }
})

app.post('/memos', upload.single('photo'), async (req, res) => {
  // 寫入 json file 
  try {
    try {
      const memos = await jsonfile.readFile('./memos.json')
    
      memos.push({
        content: req.body.content,
        image: req.file == null ? null : req.file.filename
      })
    
      await jsonfile.writeFile('./memos.json', memos);
    } catch {
      await jsonfile.writeFile('./memos.json', [{
        content: req.body.content,
        image: req.file == null ? null : req.file.filename
      }]);
    }
  
    res.redirect('/')
  } catch (e) {
    console.error(e);
    res.status(500).json({success: false});
  }
})

app.listen(8080, () => {
  console.log('Listening on port 8080')
})