import express from 'express'
import bodyParser from 'body-parser'
import path from 'path'
import multer from 'multer';

const app = express()

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `${__dirname}/uploads`);
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
})

const upload = multer({storage: storage})

app.use(express.static(path.join(__dirname, 'public')))
app.use(bodyParser.urlencoded())

app.get('/testing', (req, res) => {
  res.end('test' + req.query.password);
});

app.get('/testing/:product/', (req, res) => {
  res.header('Content-Type', 'text/plain; charset=utf8');
  res.json([
    '牛奶',
    req.params.product
  ]);
});

app.get('/old-url/:product/', (req, res) => {
  res.redirect('/testing/' + req.params.product)
});

//  vvvv method
app.post('/testing', upload.single('photo'), (req, res) => {
//        ^^^^^^^^ path (excluding query)
  // 呢個位入連儂紙

  console.log(req.file)
  res.end('Got your password: ' + req.body.password + ' ' + req.query.password)
})

app.listen(8080)