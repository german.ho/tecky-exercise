SELECT "event_venue"."venue_id", "ref_venue"."name", "event_venue"."event_id", "event_user"."user_id"
FROM "event_venue"
LEFT JOIN "event_user"
ON "event_venue"."id" = "event_user"."event_venue_id"
INNER JOIN "ref_venue"
ON "event_venue"."venue_id" = "ref_venue"."id"
WHERE "event_venue"."event_id" = 1;

SELECT "event_venue"."venue_id", "event_venue"."event_id", "event_user"."user_id"
FROM "event_venue"
LEFT JOIN "event_user"
ON "event_venue"."id" = "event_user"."event_venue_id"
WHERE "event_venue"."event_id" = 2;