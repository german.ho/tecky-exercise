export default Object.freeze({
    "USER": "user",
    "EVENT": "event",
    "REF_VENUE": "ref_venue",
    "EVENT_USER": "event_user",
    "EVENT_VENUE": "event_venue",
    "EVENT_DATE": "event_date",
});