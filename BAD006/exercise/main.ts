
import Knex from "knex";
import moment from "moment";
import tables from "./tables";

async function main() {

    const knexConfig = require('./knexfile');
    const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

    // const result = await knex.raw(/*SQL*/)
    try {
        const results = [];
        const events: {id: number, name: string, username: string}[] =
            (await knex.raw(/*SQL*/`
                SELECT "user"."username", "event"."id", "event"."name"
                FROM "event" INNER JOIN "user"
                ON "event"."creator_id" = "user"."id"
            `)).rows;

        for (const event of events) {
            const result: { [key: string]: any } = { name: event.name, created: event.username };
            // console.log(`[info] event: ${event.name}`);

            const attendees: {username: string}[] = (await knex.raw(/*SQL*/`
                SELECT "${tables.USER}"."username" FROM "${tables.USER}"
                INNER JOIN "${tables.EVENT_USER}" 
                ON "${tables.USER}"."id" = "${tables.EVENT_USER}"."user_id"
                WHERE "${tables.EVENT_USER}"."event_id" = ?
            `, [ event.id ])).rows;
            result.attendees = attendees.map(attendee => attendee.username);

            const dates: { event_date: Date, count: number }[] = (await knex.raw(/*SQL*/`
                SELECT ${tables.EVENT_DATE}."event_date", COUNT(${tables.EVENT_USER}."id")::int
                FROM ${tables.EVENT_DATE} LEFT JOIN ${tables.EVENT_USER}
                ON ${tables.EVENT_DATE}."id" = ${tables.EVENT_USER}."event_date_id"
                WHERE ${tables.EVENT_DATE}."event_id" = ? 
                GROUP BY ${tables.EVENT_DATE}."id"
            `, [ event.id ])).rows;
            result.dates = dates.map(date => ({ date: moment(date.event_date).format("YYYY-MM-DD"), votes: date.count }));

            const venues: {name: string, count: number}[] = (await knex.raw(/*SQL*/`
                SELECT ${tables.EVENT_VENUE}."venue_id", ${tables.REF_VENUE}."name", COUNT(${tables.EVENT_USER}."id")::int
                FROM ${tables.EVENT_VENUE} 
                LEFT JOIN ${tables.EVENT_USER}
                ON ${tables.EVENT_VENUE}."id" = ${tables.EVENT_USER}."event_venue_id"
                INNER JOIN ${tables.REF_VENUE}
                ON ${tables.EVENT_VENUE}."venue_id" = ${tables.REF_VENUE}."id"
                WHERE ${tables.EVENT_VENUE}."event_id" = ? 
                GROUP BY ${tables.EVENT_VENUE}."id", ${tables.REF_VENUE}."id"
            `, [ event.id ])).rows;
            result.venues = venues.map(venue => ({ name: venue.name, votes: venue.count }));

            results.push(result);
        }

        // console.log(results);
        console.log(JSON.stringify(results));

    } catch (err) {
        console.log(err.message);

    } finally {
        await knex.destroy();
    }
}

main();
