import * as Knex from "knex";
import tables from "../tables";


export async function seed(knex: Knex): Promise<any> {

    await knex(tables.EVENT_USER).del();
    await knex(tables.EVENT_DATE).del();
    await knex(tables.EVENT_VENUE).del();
    await knex(tables.EVENT).del();
    await knex(tables.USER).del();
    await knex(tables.REF_VENUE).del();

    const venues: number[] = await knex(tables.REF_VENUE).insert([
        { name: "Tecky Academy" }, 
        { name: "Party Room" },
        { name: "McDonald's" }
    ]).returning("id");

    const [kevinID, jeffID, rongID, gordonID, michaelID, alexID, weyfengID] = await knex(tables.USER).insert([
        { username: "kevin", password: "kevin" },
        { username: "jeff", password: "jeff" },
        { username: "rong", password: "rong" },
        { username: "gordon", password: "gordon" },
        { username: "michael", password: "michael" },
        { username: "alex", password: "alex" },
        { username: "weyfeng", password: "weyfeng" }
    ]).returning("id");

    const [ christmasID, countdownID ] = await knex(tables.EVENT).insert([
        { name: "Christmas Party 2018", creator_id: alexID },
        { name: "2019 Countdown Party", creator_id: kevinID }
    ]).returning("id");

    const event_venues =  await knex(tables.EVENT_VENUE).insert([
        { event_id: christmasID, venue_id: venues[0] },
        { event_id: christmasID, venue_id: venues[1] },
        { event_id: christmasID, venue_id: venues[2] },

        { event_id: countdownID, venue_id: venues[0] },
        { event_id: countdownID, venue_id: venues[1] },
    ]).returning("id");

    const event_dates = await knex(tables.EVENT_DATE).insert([
        { event_id: christmasID, event_date: "2018-12-15" }, // 0
        { event_id: christmasID, event_date: "2018-12-20" },
        { event_id: christmasID, event_date: "2018-12-21" },
        { event_id: christmasID, event_date: "2018-12-24" },

        { event_id: countdownID, event_date: "2018-12-31" }, // 4
        { event_id: countdownID, event_date: "2019-1-1" },
        { event_id: countdownID, event_date: "2019-1-2" },
    ]).returning("id");

    await knex(tables.EVENT_USER).insert([
        { user_id: jeffID, event_id: christmasID, event_date_id: event_dates[3], event_venue_id: event_venues[0] },
        { user_id: rongID, event_id: christmasID, event_date_id: event_dates[3], event_venue_id: event_venues[0] },
        { user_id: gordonID, event_id: christmasID, event_date_id: event_dates[3], event_venue_id: event_venues[1] },
        { user_id: michaelID, event_id: christmasID, event_date_id: event_dates[1], event_venue_id: event_venues[2] },

        { user_id: gordonID, event_id: countdownID, event_date_id: event_dates[4], event_venue_id: event_venues[4] },
        { user_id: michaelID, event_id: countdownID, event_date_id: event_dates[4], event_venue_id: event_venues[4] },
        { user_id: weyfengID, event_id: countdownID, event_date_id: event_dates[5], event_venue_id: event_venues[3] },
    ]);
};