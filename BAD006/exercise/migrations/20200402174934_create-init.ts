import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {

    await knex.schema.createTable("ref_venue", table => {
        table.increments();
        table.string("name").notNullable();
        table.timestamps(false, true);
    });

    await knex.schema.createTable("user", table => {
        table.increments();
        table.string("username").notNullable();
        table.string("password").notNullable();
        table.timestamps(false, true);
    });

    await knex.schema.createTable("event", table => {
        table.increments();
        table.integer("creator_id").unsigned();
        table.foreign("creator_id").references("user.id");
        table.string("name").notNullable();
        table.timestamps(false, true);
    });

    await knex.schema.createTable("event_venue", table => {
        table.increments();
        table.integer("event_id").unsigned();
        table.foreign("event_id").references("event.id");
        table.integer("venue_id").unsigned();
        table.foreign("venue_id").references("ref_venue.id");
        table.timestamps(false, true);
    });

    await knex.schema.createTable("event_date", table => {
        table.increments();
        table.integer("event_id").unsigned();
        table.foreign("event_id").references("event.id");
        table.date("event_date").notNullable();
        table.timestamps(false, true);
    });

    await knex.schema.createTable("event_user", table => {
        table.increments();
        table.integer("event_id").unsigned();
        table.foreign("event_id").references("event.id");
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("user.id");
        table.integer("event_venue_id").unsigned();
        table.foreign("event_venue_id").references("event_venue.id");
        table.integer("event_date_id").unsigned();
        table.foreign("event_date_id").references("event_date.id");
        table.timestamps(false, true);
    });
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("event_user");
    await knex.schema.dropTable("event_date");
    await knex.schema.dropTable("event_venue");
    await knex.schema.dropTable("event");
    await knex.schema.dropTable("user");
    await knex.schema.dropTable("ref_venue");
}

