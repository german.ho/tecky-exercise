import Knex from "knex";

export async function seed(knex: Knex): Promise<any> {
    // Deletes ALL existing entries

    // Query Builder
    // return knex("table_name").del()
    //     .then(() => {
    //         // Inserts seed entries
    //         return knex("table_name").insert([
    //             { id: 1, colName: "rowValue1" },
    //             { id: 2, colName: "rowValue2" },
    //             { id: 3, colName: "rowValue3" }
    //         ]);
    //     });

    await knex.raw(/* sql */`DELETE FROM students`);
    await knex.raw(/* sql */`DELETE FROM teachers`);

    const result = await knex.raw(/*sql*/`INSERT INTO teachers 
        (name, date_of_birth) VALUES (?, ?) RETURNING id`, ["Bob", "1970-01-01"]);
    console.log(result.rows);
    const teacherId = result.rows[0].id;
    await knex.raw(/*sql*/`
        INSERT INTO students
        (name,level,date_of_birth,teacher_id) VALUES
        (?, ?, ?, ?),(?, ?, ?, ?),(?, ?, ?, ?)
    `,[
        "Peter",25,"1995-05-15",teacherId,
        "John",25,"1985-06-16",teacherId,
        "Simon",25,"1987-07-17",null
    ]);
};
