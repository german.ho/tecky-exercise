import Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable("students");
    if (!hasTable) {
        await knex.schema.createTable("students", (table) => {
            table.increments(); // AI, column name = "id"
            table.string("name");
            table.string("level");
            table.date("date_of_birth");
            table.integer("teacher_id").unsigned(); // column name = "teacher_id", INTEGER
            // define relationship between students.teacher_id and teachers.id
            // 1 teacher can have N students
            table.foreign("teacher_id").references("teachers.id");
            table.timestamps(false, true);
        });
    }
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTableIfExists("students");
}

