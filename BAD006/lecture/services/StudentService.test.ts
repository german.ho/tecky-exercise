import Knex from 'knex';

// Assuming you test case is inside `services/ folder`
const knexfile = require('../knexfile');

// Now the connection is a testing connection.
const knex = Knex(knexfile["testing"]);

import { StudentService } from './StudentService';

describe("StudentService", () => {
    let studentService: StudentService;

    beforeEach(async () => {
        studentService = new StudentService(knex);
        await knex.raw(/*sql*/`DELETE from students`);
        await knex.raw(/*sql*/`INSERT INTO students (name,level,date_of_birth) 
         VALUES (?,?,?)
        `, ['Peter', "30", "1990-01-01"]);
    })

    it("should get all students", async () => {
        const students = await studentService.getStudents();
        expect(students.length).toBe(1);
    });

    afterAll(() => {
        knex.destroy(); // Important!!
    });
});

// it("dummy test", () => {
//     expect(1+1).toBe(2);
// })