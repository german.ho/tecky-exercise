# BAD004

## Content

- Knex migration

- Knex seed

- Knex basic

## Knex

**3 ways working with SQL:**

- Writing Plain SQL (Knex)

- SQL Query Builder (Knex)

- Object Relational Mapping


## Install KING

```Bash
yarn init -y
yarn add typescript ts-node @types/node
yarn add knex @types/knex pg @types/pg
touch tsconfig.json
yarn knex init -x ts
yarn add dotenv @types/dotenv
```


## Knex Command

specific env

```Text
yarn knex --env production <...>
```

default env

```Text
yarn knex <...>
```


## knexfile.ts

```Typescript
import dotenv from 'dotenv';
dotenv.config();

module.exports = {

  development: {
    debug: true,
    client: 'postgresql',
    connection: {
      database: process.env.DB_NAME,
      user:     process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  },
  ...
}
```


## Dotenv

Please note that there is no space between and after the = sign. And then after you have called the method require('dotenv').config(), the environmental variables are then accessible at the object process.env.

Remember to add your `.env` to `.gitignore `to avoid it being pushed to gitlab!

Now we have setup a working knex project. We are going to run CRUD queries on the database using knex directly.

