import redis from 'redis';
import util from 'util';

async function main() {
    // setup client
    const client = redis.createClient();
    const redisSet = util.promisify(client.set).bind(client);
    const redisGet = util.promisify(client.get).bind(client);

    await redisSet("jason", "jason is handsome", "EX", 10);
    const result = await redisGet("jason");
    console.log(result);

    client.quit(() => console.log('quit'));
}

main();
