# Redis

Redis is a **in-memory key-value** store that is commonly used for **caching and queuing tasks**. Caching is particular useful for tackling with performance issues. It allows developers to store the query result instead of querying the PostgreSQL database to get back the data all the time.

## How to use

**Update:**

1. Insert/Update DB
2. Set value into Redis

**Query:**

1. Check if value exist in Redis
2. Yes => Return value to client
3. No => Query value from DB, set value into Redis and return value to client

## Command

### Set

**Syntax:**

```Text
set <key-name> <value>
```

**Example:**

```Text
set test "Jason"
```

**Result:**

```Text
OK
```

### Get

**Syntax:**

```Text
get <key-name>
```

**Example:**

```Text
get test
```

**Result:**

```Text
"Jason"
```

### Expire

```Text
expire <key-name> <second>
```

**Example:**

```Text
expire test 10
```

**Result:**

Success

```Text
1
```

fail

```Text
0
```

### TTL(Time To Live)

**Syntax:**

```Text
ttl <key-name>
```

**Example:**

```Text
ttl test
```
