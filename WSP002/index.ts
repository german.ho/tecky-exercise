// let a: number = 10

// let arr: Array<number> = [1,2,3,4,5]

// let obj: {name: string, age: number} = {
//     name: 'Alex',
//     age: 18
// }

// // Enum (java, C++, C#)
// enum Gender {
//     MALE = "M", 
//     FEMALE = "F"
//     UNSPECIFIED = "U"
// }

// let students: {name: string, age: number, gender: Gender}[] = [
//     {name: 'Gordon', age: 19, gender: Gender.MALE},
//     {name: 'Alex', age: 18, gender: Gender.MALE},
//     {name: 'Jason', age: 18, gender: Gender.FEMALE}
// ]

// let students: Array<{name: string, age: number, gender: Gender}> = [
//     {name: 'Gordon', age: 19, gender: Gender.MALE},
//     {name: 'Alex', age: 18, gender: Gender.MALE},
//     {name: 'Jason', age: 18, gender: Gender.FEMALE}
// ]

// for (const student of students){
//     switch (student.gender){
//         case Gender.MALE:
//         console.log('Hello Mr. ' + student.name)
//         break;
//         case Gender.FEMALE:
//         console.log('Hello Ms. ' + student.name)
//         break;
//         case Gender.UNSPECIFIED:
//         console.log('Hello ' + student.name)
//         break;
//     }
// }


// let b: string | number = 10;
// b = "ten"

// function double(num: number): number{
//     if (num <10 ){
//         return num * 2
// } else {
//     return 0;
//     }
// }

// console.log(double(a) * 5)

// const triple: (num: number) => number = (num: number) =>{
//     return num * 3
// }

// function HelloWorld (who: ()=> string){
//     console.log('Hello' + who ())
// }

// HelloWorld (() => 'Alex')

// //type alias
// type HelloWorldFunction = (who: () => string) => void


// export const someObject = "HelloWorld"

// export const someFunction = function(){
//     return "Foobar"
// }

// import {someFunction, someObject} from './calculator'
// console.log(someFunction())
// console.log(someObject)

// export function abc (){
//     console.log('abc')
// }

// export default abc

// import {someFunction, someObject} from './calculator'

// import def from './exportDefault'

// def()


