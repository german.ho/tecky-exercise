import express from "express";
import bodyParser from "body-parser";

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

import { AppleService } from "./services/AppleService";
import { AppleRouter } from "./routers/AppleRouter";

const appleService = new AppleService();
const appleRouter = new AppleRouter(appleService);

app.use("/apples", appleRouter.router());

const PORT = 8080;
app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});
