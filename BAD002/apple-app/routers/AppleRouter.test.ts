import { AppleRouter } from "./AppleRouter";

import { AppleService } from "../services/AppleService"; // mock 1
jest.mock("express"); // mock 2

// type Mockify<T> = {
//   [P in keyof T]: jest.Mock<any>;
// };

describe("AppleRouter", () => {
  let router: AppleRouter;
  // let service: Mockify<AppleService>;
  let service: AppleService;
  let resJson: jest.SpyInstance;
  let req: any;
  let res: any;

  beforeEach(function() {
    // service = {
    //  getApples: jest.fn(() => [{ id: 1, breed: "fuji", weight: 100 }]),
    // 	addApple: jest.fn(body => 10),
    //  updateApple: jest.fn((id, body) => 1),
    // 	deleteApple: jest.fn(id => null)
    // };
    service = new AppleService();
    jest.spyOn(service, "getApples").mockImplementation(() => [{ id: 1, breed: "fuji", weight: 100 }]);
    jest.spyOn(service, "addApple").mockImplementation((body: any) => 10);
    jest.spyOn(service, "updateApple").mockImplementation((id: number, body: any) => 1);
		jest.spyOn(service, "deleteApple").mockImplementation((id: number) => id);
    // router = new AppleRouter((service as any) as AppleService);
    router = new AppleRouter(service);
    req = {
      body: {},
      params: {}
    };
    res = {
      json: () => {}
    };
    resJson = jest.spyOn(res, "json");
  });

  it("should handle get method correctly", () => {
    router.get(req, res);
    expect(service.getApples).toBeCalledTimes(1);
    expect(resJson).toBeCalledWith([{ id: 1, breed: "fuji", weight: 100 }]);
  });

  it("should handle post method correctly", () => {
    req.body = {
      breed: "Red delicious",
      weight: 230
    };
    router.post(req, res);
    expect(service.addApple).toBeCalledTimes(1);
    expect(service.addApple).toBeCalledWith(req.body);
    expect(resJson).toBeCalledWith({ updated: 1 });
  });

  it("should handle put method correctly", () => {
    req.body = {
      breed: "Red delicious",
      weight: 230
    };
    req.params = {
      id: 1
    };
    router.put(req, res);
    expect(service.updateApple).toBeCalledTimes(1);
    expect(service.updateApple).toBeCalledWith(req.params.id, req.body);
    expect(resJson).toBeCalledWith({ updated: 1 });
  });

  it("should handle delete method correctly", () => {
    req.params = {
      id: 1
    };
    router.delete(req, res);
    expect(service.deleteApple).toBeCalledTimes(1);
    expect(service.deleteApple).toBeCalledWith(req.params.id);
    expect(resJson).toBeCalledWith({ updated: 1 });
  });
});
