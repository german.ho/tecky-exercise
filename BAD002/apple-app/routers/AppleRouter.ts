import express from "express";
import { Request, Response } from "express";
import { AppleService } from "../services/AppleService";

export class AppleRouter {
  constructor(private appleService: AppleService) {}

  router() {
    const router = express.Router();
    router.get("/", this.get.bind(this));
    router.post("/", this.post.bind(this));
    router.put("/:id", this.put.bind(this));
    router.delete("/:id", this.delete.bind(this));
    return router;
  }

  get(req: Request, res: Response) {
    res.json(this.appleService.getApples());
  }

  post(req: Request, res: Response) {
    this.appleService.addApple(req.body);
    res.json({ updated: 1 });
  }

  put(req: Request, res: Response) {
    const updated = this.appleService.updateApple(parseInt(req.params.id), req.body);
    res.json({ updated });
  }

  delete(req: Request, res: Response) {
    this.appleService.deleteApple(parseInt(req.params.id));
    res.json({ updated: 1 });
  }
}
