import data from "./data";
import { Apple } from "./models";

export class AppleService {
  private apples: Apple[];
  private appleIdCounter: number;
  constructor() {
    this.apples = data.apples.slice(0);
    this.appleIdCounter = this.apples.length;
  }

  getApples() {
    return this.apples;
  }

  addApple(body: any) {
    const breed = body.breed;
    const weight = isNaN(body.weight) ? 0 : parseInt(body.weight);
    const apple = new Apple(breed, weight, ++this.appleIdCounter);
    this.apples.push(apple);
    return this.appleIdCounter;
  }

  updateApple(id: number, body: any) {
    const breed = body.breed;
    const weight = isNaN(body.weight) ? 0 : parseInt(body.weight);
    const apple = this.apples.find(apple => apple.id == id);
    if (!apple) {
      return 0;
    }
    apple.breed = breed;
    apple.weight = weight;
    return 1;
  }

  deleteApple(id: number) {
    this.apples = this.apples.filter(apple => apple.id != id);
    return 1;
  }
}
