import { AppleService } from './AppleService';
import { Apple } from './models'; // define type

jest.mock('./data');

describe("AppleService",()=>{

    let service: AppleService;
    let apples: Apple[];

    beforeEach(function(){
        apples = [
            {
                id:1,
                breed:'fuji',
                weight: 300
            }
				]
        service = new AppleService();
    });

    it("should get Apples correctly",()=>{
        const result = service.getApples();
        expect(result).toEqual(apples);
    });

    it("should add apples correctly",()=>{
        const id = service.addApple({
            breed:"Red Delicious",
            weight:400
        })
        expect(id).toEqual(2);
        expect(service.getApples().length).toEqual(2);
    })

    it("should update apple correctly",()=>{
        const updated = service.updateApple(1, {weight:400,breed:"Red Delicious"});
				expect(updated).toEqual(1);
				// expect(service.getApples()).not.toContainEqual(apples[0]);
        expect(service.getApples()[0]).not.toEqual(apples[0]);
    });

    it("should delete apple correctly",()=>{
        const updated = service.deleteApple(1);
        expect(updated).toEqual(1);
        expect(service.getApples().length).toEqual(0);
    });

});