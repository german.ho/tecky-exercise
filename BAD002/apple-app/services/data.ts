const data = {
  apples: [
    {
      id: 1,
      breed: "fuji",
      weight: 300
    },
    {
      id: 2,
      breed: "Red delicious",
      weight: 230
    },
    {
      id: 3,
      breed: "Ralls Janet",
      weight: 200
		},
		{
      id: 4,
      breed: "Pink Lady",
      weight: 100
    }
  ]
};

export default data;
