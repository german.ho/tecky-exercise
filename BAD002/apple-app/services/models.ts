export class Apple {
  constructor(
    public breed: string,
    public weight: number,
    public id?: number
  ) {}
}

export interface User {
  id: number;
  username: string;
  password: string;
}
