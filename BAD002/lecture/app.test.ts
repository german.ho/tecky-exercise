import printNumbers from './app'
import { Person, goToBar, printNumbersWithTimer, goToBarWithPromise } from './app'
import filter from './filter'

// import from './filter'
// import from 'express'
jest.mock('./filter');

jest.useFakeTimers();

// it("testing printNumbers", ()=> {
// 	(filter as jest.Mock).mockReturnValue([1, 3, 5, 7]);
// 	console.log = jest.fn();
// 	printNumbers();
// 	expect(filter).toBeCalledTimes(1);
// 	expect(console.log).toBeCalledWith([1, 3, 5, 7]);
// });

afterEach(() => {
	(filter as jest.Mock).mockClear();
});

it("testing printNumbers V2", ()=> {
	// (filter as jest.Mock).mockReturnValue([1, 3, 5, 7]);
	console.log = jest.fn();
	printNumbers();
	expect(filter).toBeCalledTimes(1);
	expect(console.log).toBeCalledWith([1, 3, 5]);
});

it("testing printNumbersTimer", ()=> {
	console.log = jest.fn();
	printNumbersWithTimer();
	expect(filter).toBeCalledTimes(1);
	jest.advanceTimersByTime(5000);
	expect(console.log).toBeCalledWith([1, 3, 5]);
});

it("testing spyOn drinks", () => {
	const jason = new Person(18);
	const peter = new Person(12);
	const jasonDrinkSpy = jest.spyOn(jason, "drink");
	const peterDrinkSpy = jest.spyOn(peter, "drink");
	goToBar([jason, peter]);
	expect(jasonDrinkSpy).toBeCalled();
	expect(peterDrinkSpy).not.toBeCalled();
});

it("testing spyOn drinks with Promise", () => {
	const john = new Person(15);
  const peter = new Person(20);
  const johnSpy = jest.spyOn(john, "drink");
  const peterSpy = jest.spyOn(peter, "drink");

  const adults = goToBarWithPromise([john, peter]);
  expect(johnSpy).not.toBeCalled();
  expect(peterSpy).not.toBeCalled();
  expect(adults).rejects.toEqual([peter]);
});

it("Testing goToBar with a teenager in async function", async () => {
  const john = new Person(15);
  const peter = new Person(20);
  const johnSpy = jest.spyOn(john, "drink");
  const peterSpy = jest.spyOn(peter, "drink");
  try {
    await goToBarWithPromise([john, peter]);
  } catch (err) {
    expect(err).toEqual([peter]);
  }
  expect(johnSpy).not.toBeCalled();
  expect(peterSpy).not.toBeCalled();
});