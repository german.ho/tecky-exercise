import filter from "./filter";

function printNumbers() {
  const oddNumbers = filter([1, 2, 3, 4, 5], num => num % 2 !== 0);
  console.log(oddNumbers);
}

export default printNumbers;

export class Person {
  constructor(public age: number) {}

  drink() {
    console.log("I am drunk");
  }
}

export function goToBar(people: Person[]) {
  const adults = people.filter(person => person.age >= 18);
  adults.forEach(adult => adult.drink()); // map will return [ undefined, undefined, ... ], for loop is better
}

export function goToBarWithPromise(people: Person[]): Promise<Person[]> {
  return new Promise((resolve, reject) => {
    const adults = people.filter(person => person.age >= 18);
    if (adults.length === people.length) {
      adults.forEach(person => person.drink());
      resolve(adults);
    } else {
      reject(adults);
    }
  });
}

export function printNumbersWithTimer() {
  const oddNumbers = filter([1, 2, 3, 4, 5], num => num % 2 != 0);
  setTimeout(() => {
    console.log(oddNumbers);
  }, 5000);
}
