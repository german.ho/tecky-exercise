import filter from './filter'

// it("Testing filter",()=>{
// 	const mockPredicate = jest.fn(x=>x % 2 == 0);
// 	const filtered = filter([1,2,3,4],mockPredicate);
// 	expect(mockPredicate.mock.calls.length).toBe(4);
// 	expect(mockPredicate.mock.calls[0][0]).toBe(1);
// 	expect(filtered.length).toBe(2);
// });

it("testing filter", () => {
	const predicate = (x: number) => x % 2 === 0;
	const mockPredicate = jest.fn(predicate);
	const testArr = [1, 2, 3, 4];
	const result = filter(testArr, mockPredicate); // [2, 4]
	expect(mockPredicate.mock.calls.length).toBe(testArr.length);
	for (let i = 0; i < testArr.length; i++) {
		// console.log(mockPredicate.mock.calls[i][1]);
		expect(mockPredicate.mock.calls[i][0]).toBe(testArr[i]);
	}
	expect(result.length).toBe(2);
	expect(result).toContain(2);
});