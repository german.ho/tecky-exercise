import { MemoService } from './MemoService';
import fs from 'fs';

const testFile = './memo-test.json';

describe('MemoService', () => {
  beforeEach(async () => {
    await fs.promises.writeFile(testFile, '[]');
  });

  // getMemo integration test
  it('Test get memo', async () => {
    // Arrange
    const memoService = new MemoService(testFile);

    // Act
    const memos = await memoService.getMemos();

    // Assert
    expect(memos).toEqual([]);
  })

  // getMemo integration test
  it('Update memo', async () => {
    // Arrange
    await fs.promises.writeFile(testFile, JSON.stringify([
      {content: "def"},
      {content: "123"}
    ]));
    const memoService = new MemoService(testFile);

    // Act
    await memoService.updateMemo(0, 'abc');
    const memos = await memoService.getMemos();

    // Assert
    expect(memos).toEqual([
      {content: "abc"},
      {content: "123"}
    ]);
  })
  // getMemo integration test
  it('Update -1 memo', async () => {
    // Arrange
    await fs.promises.writeFile(testFile, JSON.stringify([
      {content: "def"},
      {content: "123"}
    ]));
    const memoService = new MemoService(testFile);

    expect.assertions(1);

    // Act & Assert
    await expect(memoService.updateMemo(-1, 'abc')).rejects.toThrow('Invalid ID');
  })
})