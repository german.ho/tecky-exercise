import jsonfile from 'jsonfile';

export class MemoService {
	private memosJsonPath: string;

	// Dependency Injection
	// Inverse of Control
	constructor(memosJsonPath: string) {
		this.memosJsonPath = memosJsonPath;
	}

	async getMemos() {
		const memos = await jsonfile.readFile(this.memosJsonPath);
		return memos;
	}

	async updateMemo(id: number, content: string) {
		const memos = await this.getMemos();
		if (id < 0 || id >= memos.length) {
			throw new Error('Invalid ID');
		}
		memos[id].content = content;
		await jsonfile.writeFile(this.memosJsonPath, memos);
	}

	async updateMemoWithImage(id: number, filename: string) {
		const memos = await this.getMemos();
		memos[id].image = filename;
		await jsonfile.writeFile(this.memosJsonPath, memos);
	}

}