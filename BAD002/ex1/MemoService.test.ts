import { MemoService } from './MemoService';
import jsonfile from 'jsonfile';

jest.mock('jsonfile')

describe('MemoService', () => {

  // getMemo unit test
  it('Test get memo', async () => {
    // Arrange
    const memoService = new MemoService("./memo.json");
    (jsonfile.readFile as jest.Mock).mockReturnValue([]);

    // Act
    const memos = await memoService.getMemos();

    // Assert
    expect(memos).toEqual([]);
  })

  it('Test update memo', async () => {
    // Arrange
    const memoService = new MemoService("./memo.json");
    (jsonfile.readFile as jest.Mock).mockReturnValue([
      {content: "def"},
      {content: "123"}
    ]);
    (jsonfile.writeFile as jest.Mock).mockReturnValue(null);
    
    // Act
    await memoService.updateMemo(0, "abc");

    // Assert
    expect(jsonfile.writeFile as jest.Mock).toBeCalledWith(
      "./memo.json",
      [
        {content: "abc"},
        {content: "123"}
      ]
    );
  })
})