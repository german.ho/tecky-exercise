import { MemoRouter } from "./MemoRouter"
import { IMemoService } from "./IMemoService";
import { MemoService } from "./MemoService";
import fs from 'fs';

const testFile = './memo-test.json';

describe('MemoRouter', () => {
  let memoRouter: MemoRouter;
  let io: SocketIO.Server;
  let memoService: IMemoService;

  beforeEach(async () => {
    memoService = new MemoService(testFile);

    io = {
      emit: jest.fn()
    } as any;

    memoRouter = new MemoRouter(memoService, io, {});

    await fs.promises.writeFile(testFile, JSON.stringify([
      {content: 'abc'},
      {content: '123'}
    ]))
  })

  it('get memo', async () => {
    // Arrange
    let res = {
      json: jest.fn()
    };

    // Act
    await memoRouter.getMemos({} as any, res as any);

    // Assert
    expect(res.json).toBeCalledWith([
      {content: 'abc'},
      {content: '123'}
    ])
  })

  it('update memo with session', async () => {
    // Arrange
    let req = {
      session: {
        isAdmin: true,
        socketId: '999'
      },
      params: {
        id: '0'
      },
      body: {
        content: '123'
      }
    }
    let res = {
      json: jest.fn()
    };

    // Act
    await memoRouter.updateMemo(req as any, res as any);

    // Assert
    expect(res.json).toBeCalledWith({success: true})
    expect(io.emit).toBeCalledWith('memo-update', '999');
    expect(JSON.parse(await fs.promises.readFile(testFile, 'utf8'))).toEqual([
      {content: '123'},
      {content: '123'}
    ])
  })
})