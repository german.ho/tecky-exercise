export interface Memo {
	content: string;
}

export interface IMemoService {
	getMemos():Promise<Memo[]>;
	updateMemo(id: number, content: string):Promise<void>;
	updateMemoWithImage(id: number, filename: string) :Promise<void>;
}