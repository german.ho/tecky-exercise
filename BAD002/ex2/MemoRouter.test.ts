import { MemoRouter } from "./MemoRouter"
import { IMemoService } from "./IMemoService";

describe('MemoRouter', () => {
  let memoRouter: MemoRouter;
  let io: SocketIO.Server;
  let memoService: IMemoService;

  beforeEach(() => {
    memoService = {
      getMemos: jest.fn(),
      updateMemo: jest.fn(),
      updateMemoWithImage: jest.fn(),
    } as any;

    io = {
      emit: jest.fn()
    } as any;

    memoRouter = new MemoRouter(memoService, io, {});
  })

  it('get memo', async () => {
    // Arrange
    let res = {
      json: jest.fn()
    };
    (memoService.getMemos as jest.Mock).mockReturnValue([
      {content: 'abc'},
      {content: '123'}
    ]);

    // Act
    await memoRouter.getMemos({} as any, res as any);

    // Assert
    expect(res.json).toBeCalledWith([
      {content: 'abc'},
      {content: '123'}
    ])
  })

  it('update memo with session', async () => {
    // Arrange
    let req = {
      session: {
        isAdmin: true,
        socketId: '999'
      },
      params: {
        id: '0'
      },
      body: {
        content: '123'
      }
    }
    let res = {
      json: jest.fn()
    };

    // Act
    await memoRouter.updateMemo(req as any, res as any);

    // Assert
    expect(res.json).toBeCalledWith({success: true})
    expect(memoService.updateMemo).toBeCalledWith(0, '123');
    expect(io.emit).toBeCalledWith('memo-update', '999');
  })

  it('update memo without session', async () => {
    // Arrange
    let req = {
      params: {
        id: '0'
      },
      body: {
        content: '123'
      }
    }
    let res: any = {
      json: jest.fn(),
      status: jest.fn(() => res),
    };

    // Act
    await memoRouter.updateMemo(req as any, res as any);

    // Assert
    expect(res.status).toBeCalledWith(401)
    expect(res.json).toBeCalledWith({success: false, message: 'unauthorized'})
    expect(memoService.updateMemo).not.toBeCalled();
    expect(io.emit).not.toBeCalled();
  })
})