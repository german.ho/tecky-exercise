import express from "express";
import { Request, Response } from "express";
import { IMemoService } from "./IMemoService";
import socketIO from 'socket.io';

export class MemoRouter {
  constructor(
    private memoService: IMemoService,
    private io: socketIO.Server,
    private upload: any
  ) {}

  router() {
    const router = express.Router();
    router.get("/", this.getMemos);
    router.put("/:id", this.updateMemo);
    router.put("/:id/image", this.upload.single("photo"), this.updateMemoWithImage);
    return router;
  }

  getMemos = async (req: Request, res: Response) => {
    try {
      const memos = await this.memoService.getMemos();
      res.json(memos);
    } catch (e) {
      console.error(e);
      res.status(500).json({ success: false });
    }
  };

  updateMemo = async (req: Request, res: Response) => {
    try {
      if (!(req.session && req.session.isAdmin)) {
        res.status(401).json({ success: false, message: "unauthorized" });
        return;
      }

      const { id } = req.params;
      const idNum = parseInt(id);
      if (isNaN(idNum)) {
        res.status(400).json({ success: false });
        return;
      }
			await this.memoService.updateMemo(idNum, req.body.content);
			console.log(req.session.socketId);
			this.io.emit("memo-update", req.session.socketId);
      res.json({ success: true });
    } catch (e) {
      console.error(e);
      res.status(500).json({ success: false });
    }
  };

  updateMemoWithImage = async (req: Request, res: Response) => {
		try {
			if (!(req.session && req.session.isAdmin)) {
				res.status(401).json({success: false, message: 'unauthorized'})
				return;
			}
			// console.log(req.file);
			const { id } = req.params;
      const idNum = parseInt(id);
      if (isNaN(idNum)) {
        res.status(400).json({ success: false });
        return;
			}
			await this.memoService.updateMemoWithImage(idNum, req.file.filename)
			res.json({success: true})
		} catch (e) {
			console.error(e);
			res.status(500).json({success: false});
		}
	};
}
