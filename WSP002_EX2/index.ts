function findFactors(num:number){
    let factors: number[] = [];
    for(let factor = 2; factor <= num / 2 ; factor++){
        if(num % factor === 0){
           factors.push(factor);
        }
    }
    return factors;
 }
 
 function leapFunction (year: number){
     if (year % 4 === 0 && year % 100 !==0){
         console.log("year is leap year")
     } else if (year % 400 === 0){
         console.log("year is leap year")
     } else {
         console.log("year is ordinary year")
     }
 }

 function rnaTranscription(input: string) {
	let result = "";
	for (let dna of input) {
		switch (dna) {
			case 'G':
				result += 'C';
				break;
			case 'C':
				result += 'G';
				break;
			case 'T':
				result += 'A';
				break;
			case 'A':
				result += 'U';
				break;
		}
	}
	console.log(result);
	return result;
}

rnaTranscription("GCTAGCTJJJJJ")

function factorial(number: number): number{
    if(number === 0 || number === 1){
       return 1;
    }
 
    return factorial(number - 1) * number
 }

 type Student = {
    name: string,
    age: number,
    exercises?: { score: number, date: Date }[]
  }
  
  const peter: {
    name: string,
    age: number,
    students: Array<Student>
  } = {
    name: "Peter",
    age: 50,
    students:[
       { name:"Andy", age:20},
       { name:"Bob", age:23},
       {name: "Charlie", age:25 , exercises:[
           { score: 60 , date: new Date("2019-01-05") }
       ]}
    ]
  }  


const timeoutHandler: () => void = ()=>{
    console.log("Timeout happens!");
}

const timeout:1000 = 1000;

setTimeout(timeoutHandler,timeout);


const someValue:12 | null = Math.random() > 0.5? 12: null;