const person = {name: "Jason", age: 18, students: [
    {
    name: "peter",
    sid: 1,
    age: 12,
    languages: ["Javascript", "Java"]
}, 
{
    name: "tom",
    sid: 2,
    age: 20,
    languages: ["Javascript"]
}, 
{
    name: "mary",
    sid: 3,
    age: 30,
    languages: ["python"]
}
]
}

for (let key in person){
    console.log(person[key])
}

for (const num of [1,2,3]){
    console.log(num)
}

for (const students of person.students){
    console.log(students.name)
    for (let i = 0; i < students.languages.length; i++){
        console.log(students.languages[i])
    }
    for (const languages of students.languages
        console.log(language))
}

const studentsOver15 = []
for (const student of person.students){
    if (student.age >= 15){
        studentsOver15.push(student)
    }
}
console.log(studentsOver15)



const studentsOver15 = []
for (const student of person.students){
    if (student.age >= 15){
        studentsOver15.push({
            name: student.name, 
            sid: student.sid,
            age: student.age,
        })
    }
}
console.log(studentsOver15)


function findStudent(name){
    let studentFound = {};
    for(let student of peter.students){
        if(student.name === name){
           studentFound = student;
           return studentFound;
        }
    }
    return studentFound
}

findStudent("mary");


function findStudent(name){
    let studentFound = [];
    for(let student of peter.students){
        if(student.name === name){
        studentFound.push(student)
        }
    }
    return studentFound

    //const personString = JSON.stringify(person); object -> JSON
    //const newPerson = JSON.parse(personString); JSON -> object

    const jsonStr = JSON.stringify(person)
    //string
    console.log(typeof jsonStr) 
    
    const jasonFromJson = JSON.parse(jsonStr)
    //string
    console.log(typeof jasonFromJson);
    console.log(jasonFromJson) 

    const jasonFromJson = JSON.parse('{"students": [123')
    //string
    console.log(typeof jasonFromJson);
    console.log(jasonFromJson) 

    let a = {val : 10}
    let b = a
    b.val = 100
    console.log(a,b)

    let jason = {name: "Jason"}
    let teacher = jason
    
    let a = {val: 10}
    let b = Object.assign({} ,a})
    console.log (a,b)

    b.val = 100
    console.log (a,b)


    let a = {val:10}
    let {val} = a
    console.log(val)

    let a = {name: "jason"}
    let {name} = a
    console.log(name)
    
    let b ={name: name}
    console.log (b)
    b.name = "peter"
    console.log (a,b)

    let a = {name: "jason", age: 18}
    let b ={...a}
    console.log (a,b)

    b.name = "peter"
    console.log(a,b)

    let a = [1,2,3]
    let b = a.slice()
    let c = [...a]
    let [num1, num2] = a
    let d = [num1, num2, num 3]
    b.push(4) // 1,2,3,4 - b
    c.push(5) // 1,2,3,5 -c
    d.push(6) // 1,2,3,6 - d
    console.log (a,b,c,d)

    //a - 1,2,3

    const person = {name: "Jason", age: 18, students: [
        {
        name: "peter",
        sid: 1,
        age: 12,
        languages: ["Javascript", "Java"]
    }, 
    {
        name: "tom",
        sid: 2,
        age: 20,
        languages: ["Javascript"]
    }, 
    {
        name: "mary",
        sid: 3,
        age: 30,
        languages: ["python"]
    }
    ]
    }

    const anotherPerson = {...person}
    console.log(person, anotherPerson)

    anotherPerson.students[0].age -= 1
    console.log (person, anotherPerson)

    Array.map(function callback(currentValue))
    //output: array
    //const map1 = array1.map( x => x *2 )
    //retrn element for new_array
    //the length of the output is equal to length of X
    //change to original function:

    const map1 = array1.map(x => x * 2) OR 
    const map1 = array1.map (function (x, index, arr)){
        return x * 2
    }

    const map2 = array1.map(function(x,idx)){
        if (idx <2){
            return x * 2
        } else {
            return x
        }
      }

    //Reduce Function
    // output: single output value 
    arr.reduce(callback(accumulator, currentValue [, index [, array](,initialValue)]))

    console.log(array1.reduce(reducer, 0))
    initialValue: 0
    A: 0 ; C: 1 => 1
    A: 1  ; C 2 => 3
    A: 3  ; C 3 => 6
    A: 6 ;  C 4 => 10

    console.log(array1.reduce(reducer, 5))
    init: 5
    A: 5, C 1 =>6
    A: 6, C 2 =>8
    A: 8  C 3 =>11
    A: 11 C 4 => 15

    const reverseStringV4 = input =>{
    const arr = input.split ("")
    const result = arr.reduce((acc, cur) => cur + acc, "")
    console.log (arr)  
    return result
    }
    console.log(reverseStringV4("cool"))

    const keyArr = ["name","age"]
    const valArr = ["jason", 18]

    const jason = keyArr.reduce((acc,key, idx) => {
        console.log(key, idx)
        acc[key] = valArr[idx]
        return acc;
    } , {})
    console.log(jason)

     //{
       // name: "jason",
        //age: 18
    //}

    //filter
    
    const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
    const result = words.filter((word) => word.length > 6);
    console.log(result);

    const data = ""
    const json = JSON.parse (data)[0]

    console.log (hongKong)
    
    const json = {
        name: "jason",
        age: 18
    }

    for (let key in jason){
        console.log('${key}: ${jason[key]}')
    }