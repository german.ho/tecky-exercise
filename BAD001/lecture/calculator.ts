export class Calculator {

	private num: number;

	constructor() {
		this.num = 0;
	}

	add(num: number) {
		this.num += num;
		return this.num;
	}
}