import { Calculator } from './calculator';

let calculator: Calculator;

beforeEach(() => {
	calculator = new Calculator();
});

it('case 1', () => {
	// const calculator = new Calculator();
	console.log('case 1')
	expect(calculator.add(10)).toBe(10);
	expect(calculator.add(15)).toBe(25);
});

it.only('case 2', () => {
	// const calculator = new Calculator();
	console.log('case 2')
	expect(calculator.add(10)).toBe(10);
	expect(calculator.add(15)).not.toBe(10);
});

afterEach(() => {
	// calculator = new Calculator();
	console.log('HI, Jason');
});

// ==============================================================================
// Demo 2

// let calculator: Calculator;

// beforeAll(() => {
// 	console.log('start');
// 	calculator = new Calculator();
// });

// afterAll(() => {
// 	console.log('end');
// });

// it('case 1', () => {
// 	console.log('case 1');
// 	expect(calculator.add(10)).toBe(10);
// 	expect(calculator.add(15)).toBe(25);
// });

// it('case 2', () => {
// 	console.log('case 2');
// 	expect(calculator.add(10)).toBe(35);
// });

// ==============================================================================
// Demo 3

// beforeAll(() => console.log('1 - beforeAll'));
// afterAll(() => console.log('1 - afterAll'));
// beforeEach(() => console.log('1 - beforeEach'));
// afterEach(() => console.log('1 - afterEach'));
// test('', () => console.log('1 - test'));

// describe('Scoped / Nested block', () => { // define a scope
//   beforeAll(() => console.log('2 - beforeAll'));
//   afterAll(() => console.log('2 - afterAll'));
//   beforeEach(() => console.log('2 - beforeEach'));
//   afterEach(() => console.log('2 - afterEach'));
// 	test('', () => console.log('2 - test'));
// });

// describe('Scoped / Nested block 2', () => { // define a scope
// 	beforeAll(() => console.log('3 - beforeAll'));
// 	afterAll(() => console.log('3 - afterAll'));
// 	beforeEach(() => console.log('3 - beforeEach'));
// 	afterEach(() => console.log('3 - afterEach'));
// 	test('', () => console.log('3 - test'));
// });
