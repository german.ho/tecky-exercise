import sum from './sum'

test('adds 1 + 2 to equal 3', () => {
	expect(sum(1, 2)).toBe(3);
});

// it('case 1 number to be', () => {
// 	const num = 10;
// 	expect(num).toBe(10);
// 	expect(num).not.toBe(15);
// });

// it('case 2 string to be', () => {
// 	const str = 'jason';
// 	expect(str).toBe('jason');
// });

// it('case 3 object to be', () => { // reference
// 	const person = { name: 'Jason' };
// 	expect(person).toBe(person);
// });

// it('case 4 object to Equal', () => { // value
// 	const person = { name: 'Jason' };
// 	expect(person).toEqual({ name: 'Jason' });
// });

// test('case 5 null', () => {
// 	const n = null;
// 	expect(n).toBeNull();
// 	expect(n).toBeDefined();
// 	expect(n).not.toBeUndefined();
// 	expect(n).not.toBeTruthy();
// 	expect(n).toBeFalsy();
// });

// it('case 6 10 + 10', () => {
// 	const result = sum(10, 10);
// 	expect(result).toBeGreaterThan(19);
// 	expect(result).toBeGreaterThanOrEqual(20);
// 	expect(result).toBeLessThan(21);
// 	expect(result).toBeLessThanOrEqual(20);
// 	expect(result).toBe(20);
// 	expect(result).toEqual(20);
// });

// it('case 7 0.1 + 0.2', () => {
// 	const num = 0.1 + 0.2;
// 	expect(num).toBeCloseTo(0.3);
// });

// it('case 8 PI', () => {
// 	expect(3.14).toBeCloseTo(Math.PI);
// });

// it('case 9 the shopping list has beer on it', () => {
// 	const shoppingList = [
// 		'diapers',
// 		'kleenex',
// 		'trash bags',
// 		'paper towels',
// 		'beer',
// 	];
// 	expect(shoppingList).toContain('beer');
// 	expect(shoppingList).not.toContain('milk');
// });