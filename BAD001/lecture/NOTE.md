# BAD001

## Installation od Jest

### Install Yarn

```Bash
sudo npm i -g yarn
```

### Create a Project

```Bash
yarn init -y
```

### Install Jest Package

```Bash
yarn add --dev jest
yarn add --dev typescript ts-jest @types/jest
yarn ts-jest config:init
```

## Hello, world Example

```sum.test.ts
import sum from './sum'

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});
```

## Run the test case

```Bash
yarn jest
```

Result:

```Bash
 PASS  ./sum.test.ts (5.232s)
  ✓ adds 1 + 2 to equal 3 (6ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        5.484s
Ran all test suites.
✨  Done in 7.88s.
```

## Scope

```Typescript
beforeAll(() => console.log('1 - beforeAll'));
afterAll(() => console.log('1 - afterAll'));
beforeEach(() => console.log('1 - beforeEach'));
afterEach(() => console.log('1 - afterEach'));
test('', () => console.log('1 - test'));

describe('Scoped / Nested block', () => { // define a scope
  beforeAll(() => console.log('2 - beforeAll'));
  afterAll(() => console.log('2 - afterAll'));
  beforeEach(() => console.log('2 - beforeEach'));
  afterEach(() => console.log('2 - afterEach'));
  test('', () => console.log('2 - test'));

  describe('Scoped / Nested block 2', () => { // define a scope
    beforeAll(() => console.log('3 - beforeAll'));
    afterAll(() => console.log('3 - afterAll'));
    beforeEach(() => console.log('3 - beforeEach'));
    afterEach(() => console.log('3 - afterEach'));
    test('', () => console.log('3 - test'));
  });
});
```

```Bash
1 - beforeAll

1 - beforeEach
1 - test
1 - afterEach

2 - beforeAll
1 - beforeEach
2 - beforeEach
2 - test
2 - afterEach
1 - afterEach
2 - afterAll

3 - beforeAll
1 - beforeEach
3 - beforeEach
3 - test
3 - afterEach
1 - afterEach
3 - afterAll

1 - afterAll
```
