import { printFizzBuzz, FizzBuzz } from './ex2';

it("case negative number", () => {
	expect(() => { printFizzBuzz(-10) }).toThrow("Found Negative Number");
});

it("case floating number", () => {
	expect(() => { printFizzBuzz(1.15) }).toThrow("Found Floating Number");
});

it("case 3 numbers", () => {
	expect(printFizzBuzz(3)).toBe("1, 2, Fizz");
});

it("case 5 numbers", () => {
	expect(printFizzBuzz(5)).toBe("1, 2, Fizz, 4, Buzz");
});

it("case 15 numbers", () => {
	expect(printFizzBuzz(15)).toBe("1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz");
});

it("case 36 numbers" , () => {
	expect(printFizzBuzz(36)).toBe("1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz, 16, 17, Fizz, 19, Buzz, Fizz, 22, 23, Fizz, Buzz, 26, Fizz, 28, 29, Fizz Buzz, 31, 32, Fizz, 34, Buzz, Fizz");
})

it.only("test FizzBuzz Class: 15 numbers", () => {
	const fizzBuzz = new FizzBuzz();
	fizzBuzz.addNumber(3);
	fizzBuzz.addNumber(12);
	expect(fizzBuzz.printFizzBuzz()).toBe("1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz");
})