export function factorial(num: number): number {
	if (num < 0) {
		throw new Error("Found Negative Number");
	}
	if (num % 1 !== 0) {
		throw new Error("Found Floating Number");
	}
  if (num === 0 || num === 1) {
    return 1;
  }
  return factorial(num - 1) * num;
}

export function fibonacci(num: number): number {
	if (num < 0) {
		throw new Error("Found Negative Number");
	}
  if (num == 1 || num == 2) {
    return 1;
  }
  return fibonacci(num - 1) + fibonacci(num - 2);
}

export function fibonacciV2(num: number) {
	let count = 0;
	let a = 1;
	let b = 0;
	let tmp: number;

	let n = 1;

	while (n < num) {
		const aStr = String(a);
		if (aStr[0] === '1') {
			count++;
		}
		tmp = a;
		a += b;
		b = tmp;
		n++;
	}

	return count / num;
}