export function printFizzBuzz(num: number) {
	if (num < 0) {
		throw new Error("Found Negative Number");
	}
	if (num % 1 !== 0) {
		throw new Error("Found Floating Number");
	}
	const arr: (string|number)[] = [];
	for (let i = 1; i <= num; i++) {
		if (i % 5 === 0 && i % 3 === 0) {
			arr.push("Fizz Buzz");
		} else if (i % 5 === 0) {
			arr.push("Buzz");
		} else if (i % 3 === 0) {
			arr.push("Fizz");
		} else {
			arr.push(i);
		}
	}
	return arr.join(", ");
}

export class FizzBuzz {

	private arr: (string|number)[] = [];

	constructor() {
		this.arr = [];
	}

	addNumber(num: number) {
		if (num < 0) {
			throw new Error("Found Negative Number");
		}
		if (num % 1 !== 0) {
			throw new Error("Found Floating Number");
		}
		const tmp = this.arr.length;
		for (let i = tmp + 1; i <= tmp + num; i++) {
			if (i % 5 === 0 && i % 3 === 0) {
				this.arr.push("Fizz Buzz");
			} else if (i % 5 === 0) {
				this.arr.push("Buzz");
			} else if (i % 3 === 0) {
				this.arr.push("Fizz");
			} else {
				this.arr.push(i);
			}
		}
	}

	printFizzBuzz() {
		return this.arr.join(", ");
	}
}