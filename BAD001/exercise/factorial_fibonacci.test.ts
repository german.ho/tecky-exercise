import { factorial, fibonacci, fibonacciV2 } from './factorial_fibonacci';

it("[factorial] case negative number", () => {
	expect(() => { factorial(-10) }).toThrow("Found Negative Number");
});

// floating number??
it("[factorial] case floating number", () => {
	expect(() => { factorial(1.15) }).toThrow("Found Floating Number");
});

it("[factorial] test 0! = 1", () => {
	expect(factorial(0)).toBe(1);
});

it("[factorial] test 1! = 1", () => {
	expect(factorial(1)).toBe(1);
});

it("[factorial] test 10! = 3628800", () => {
	expect(factorial(10)).toBe(3628800);
});

it("[fibonacci] case negative number ", () => {
	expect(() => { fibonacci(-10) }).toThrow("Found Negative Number");
});

it("[fibonacci] test 1st number = 1", () => {
	expect(fibonacci(1)).toBe(1);
});

it("[fibonacci] test 10th number = 34", () => {
	expect(fibonacci(10)).toBe(55);
});

it("[fibonacci] test ben-ford's law 100 numbers", () => {
	expect(fibonacciV2(100)).toBeCloseTo(0.3);
});

it("[fibonacci] test ben-ford's law 500 numbers", () => {
	expect(fibonacciV2(500)).toBeCloseTo(0.3);
});