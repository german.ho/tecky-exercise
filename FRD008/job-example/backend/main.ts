import express from 'express'
import bodyParser from 'body-parser';
import cors from 'cors';

const app = express();

interface Student {
  id: number,
  name: string;
  status: string;
  cohort: string;
}

interface Job {
  id: number,
  title: string;
  company: string;
  location: string;
  salary: string;
  post_time: number;
}


app.use(cors({
  origin: [
    'http://localhost:3000'
  ]
}))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

const students: Student[] = [
  {id: 1, name: 'Alex', status: 'LFJ', cohort: 'hk-map-08'},
  {id: 2, name: 'Gordon', status: 'LFJ', cohort: 'hk-map-07'},
  {id: 3, name: 'Michael', status: 'LFJ', cohort: 'hk-map-07'},
  {id: 4, name: 'Joan', status: 'LFJ', cohort: 'hk-map-09'},
  {id: 5, name: 'Andrew', status: 'LFJ', cohort: 'hk-map-09'},
  {id: 6, name: 'Jason', status: 'LFJ', cohort: 'hk-map-08'},
]

const jobs: Job[] = [
  {id: 1, title: 'Web Developer', company: 'Tricky Academy', location: 'Tsuen Wan', salary: 'HKD20000', post_time: 1589341677},
  {id: 2, title: 'App Developer', company: 'Tricky Academy', location: 'Tsuen Wan', salary: 'HKD20000', post_time: 1589343998},
  {id: 3, title: 'Software Enginner', company: 'HSBC', location: 'Olympic', salary: 'HKD25000', post_time: 1589341677},
  {id: 4, title: 'Java Programmer', company: 'PALO IT', location: 'Taikoo Place', salary: 'HKD26000', post_time: 1589341677},
]

interface StudentJob {
  student_id: number;
  job_id: number;
}

const students_jobs: StudentJob[] = [
]

app.get('/students', (req, res) => {
  res.json(students)
})

app.get('/jobs', (req, res) => {
  res.json(jobs)
})

app.get('/jobs/:id/students', (req, res) => {
  res.json(students_jobs.filter(student_job => student_job.job_id === parseInt(req.params.id)))
})

app.post('/jobs/:id/students', (req, res) => {
  students_jobs.push({
    student_id: parseInt(req.body.student_id),
    job_id: parseInt(req.params.id)
  })

  res.json(students_jobs.filter(student_job => student_job.job_id === parseInt(req.params.id)))
})

app.listen(8000, () => {
  console.log('listening on *:8000');
});
