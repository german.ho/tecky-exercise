import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchJobs } from '../redux/jobs/thunk';
import { RootState } from '../store';
import { push } from 'connected-react-router';
import moment from 'moment'

export function Jobs() {
  const dispatch = useDispatch();
  const jobIds = useSelector((state: RootState) => state.jobs.jobs)
  const jobs = useSelector((state: RootState) => jobIds.map(id => state.jobs.jobsById[id]))

  useEffect(() => {
    dispatch(fetchJobs())
  }, [dispatch])

  return (
    <div>
      <h2>Jobs</h2>
      <div>
        {jobs.map(job => (
          <div onClick={() => {
            // 選擇了這 job
            dispatch(push(`/mapping/${job.id}`))
          }} className="job">
            <p>{job.title}</p>
            <p>{job.company}</p>
            <p>{job.location}</p>
            <p>{job.salary}</p>
            <p>{moment.duration(moment(job.post_time, 'X').diff(moment())).humanize(true)}</p>
          </div>
        ))}
      </div>
    </div>
  )
}