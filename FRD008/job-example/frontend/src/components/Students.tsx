import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchStudents } from '../redux/students/thunk';
import { RootState } from '../store';
import { useRouteMatch } from 'react-router';
import { fetchEmailMapping, postEmailMapping } from '../redux/emailMapping/thunk';
import { loadEmailMapping } from '../redux/emailMapping/action';

export function Students() {
  const cohorts = useSelector((state: RootState) => state.cohorts.cohorts)
  const [selectedCohort, setSelectedCohort] = useState('')
  const studentIds = useSelector((state: RootState) => state.students.studentsByCohort[selectedCohort])
  const students = useSelector((state: RootState) => studentIds?.map(id => state.students.studentsById[id]))
  const dispatch = useDispatch();
  const match = useRouteMatch<{jobId?: string}>();
  const jobId = match.params.jobId
  
  useEffect(() => {
    dispatch(fetchStudents())
  }, [dispatch])

  return (
    <div>
      <h2>Students</h2>
      <select onChange={event => setSelectedCohort(event.currentTarget.value)}>
        <option></option>
        {cohorts.map(cohort => <option>{cohort.name}</option>)}
      </select>
      <div>
        {students?.map(student => (
          <div className="student" onClick={async () => {
            if (jobId == null) {
              return;
            }

            dispatch(postEmailMapping(parseInt(jobId), student.id))
          }}>
            <p>{student.name}</p>
            <p>{student.status}</p>
          </div>
        ))}
      </div>
    </div>
  )
}