import React, { useEffect } from 'react';
import { useRouteMatch } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { fetchEmailMapping } from '../redux/emailMapping/thunk';
import { RootState } from '../store';

export function EmailMapping() {
  const match = useRouteMatch<{jobId?: string}>();
  const dispatch = useDispatch();
  const jobId = match.params.jobId
  const selectedStudentIds = useSelector((state: RootState) => jobId == null ? undefined : state.emailMapping.studentsByJobId[jobId])
  const selectedStudents = useSelector((state: RootState) => selectedStudentIds?.map(id => state.students.studentsById[id]))
  
  useEffect(() => {
    if (jobId == null) {
      return;
    }
    dispatch(fetchEmailMapping(parseInt(jobId)))
  }, [jobId])

  return (
    <div>
      <h2>EmailMapping</h2>
      <div>
        {selectedStudents?.map(student => (
          <div className="student">
            <p>{student.name}</p>
            <p>{student.status}</p>
          </div>
        ))}

        <button onClick={() => {
          // console.log(selectedStudentIds)
        }}>APPLY</button>
      </div>
    </div>
  )
}