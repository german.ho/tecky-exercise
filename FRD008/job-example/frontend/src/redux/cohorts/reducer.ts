export function cohortsReducer() {
  return {
    cohorts: [
      {name: 'hk-map-07'},
      {name: 'hk-map-08'},
      {name: 'hk-map-09'},
      {name: 'hk-map-10'},
    ]
  }
}

export interface Cohort {
  name: string;
}

export interface CohortsState {
  cohorts: Cohort[];
}