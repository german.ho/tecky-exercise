import { Job } from "./reducer";

export function loadJobs(jobs: Job[]) {
  return {
    type: "@@JOBS/LOAD_JOBS" as "@@JOBS/LOAD_JOBS",
    jobs
  }
}

export type JobsActions = ReturnType<typeof loadJobs>;