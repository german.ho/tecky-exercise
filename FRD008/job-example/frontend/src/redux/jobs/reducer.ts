import produce from "immer";
import { JobsActions } from "./action";

export interface Job {
  id: number,
  title: string;
  company: string;
  location: string;
  salary: string;
  post_time: number;
}

export interface JobsState {
  jobsById: {
    [jobId: string]: Job
  },
  jobs: number[],
}

const initialState: JobsState = {
  jobsById: {},
  jobs: []
}

export function jobsReducer (state: JobsState = initialState, action: JobsActions): JobsState {
  return produce(state, state => {
    switch (action.type) {
      case "@@JOBS/LOAD_JOBS":
        for (const job of action.jobs) {
          state.jobsById[job.id] = job
        }
        state.jobs = action.jobs.map(job => job.id);
        break;
    }
  })
}