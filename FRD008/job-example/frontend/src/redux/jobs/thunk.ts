import { ThunkDispatch } from "../../store";
import { loadJobs } from "./action";

export function fetchJobs() {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/jobs`);
    const json = await res.json();

    dispatch(loadJobs(json))
  }
}