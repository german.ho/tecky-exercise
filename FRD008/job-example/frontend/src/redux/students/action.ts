import { Student } from "./reducer";

export function loadStudents(students: Student[]) {
  return {
    type: "@@STUDENTS/LOAD_STUDENTS" as "@@STUDENTS/LOAD_STUDENTS",
    students
  }
}

export type StudentsActions = ReturnType<typeof loadStudents>;