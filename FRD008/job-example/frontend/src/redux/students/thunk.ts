import { ThunkDispatch } from "../../store";
import { loadStudents } from "./action";

export function fetchStudents() {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/students`);
    const json = await res.json();

    dispatch(loadStudents(json))
  }
}