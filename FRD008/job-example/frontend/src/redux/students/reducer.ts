import produce from "immer";
import { StudentsActions } from "./action";

export interface Student {
  id: number,
  name: string;
  status: string;
  cohort: string;
}

export interface StudentsState {
  studentsById: {
    [studentId: string]: Student
  },
  studentsByCohort: {
    [cohort: string]: number[]
  }
}

const initialState: StudentsState = {
  studentsById: {},
  studentsByCohort: {}
}

export function studentsReducer (state: StudentsState = initialState, action: StudentsActions): StudentsState {
  return produce(state, state => {
    switch (action.type) {
      case "@@STUDENTS/LOAD_STUDENTS":
        for (const student of action.students) {
          state.studentsById[student.id] = student
          if (state.studentsByCohort[student.cohort] == null) {
            state.studentsByCohort[student.cohort] = [];
          }
          state.studentsByCohort[student.cohort].push(student.id)
        }
        break;
    }
  })
}