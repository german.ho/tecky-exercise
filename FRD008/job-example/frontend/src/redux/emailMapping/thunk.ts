import { ThunkDispatch } from "../../store";
import { loadEmailMapping } from "./action";
import { EmailMappingResult } from "./reducer";

export function fetchEmailMapping(jobId: number) {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/jobs/${jobId}/students`);
    const json: EmailMappingResult[] = await res.json(); 

    dispatch(loadEmailMapping(jobId, json.map(res => res.student_id)))
  }
}

export function postEmailMapping(jobId: number, studentId: number) {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/jobs/${jobId}/students`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        student_id: studentId
      })
    })
    const json: EmailMappingResult[] = await res.json(); 

    dispatch(loadEmailMapping(jobId, json.map(res => res.student_id)))
  }
}