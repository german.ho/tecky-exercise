export function loadEmailMapping(jobId: number, students: number[]) {
  return {
    type: "@@EMAIL_MAPPING/LOAD_EMAIL_MAPPING" as "@@EMAIL_MAPPING/LOAD_EMAIL_MAPPING",
    jobId,
    students
  }
}

export type EmailMappingActions = ReturnType<typeof loadEmailMapping>;