import React from 'react';
import { Students } from './components/Students';
import { Jobs } from './components/Jobs';
import { EmailMapping } from './components/EmailMapping';
import { Route } from 'react-router';
import { Link } from 'react-router-dom';

function App() {
  return (
    <div>
      <div>
        <Link to="/mapping">Mapping</Link>
      </div>
      <div className="App">
        <Route path="/mapping/:jobId?">
          <Students />
          <EmailMapping />
          <Jobs />
        </Route>
      </div>
    </div>
  );
}

export default App;
