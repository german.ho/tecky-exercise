import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from './store'
import { push } from 'connected-react-router';

export function Meetings() {
  const meetings = useSelector((state: RootState) => state.meetings.meetings)
  const dispatched = useDispatch();

  return (
    <div>
      {meetings.map(meeting => (
        <div className="meeting" onClick={() => dispatched(push(`/meetings/${meeting.id}`))}>
          {meeting.name}
        </div>
      ))}
    </div>
  )
  
}