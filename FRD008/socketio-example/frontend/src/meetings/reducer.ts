export interface Meeting {
  id: number;
  name: string;
}

export interface MeetingsState {
  meetings: Meeting[];
}

const initialState: MeetingsState = {
  meetings: [
    {
      id: 1,
      name: 'Tec Tec Tecky'
    },
    {
      id: 2,
      name: '導師真情剖白'
    }
  ]
}

export function meetingsReducer (state: MeetingsState = initialState, action: any): MeetingsState {
  return state
}