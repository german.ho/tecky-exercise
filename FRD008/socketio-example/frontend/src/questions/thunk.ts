import { ThunkDispatch } from "../store";
import { loadQuestions } from "./action";

export function fetchQuestions(meetingId: number) {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/questions?meetingId=${meetingId}`);
    const json = await res.json();
    
    dispatch(loadQuestions(json, meetingId));
  }
}