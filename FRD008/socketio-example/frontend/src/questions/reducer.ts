import { QuestionsActions } from "./action";
import produce from "immer"

export interface Question {
  id: number;
  question: string;
  vote: number;
  meeting_id: number;
}

export interface QuestionsState {
  questionsById: {
    [questionId: string]: Question,
  },
  questionsByMeeting: {
    [meetingId: string]: number[]
  }
}

const initialState: QuestionsState = {
  questionsById: {},
  questionsByMeeting: {}
}

export function questionsReducer(state: QuestionsState = initialState, action: QuestionsActions): QuestionsState {
  return produce(state, state => {
    switch (action.type) {
      case "@@QUESTIONS/LOAD_QUESTIONS":
        {
          for (const question of action.questions) {
            state.questionsById[question.id] = question;
          }
          state.questionsByMeeting[action.meetingId] = action.questions.map(question => question.id);
        }
        break;
      case "@@QUESTIONS/ADD_QUESTION":
        {
          state.questionsById[action.question.id] = action.question;
          if (state.questionsByMeeting[action.question.meeting_id] == null) {
            state.questionsByMeeting[action.question.meeting_id] = [];
          }
          state.questionsByMeeting[action.question.meeting_id].push(action.question.id)
        }
        break;
      case "@@QUESTIONS/UPDATE_QUESTION":
        {
          state.questionsById[action.question.id] = action.question;
        }
        break;
    }
  })
}