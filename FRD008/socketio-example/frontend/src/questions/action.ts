import { Question } from "./reducer";

export function loadQuestions(questions: Question[], meetingId: number) {
  return {
    type: "@@QUESTIONS/LOAD_QUESTIONS" as "@@QUESTIONS/LOAD_QUESTIONS",
    questions,
    meetingId
  }
}

export function addQuestion(question: Question) {
  return {
    type: "@@QUESTIONS/ADD_QUESTION" as "@@QUESTIONS/ADD_QUESTION",
    question
  }
}

export function updateQuestion(question: Question) {
  return {
    type: "@@QUESTIONS/UPDATE_QUESTION" as "@@QUESTIONS/UPDATE_QUESTION",
    question
  }
}

export type QuestionsActions =
  ReturnType<typeof loadQuestions> |
  ReturnType<typeof addQuestion> |
  ReturnType<typeof updateQuestion>;