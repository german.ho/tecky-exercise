import React, { useEffect } from 'react'
import { useFormState } from 'react-use-form-state'
import { useRouteMatch } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { fetchQuestions } from './questions/thunk';

import { RootState } from './store';
import { socket } from './socket';
import { addQuestion, updateQuestion } from './questions/action';
import { Question } from './questions/reducer';

export function MeetingRoom() {
  const [formState, {text}] = useFormState();
  const dispatch = useDispatch();
  const match = useRouteMatch<{id: string}>();
  const meetingId = match.params.id;
  const questionIds = useSelector((state: RootState) => state.questions.questionsByMeeting[meetingId])
  const questions = useSelector((state: RootState) => questionIds?.map(id => state.questions.questionsById[id]))

  useEffect(() => {
    dispatch(fetchQuestions(parseInt(meetingId)))
  }, [meetingId, dispatch]);

  useEffect(() => {
    socket.emit('join_meeting', meetingId);

    const newQuestionListener = (question: Question) => {
      dispatch(addQuestion(question))
    };
    socket.on('new_question', newQuestionListener)

    const updateQuestionListener = (question: Question) => {
      dispatch(updateQuestion(question))
    };
    socket.on('vote_question', updateQuestionListener)

    return () => {
      socket.off('new_question', newQuestionListener)
      socket.off('vote_question', updateQuestionListener)
      socket.emit('leave_meeting', meetingId);
    }
  }, [meetingId, dispatch]);

  return (
    <div>
      <form onSubmit={async event => {
        event.preventDefault();

        const question = formState.values.question;
        formState.reset();

        await fetch(`${process.env.REACT_APP_BACKEND_URL}/questions`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            question: question,
            meetingId: meetingId
          })
        })
      }}>
        <input {...text('question')} />
        <input type="submit" value="問問問" />
      </form>
      <div>
        {questions?.map(question => (
          <div className="question">
            {question.question} <button onClick={async () => {
              await fetch(`${process.env.REACT_APP_BACKEND_URL}/questions/${question.id}`, {
                method: 'PUT',
              })
            }}>(LIKE: {question.vote})</button>
          </div>
        ))}
      </div>
    </div>
  )
}