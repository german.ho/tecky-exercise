import React from 'react';
import { Route } from 'react-router';
import { Meetings } from './Meetings';
import { MeetingRoom } from './MeetingRoom';
import { Link } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <div>
        <Link to="/">Home</Link>
      </div>
      <Route path="/" exact><Meetings /></Route>
      <Route path="/meetings/:id"><MeetingRoom /></Route>
    </div>
  );
}

export default App;
