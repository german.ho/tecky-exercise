import express from 'express'
import http from 'http'
import SocketIO from 'socket.io'
import bodyParser from 'body-parser';
import cors from 'cors';

const app = express();
const server = http.createServer(app);
const io = SocketIO(server)

app.use(cors({
  origin: [
    'http://localhost:3000'
  ]
}))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

interface Question {
  id: number;
  question: string;
  vote: number;
  meeting_id: number;
}

const questions: Question[] = [];

// GET /questions?meeting_id=1
app.get('/questions', (req, res) => {
  res.json(questions.filter(question => question.meeting_id === parseInt(req.query.meetingId + "")));
})

// POST /questions
app.post('/questions', (req, res) => {
  const question = {
    id: questions.length + 1,
    question: req.body.question,
    vote: 0,
    meeting_id: parseInt(req.body.meetingId)
  };

  questions.push(question)

  io.to(`meeting:${question.meeting_id}`).emit('new_question', question);

  res.json({success: true});
})

app.put('/questions/:id', (req, res) => {
  const question = questions.find(question => question.id === parseInt(req.params.id));

  if (question == null) {
    res.status(400).json({result: "question_not_found"})
    return;
  }

  question.vote += 1;

  io.to(`meeting:${question.meeting_id}`).emit('vote_question', question);

  res.json({success: true});
})

io.on('connection', socket => {
  socket.on('join_meeting', (meetingId: number) => {
    socket.join('meeting:' + meetingId)
  })

  socket.on('leave_meeting', (meetingId: number) => {
    socket.leave('meeting:' + meetingId)
  })
});

server.listen(8000, () => {
  console.log('listening on *:8000');
});
