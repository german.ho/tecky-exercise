function reverseString (str){
    let words = []
    for (let x = 0; x < 4; x++){
        console.log(str[x])
    }
    for (let x = 4; x >= 0 ; x--){
        words.push(str[x])
    }
    console.log(words.join(""))

} reverseString("cool")


function rnaTranscription(input) {
	let result = "";
	for (let dna of input) {
		switch (dna) {
			case 'G':
				result += 'C';
				break;
			case 'C':
				result += 'G';
				break;
			case 'T':
				result += 'A';
				break;
			case 'A':
				result += 'U';
				break;
		}
	}
	console.log(result);
	return result;
}

rnaTranscription("GCTAGCTJJJJJ")
