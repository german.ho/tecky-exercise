function firstExercise (question){
    if (question == "ABC"){
        return "Hello World"
    } else {
        return "No thanks"
    }
}

function secondExercise (question){
    if (question == "XYZ"){
        return "footer"
    } else {
        return "Thanks very Much"
    }
}

module.exports = {
someObject: firstExercise,
someFunction: secondExercise
}