import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import multer from 'multer'

const app = express()

app.use(cors({
  origin: [
    'http://localhost:3000',
    'http://whatthefook.ga'
  ]
}))
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

interface Todo {
  id: number;
  item: string;
  completed: boolean;
  projectId: number;
}

const todos: Todo[] = []
const user = {
  name: '',
  profile: ''
}

const upload = multer({
  dest: 'uploads/'
})

app.use('/uploads', express.static('uploads'))

app.get('/todos', (req, res) => {
  res.json(todos.filter(todo => todo.projectId == parseInt(req.query.projectId + "")))
})

app.post('/todos', (req, res) => {
  todos.push({
    id: todos.length + 1,
    item: req.body.item,
    completed: false,
    projectId: parseInt(req.body.projectId)
  })

  res.json({success: true})
})

app.put('/todos/:id', (req, res) => {
  const todo = todos.find(todo => todo.id == parseInt(req.params.id))
  if (todo == null) {
    res.status(400).json({msg: '好似冇呢個 todo 喎'})
    return;
  }

  todo.completed = req.body.completed == 'true';

  res.json({completed: todo.completed})
})

app.get('/user', (req, res) => {
  res.json(user)
});

app.post('/user', upload.single('photo'), (req, res) => {
  user.name = req.body.name
  user.profile = req.file.filename

  res.json({success: true})
})

const port = process.env.PORT || 8000
app.listen(port, () => {
  console.log('Listening on port ' + port)
})