import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { memoReducer, MemosState } from './memos/reducer';
import { RouterState, connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { UserState, userReducer } from './user/reducer';
export const history = createBrowserHistory();

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

export interface RootState {
  memos: MemosState,
  user: UserState,
  router: RouterState
}

const reducer = combineReducers<RootState>({
  memos: memoReducer,
  user: userReducer,
  router: connectRouter(history)
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(reducer,
  composeEnhancers(
      applyMiddleware(routerMiddleware(history))
  ));