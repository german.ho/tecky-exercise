import { UserActions } from "./action";

export enum Role {
  ADMIN = 'admin',
  GUEST = 'guest'
}

export interface UserState {
  role: Role;
}

const initialState: UserState = {
  role: Role.GUEST
}

export const userReducer = (oldState: UserState = initialState, action: UserActions): UserState => {
  switch (action.type) {
    case "@@USER/CHANGE_ROLE":
      return {
        ...oldState,
        role: action.role
      }
  }
  return oldState;
}