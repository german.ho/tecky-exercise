import React, { useState, useCallback } from 'react';
import { useFormState } from 'react-use-form-state';
import { useDispatch } from 'react-redux';
import { loadMemo } from './memos/action';

export const CreateMemo = () => {
  const [formState, { textarea }] = useFormState()
  const [photo, setPhoto] = useState<undefined | File>(undefined);
  const [submitting, setSubmitting] = useState(false);

  const dispatch = useDispatch();

  const readMemos = useCallback(async function () {
    const fetchRes = await fetch('http://localhost:8080/memos'); // GET + 'memos'
    const memos = await fetchRes.json();

    dispatch(loadMemo(memos));
  }, []);

  return (
    <form onSubmit={async event => {
      event.preventDefault();

      setSubmitting(true)
      setTimeout(() => {
        setSubmitting(false)
      }, 5000);

      const formData = new FormData();
      formData.append('content', formState.values.content);
      if (photo != null) {
        formData.append('photo', photo)
      }

      formState.reset();
    
      await fetch('http://localhost:8080/memos', {
        method: 'POST',
        body: formData  
      })  

      readMemos();
    }}>
      <textarea { ...textarea('content') }></textarea>
      <input type="file" accept="image/*" onChange={event => setPhoto(event.currentTarget.files?.[0])} />
      <input disabled={submitting} type="submit" value="貼" />
    </form>
  )
}