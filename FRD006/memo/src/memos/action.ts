import { Memo } from "./reducer";

export function loadMemo(memos: Memo[]) {
  return {
    type: "@@MEMOS/LOADED_MEMO" as  "@@MEMOS/LOADED_MEMO",
    memos
  };
}

export function editMemo(i: number, content: string) {
  return {
    type: "@@MEMOS/EDIT_MEMO" as  "@@MEMOS/EDIT_MEMO",
    i,
    content
  };
}

export type MemosActions = ReturnType<typeof loadMemo> | ReturnType<typeof editMemo>;