export interface Project {
  id: number;
  name: string;
}

const initialState: Project[] = [
  {id: 1, name: 'SimCountry'},
  {id: 2, name: '過三關'},
  {id: 3, name: '過四關'},
  {id: 4, name: '過五關'},
  {id: 5, name: '過六關'}
];

export const projectsReducer = (oldState: Project[] = initialState, action: any) => {
  return oldState;
}