import React, { useState, useEffect } from 'react'
import { useFormState } from 'react-use-form-state'
import { useDispatch } from 'react-redux';
import { setName } from './user/action';

function Profile() {
  const [formState, { text }] = useFormState();
  const [file, setFile] = useState<File | undefined>(undefined);
  const [showModal, setShowModal] = useState(false)
  const dispatch = useDispatch();

  useEffect(() => {
    async function load() {
      const res = await fetch('http://localhost:8000/user');
      const json = await res.json();

      dispatch(setName(json.name))
    }

    load();
  }, [dispatch])

  return (
    <>
      <h2>Profile</h2>
      <form onSubmit={async (event) => {
        event.preventDefault();

        const formData = new FormData();
        formData.append('name', formState.values.name)
        if (file) {
          formData.append('photo', file);
        }

        await fetch('http://localhost:8000/user', {
          method: 'POST',
          body: formData
        })

        dispatch(setName(formState.values.name))
        setShowModal(true)
      }}>
        <div>名：<input {...text('name')} /></div>
        <div>相：<input type="file" onChange={event => setFile(event.currentTarget.files?.[0])} /></div>
        <div><input type="submit" value="更新" /></div>
      </form>
      <div className="modal" data-show={showModal} onClick={() => setShowModal(false)}>
        <div className="modal-content" onClick={(event) => event.stopPropagation()}>
          更新咗喇！！！
          <button onClick={() => setShowModal(false)}>X</button>
        </div>
      </div>
    </>
  )
}

export default Profile