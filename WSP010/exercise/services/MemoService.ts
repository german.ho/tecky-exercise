import path from 'path'
import jsonfile from 'jsonfile';

export class MemoService {
	private memosJsonPath: string;

	constructor() {
		this.memosJsonPath = path.join(__dirname, "../memos.json");
	}

	async getMemos() {
		const memos = await jsonfile.readFile(this.memosJsonPath);
		return memos;
	}

	// async updateMemoV2(id: number, memoOpt: { content?:string, filename?:string }) {
	// 	const memos = await this.getMemos();
	// 	const memo = memos[id];
	// 	Object.assign(memo, memoOpt);
	// 	await jsonfile.writeFile(this.memosJsonPath, memos);
	// }

	async updateMemo(id: number, content: string) {
		const memos = await this.getMemos();
		memos[id].content = content;
		await jsonfile.writeFile(this.memosJsonPath, memos);
	}

	async updateMemoWithImage(id: number, filename: string) {
		const memos = await this.getMemos();
		memos[id].image = filename;
		await jsonfile.writeFile(this.memosJsonPath, memos);
	}

}