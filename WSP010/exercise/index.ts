import express from 'express'
import bodyParser from 'body-parser'
import path from 'path'
import multer from 'multer'
import jsonfile from 'jsonfile'
import expressSession from 'express-session';

import http from 'http';
import socketIO from 'socket.io';

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, 'uploads'));
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
})

export const upload = multer({storage: storage})

const app = express();
const server = new http.Server(app);
const io = socketIO(server);

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const sessionMiddleware = expressSession({
  secret: "Tecky Academy teaches typescript",
  resave: true,
  saveUninitialized: true,
  cookie: { secure: false }
});

app.use(sessionMiddleware);

io.use((socket, next) => {
  sessionMiddleware(socket.request, socket.request.res, next);
});

io.on("connection", function(socket) {
  socket.request.session.socketId = socket.id;
  socket.request.session.save();

  socket.on("disconnect", () => {
    socket.request.session.socketId = null;
    socket.request.session.save();
  });
});


import {MemoService} from './services/MemoService';
import {MemoRouter} from './routers/MemoRouter';

const memoService = new MemoService();
const memoRouter = new MemoRouter(memoService, io);

const API_VERSION = "/api/v1"
app.use(`${API_VERSION}/memos`, memoRouter.router());

app.get('/currentUser', async (req, res) => {
  if (req.session && req.session.isAdmin) {
    res.json({role: 'admin'})
  } else {
    res.json({role: 'guest'})
  }
});

app.post('/login', (req, res) => {
  if (req.body.username === 'alex' && req.body.password === '1234') {
    if (req.session) {
      req.session.isAdmin = true;
    }
    res.json({success: true})
  } else {
    res.json({success: false})
  }
})

app.delete('/memos/:id/image', async (req, res) => {
  try {
    if (!(req.session && req.session.isAdmin)) {
      res.status(401).json({success: false, message: 'unauthorized'})
      return;
    }
    const memos = await jsonfile.readFile('./memos.json')

    memos[req.params.id].image = null;

    await jsonfile.writeFile('./memos.json', memos);

    res.json({success: true})
  } catch (e) {
    console.error(e);
    res.status(500).json({success: false});
  }
})

app.delete('/memos/:id', async (req, res) => {
  try {
    if (!(req.session && req.session.isAdmin)) {
      res.status(401).json({success: false, message: 'unauthorized'})
      return;
    }
    const memos = await jsonfile.readFile('./memos.json')

    memos.splice(req.params.id, 1)

    await jsonfile.writeFile('./memos.json', memos);

    res.json({success: true})
  } catch (e) {
    console.error(e);
    res.status(500).json({success: false});
  }
})

app.post('/logout', (req, res) => {
  if (req.session) {
    req.session.destroy(() => {

    });
  }
  res.redirect('/')
})

app.post('/memos', upload.single('photo'), async (req, res) => {
  // 寫入 json file 
  try {
    if (req.body.content == null || req.body.content == '') {
      if (!(req.session && req.session.isAdmin)) {
        res.status(400).json({success: false, message: '無字天書唔接受!'})
        return;
      }
    }

    if (req.session) {
      if (req.session.submission == null) {
        req.session.submission = 0;
      }
      req.session.submission = req.session.submission + 1;

      if (req.session.submission > 5) {
        res.status(400).json({success: false, message: '每人最多寫五次!'})
        return;
      }
    }

    try {
      const memos = await jsonfile.readFile('./memos.json')
    
      memos.push({
        content: req.body.content,
        image: req.file == null ? null : req.file.filename
      })
    
      await jsonfile.writeFile('./memos.json', memos);
    } catch {
      await jsonfile.writeFile('./memos.json', [{
        content: req.body.content,
        image: req.file == null ? null : req.file.filename
      }]);
    }

    res.redirect('/')
    // res.json({msg: "test"});
  } catch (e) {
    console.error(e);
    res.status(500).json({success: false});
  }
})

// 增強 express 的功能
// Middleware (未講)
app.use(/* 放加強器 */ express.static(path.join(__dirname, 'public')) )
app.use(/* 放加強器 */ express.static(path.join(__dirname, 'uploads')) )

server.listen(8080, () => {
  console.log('Listening on port 8080')
});