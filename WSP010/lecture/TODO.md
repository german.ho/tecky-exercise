# Create a TS Express Project

- [x] create project folder

  ```Bash
  mkdir <project-name>
  ```

- [x] get into the folder

  ```Bash
  cd <project-name>
  ```

- [x] init project

  ```Bash
  npm init -y
  ```

- [x] install basic packages

  ```Bash
  npm install ts-node typescript @types/node
  ```

- [x] setup `tsconfig.json`

  ```Json
  {
    "compilerOptions": {
        "module": "commonjs",
        "target": "es5",
        "lib": ["es6", "dom"],
        "sourceMap": true,
        "allowJs": true,
        "jsx": "react",
        "esModuleInterop":true,
        "moduleResolution": "node",
        "noImplicitReturns": true,
        "noImplicitThis": true,
        "noImplicitAny": true,
        "strictNullChecks": true,
        "suppressImplicitAnyIndexErrors": true,
        "noUnusedLocals": true
    },
    "exclude": [
        "node_modules",
        "build",
        "scripts",
        "index.js"
    ]
  }
  ```

- [x] config `index.js`

  ```Javascript
  require('ts-node/register');
  require('./main');
  ```

- [x] create `main.ts`

- [x] testing on `main.ts`

- [x] install express packages

  ```Bash
  npm install express @types/express
  ```

- [x] express template

  ```Typescript
  import * as express from 'express';
  import {Request,Response} from 'express';

  const app = express();

  app.get('/',function(req:Request,res:Response){
      res.end("Hello World");
  })

  const PORT = 8080;

  app.listen(PORT, () => {
      console.log(`Listening at http://localhost:${PORT}/`);
  });
  ```

- [x] testing on express template

- [x] install bodyParser

  ```Bash
  npm install body-parser @types/body-parser
  ```

- [x] import bodyParser and test

  **`main.ts`:**

  ```Typescript
  import bodyParser from 'body-parser';

  const app = express();
  app.use(bodyParser.urlencoded({extended:true}));
  app.use(bodyParser.json());
  ```

- [ ] Design Router, Service
  - [ ] User Router, Service
  - [ ] Memo Router, Service
