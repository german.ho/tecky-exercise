export interface IUser {
	name: string;
	age: number;
}

export type User = {
	name: string;
	age: number;
}