window.onload = () => {

	console.log("hello");
	const socket = io.connect("localhost:8080");

	socket.on("jason",(msg)=>{
    console.log(msg);
	});

	// socket.emit("students", "hello, students");

	socket.on('get-students',(students)=>{
		// DOM Logic to populate students here
		console.log(students);
	})

	document.getElementById("test-button").addEventListener("click", async() => {
		await test();
		// socket.emit("students", "hello, students");
	})
}

const test = async () => {
	const res = await fetch("/memos/testing");
	console.log(res.status);
};