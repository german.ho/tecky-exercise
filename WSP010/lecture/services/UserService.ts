// import {IUser} from '../models/User';

export class UserService {
  private users: {
		// userId?
		name: string;
		password: string;
    age: number;
	}[];

  constructor() {
    this.users = [{
			name: 'jason',
			password: '1234',
			age: 18
		}];
	}

  getUsers() {
    return this.users.map(user => ({
			name: user.name,
			age: user.age,
		}));
		// return this.users;
	}

	findUserByName(name: string) {
		const user = this.users.find(user => user.name === name);
		return user;
	}

	createUser(name: string, age: number, password: string) {
		this.users.push({
			name, age, password
		});
	}
}

// export const jason = "Jason";