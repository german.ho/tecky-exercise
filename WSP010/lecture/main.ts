import express from "express";
import bodyParser from "body-parser";
import http from "http";
import socketIO from "socket.io";
import path from "path";
import expressSession from "express-session";

const app = express();
const server = new http.Server(app);
const io = socketIO(server);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const sessionMiddleware = expressSession({
  secret: "Tecky Academy teaches typescript",
  resave: true,
  saveUninitialized: true,
  cookie: { secure: false }
});

app.use(sessionMiddleware);

io.use((socket, next) => {
  sessionMiddleware(socket.request, socket.request.res, next);
});

io.on("connection", function(socket) {
  socket.request.session.socketId = socket.id;
  socket.request.session.save();

  socket.on("students", (payload: string) => {
    io.emit("get-students", payload);
  })

  socket.on("disconnect", () => {
    socket.request.session.socketId = null;
    socket.request.session.save();
  });
});

import { UserService } from "./services/UserService";
import { UserRouter } from "./routers/UserRouter";
import { MemoService } from "./services/MemoService";
import { MemoRouter } from "./routers/MemoRouter";

// method 1
const userService = new UserService();
const userRouter = new UserRouter(userService);

// method 2
const memoService = new MemoService();

app.use("/users", userRouter.router());
app.use("/memos", new MemoRouter(memoService, io).router());

app.use(express.static(path.join(__dirname, "public")));

const PORT = 8080;

server.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});
