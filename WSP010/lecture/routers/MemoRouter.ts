import express from "express";
import { Request, Response } from "express";
import socketIO from "socket.io";
import { MemoService } from '../services/MemoService';

export class MemoRouter {
  constructor(private memoService: MemoService, private io: socketIO.Server){}

  // Pattern: /memos/test Method: GET
  router() {
    const router = express.Router();
    router.get("/", this.get);
    router.get("/testing", (req: Request, res: Response) => {
      if(req.session){
        console.log("in testing");
        console.log(req.session);
        this.io.to(req.session.socketId).emit("jason","Congratulations! Jason!");
      }
      res.end();
    });
		// router.post("/", this.post);
		// router.put("/", this.updateMemo);
		// router.delete("/", this.deleteMemo);
    return router;
  }

  get = (req: Request, res: Response) => {
    try {
      const memos = this.memoService.getMemos();
      res.json({ memos });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: "internal error" });
    }
  };
}
