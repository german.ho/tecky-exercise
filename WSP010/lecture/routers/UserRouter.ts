import express from "express";
import { Request, Response } from "express";
// import UserService, { jason } from '../services/UserService';
import { UserService } from '../services/UserService';

export class UserRouter {
  constructor(private userService: UserService){}

  router() {
    const router = express.Router();
    router.get("/", this.get);
    router.post("/", this.post);
    return router;
  }

  get = (req: Request, res: Response) => {
		const users = this.userService.getUsers();
    res.json({ users });
  };

  post = (req: Request, res: Response) => {
		const { name, age, password } = req.body;
		const ageNUm = parseInt(age);
		if (isNaN(ageNUm)) {
			res.status(400).json({ message: "invalid age" });
			return;
		}
		const user = this.userService.findUserByName(name);
		if (user) {
			res.status(400).json({ message: "duplicated user" });
			return;
		}
		this.userService.createUser(name, ageNUm, password);
    res.json({ message: "success" });
  };
}
