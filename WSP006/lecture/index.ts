import { createQuestionPromise } from './readline'

// 井字過兩關
// 井字過三關

abstract class TicTacToe {
  protected currentPlayer: 'x' | 'o' = 'x';

  // template method
  // vvv
  turn = async () => {
    // ask current player which box to put
    console.log('開始 turn')
    let box = null;
    let valid = true;
    do {
      box = await createQuestionPromise(`玩家 ${this.currentPlayer} 😛 想放邊格啊? `, false)
      valid = true;

      if (!this.checkBoxValid(box)) {
        // check box valid
        console.log('好似唔係格黎架喎')
        valid = false
      } else if (this.checkBoxOccuiped(box)) {
        // check box occuiped
        console.log('好似被人霸咗喇喎...')
        valid = false
      }

    } while (!valid);

    // place mark
    this.placeMark(box);

    // show board
    this.showBoard();

    // check winning condition
    const winner = this.checkWinner();

    // check draw condition
    const isDraw = this.isDraw();

    // if not win and not draw 
    if (winner == null && !isDraw) {
      // => go to next turn
      this.currentPlayer = (this.currentPlayer == 'x' ? 'o' : 'x')
      this.turn()
    }
    // else
    else {
      // => announce win or draw
      if (winner) {
        console.log(`恭喜 ${winner} 勝出`);
      } else {
        console.log(`邊個贏？無人贏，無人贏`);
      }
    }
  }

  abstract checkBoxValid(box: string): boolean;
  abstract checkBoxOccuiped(box: string): boolean;
  abstract placeMark(box: string): void;
  abstract checkWinner(): 'x' | 'o' | null;
  abstract isDraw(): boolean;
  abstract showBoard(): void;
}

class 過兩關 extends TicTacToe {
  protected board: ('x' | 'o' | null)[] = [null, null, null, null];

  // [0, 1,
  //  2, 3]

  private winningConditions = [
    [0, 1],
    [0, 2],
    [1, 2],
    [1, 3],
    [2, 3]
  ]

  checkBoxValid(box: string): boolean {
    const boxNumber = parseInt(box);
    if (boxNumber >= 1 && boxNumber <= 4) {
      return true;
    } else {
      return false;
    }
  }
  checkBoxOccuiped(box: string): boolean {
    const boxNumber = parseInt(box);
    return this.board[boxNumber - 1] != null;
  }
  placeMark(box: string): void {
    const boxNumber = parseInt(box);
    this.board[boxNumber - 1] = this.currentPlayer
  }
  checkWinner(): "x" | "o" | null {
    for (const winningCondition of this.winningConditions) {
      if (this.board[winningCondition[0]] == this.board[winningCondition[1]] &&
          this.board[winningCondition[0]] != null) {
            return this.board[winningCondition[0]]
      }
    }
    return null;
  }
  isDraw(): boolean { 
    for (const box of this.board) {
      if (box == null) {
        return false;
      }
    }

    return true;
  }
  showBoard() {
    console.log(`${this.board[0] || ' '} | ${this.board[1] || ' '} `)
    console.log(`----- `)
    console.log(`${this.board[2] || ' '} | ${this.board[3] || ' '} `)
  }
}

const game = new 過兩關();
game.turn();