const numbers1 = [0, 2, 4, 5, 6, 8]
const numbers2 = [1, 3, 5, 7, 9]

function checker除3(num /*一個數*/) {
  if (num % 3 == 0) {
    return true;
  } else {
    return false;
  }
}

// input: 俾個 array
// output: 俾個答案你
function filtering(入口para, checker) {
  const result = []
  for (const num of 入口para) {
    if (checker(num) == true) {
      result.push(num)
    }
  }
  return result;
}

console.log(filtering(numbers1, checker除3))
console.log(filtering(numbers2, (num) => num % 2 == 0))