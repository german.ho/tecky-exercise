import * as readline from 'readline';

const readLineInterface = readline.createInterface({
    input: process.stdin, // standard input
    output: process.stdout // standard output | standard error
})

export function createQuestionPromise(question: string, closeInterface: boolean = true) {
  return new Promise<string>((resolve, reject) => {
    readLineInterface.question(question, (answer:string)=>{
      resolve(answer);
      if (closeInterface) {
        readLineInterface.close();
      }
    });
  })
}
