import express from 'express'
import path from 'path'

const app = express()
app.use(express.static(path.join(__dirname, 'public')))
app.listen(8080)

//
//        host 主機 (IP/Domain)   
//        vvvvvvvvv     vvvvvvvvvvvvv path 路徑
// http://localhost:8080/secret
// ^^^^             ^^^^
// protocol         port
// 協定              連接埠

//                Request  Response
//       path      請求    回應
// app.get('/secret', (req,   res) => {
//   res.end(`<html><link rel="stylesheet" href="style.css"><img src="kinko.png"></html>`)
// })

// app.get('/style.css', (req, res) => {
//   res.header('Content-Type', 'text/css');
//   res.end('body {background: black; color: white;}')
// })

// app.get('/kinko.png', (req, res) => {
//   res.sendFile(path.join(__dirname, 'kinko.png'));
// });

// let i = 0;

// //                Request  Response
// //       path      請求    回應
// app.get('/',       (req,   res) => {
//   i++
//   res.end('Sorry. We are closed. You are the ' + i + 'th guy comes here so far')
// })


