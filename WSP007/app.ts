import express from 'express'
import path from 'path'
import bodyParser from 'body-parser'
import multer from 'multer'
import jsonfile from 'jsonfile'
import expressSession from 'express-session';

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(__dirname, 'uploads'));
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
  })
const upload = multer({storage})

const app = express()

app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static(path.join(__dirname, 'uploads')))
app.use(bodyParser.urlencoded())

app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true
}));


app.get('/memos', async (req,res)=>{
    try{
    const memos = await jsonfile.readFile('./memos.json')
    res.json (memos)
    } catch (err){
        console.error(err)
        res.status(500).json({result: false})
    }

})

app.post('/login',(req, res)=>{
    if (req.body.username === "german" && req.body.password === "123456"){
        if (req.session){
            req.session.isAdmin = true 
        }
        res.json({success: true})
    } else{
        res.status(400).json({success: false})
    }
})

app.delete('/memos/:id', async(req, res)=>{
    try{const memos = await jsonfile.readFile('./memos.json')

    memos.splice(req.params.id, 1)

    await jsonfile.writeFile('./memos.json', memos)

    res.json({success: true})
    } catch (err){
    console.error(err)
    res.status(500).json({success: false})
}
})

app.post('/memos', upload.single('photo'), async (req,res)=>{

    if (req.body.content == "null" || req.body.content == ''){
        res.status(400).json({success: false, message:'文宣唔可以無字'})
        return;
    }

    if (req.session){
        if (req.session.submission == null){
            req.session.submission = 0
        }
        req.session.submission = req.session.submission + 1

        if (req.session.submission > 5){
            res.status(400).json({success:false, message:'每人只可以貼最多5張文宣'})
            return;
        }
    }
 
    try{const memos = await jsonfile.readFile('./memos.json')

    memos.push({
        content:req.body.content,
        image: req.file == null? null: req.file.filename
    })

    await jsonfile.writeFile('./memos.json', memos)

    res.redirect('/')
    } catch(err){
        console.error(err)
        res.status(500).json({success:false});
    }
    
})

app.listen(8080, ()=>{
    console.log('listening on port 8080')
})