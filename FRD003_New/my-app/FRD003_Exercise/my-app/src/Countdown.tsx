import React, { useState, useEffect } from 'react'
import { useFormState } from 'react-use-form-state';
import { formatSecond } from './utils'

function Countdown() {
  const [formState, { select }] = useFormState({
    hour: '0',
    minute: '0',
    second: '0'
  })
  const [count, setCount] = useState(0)
  const [start, setStart] = useState(false)
  const [counting, setCounting] = useState(false)
  const [ring, setRing] = useState(false)

  useEffect(() => {
    if (counting) {
      const timer = setInterval(() => {
        setCount(count => count + 16)
      }, 16);

      return () => {
        clearInterval(timer)
      }
    }
  }, [counting]);

  const totalSecond = (parseInt(formState.values.hour) * 60 * 60 + 
                       parseInt(formState.values.minute) * 60 + 
                       parseInt(formState.values.second)) * 1000;

  useEffect(() => {
    if (totalSecond < count) {
      setCounting(false);
      setStart(false);
      setRing(true)
    }
  }, [count, totalSecond]);

  return <div>
    { ring && <audio src="/ringtone.mp3"  autoPlay /> }
    {
      !start && <>
      <select {...select('hour',)}>
        {Array(24).fill('乜都得').map((_, i) => <option>{i}</option>)}
      </select> 小時
      <select {...select('minute')}>
        {Array(60).fill('乜都得').map((_, i) => <option>{i}</option>)}
      </select> 分
      <select {...select('second')}>
        {Array(60).fill('乜都得').map((_, i) => <option>{i}</option>)}
      </select> 秒
      <button onClick={() => {
        setStart(true);
        setCounting(true);
        setRing(false)
        setCount(0)
       }}>開始</button>
      </>
    }
    { start && <>
        <div>{formatSecond(totalSecond - count)}</div>
        <button onClick={() => {
          setStart(false);
          setCounting(false);
        }}>
          取消
        </button>
        <button onClick={() => {
          setCounting(counting => !counting);
        }}>
          {counting && '暫停'}
          {!counting && '繼續'}
        </button>
      </>
    }
  </div>
}

export default Countdown;
