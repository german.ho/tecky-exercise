export const formatSecond = (ms: number) => {
    const min = Math.floor(ms / 60 / 1000);
    const sec = Math.floor((ms - min * 60 * 1000) / 1000);
    const msec = ms % 1000;
  
    return min + ':' + sec + '.' + msec
  }
  