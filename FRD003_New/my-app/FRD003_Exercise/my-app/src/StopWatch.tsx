import React, { useState, useEffect } from 'react'
import { formatSecond } from './utils'

const StopWatch = () => {
  const [count, setCount] = useState(0)
  const [start, setStart] = useState(false)
  const [laps, setLaps] = useState<number[]>([])

  useEffect(() => {
    if (start) {
      const timer = setInterval(() => {
        setCount(count => count + 16)
      }, 16);

      return () => {
        clearInterval(timer)
      }
    }

  }, [start]);

  return (
    <div className="stopwatch">
      <div className="time">
        {formatSecond(count)}
      </div>
      <div className="buttons">
        { start &&
          <button onClick={() => setLaps(laps => laps.concat([count]))}>
            Lap
          </button> }
        { !start &&
          <button onClick={() => {
            setCount(0)
            setLaps([])
          }}>
            Reset
          </button> }
        <button onClick={() => setStart(start => !start)}>
          {start && 'Pause'}
          {!start && 'Start'}
        </button>
      </div>
      <div className="laps">
        {laps.map((lap, i) => (
          <section>
            <div>Lap #{i + 1}</div>
            <div>{i == 0 ? formatSecond(lap) : formatSecond(lap - laps[i - 1])}</div>
          </section>
        ))}
      </div>
    </div>
  )
}

export default StopWatch;
