import React, { useState } from 'react';
import StopWatch from './StopWatch';
import Countdown from './Countdown';

function App() {
  const [page, setPage] = useState('countdown');

  return (
    <div className="App">
      <div>
        <button onClick={() => setPage('stopwatch')}>Stopwatch</button>
        <button onClick={() => setPage('countdown')}>Countdown</button>
      </div>
      <div>
        { page === 'stopwatch' && <StopWatch /> }
        { page === 'countdown' && <Countdown /> }
      </div>
    </div>
  );
}

export default App;
