import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import Countdown from './Countdown';
import FormV2 from './FormV2';

function App() {
  const [show, setShow] = useState(false);
  // const [count, setCount] = useState(0);

  // componentDidMount

  // LIFECYCLE!!!!!

  // useEffect(() => {
  //   setTimeout(() => {
  //     setShow(!show);
  //   }, 1000);
  // }, [])

  // useEffect(() => {
  //   setTimeout(() => {
  //     setShow(!show);
  //   }, 1000);
  // }, [count])

  return (
    <div className="App">
      <FormV2 />
      <header className="App-header">
        { show && <img src={logo} className="App-logo" alt="logo" /> }
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        { show && <Countdown></Countdown> }
        <button onClick={() => {
          setShow(show => !show)
          // fetch(...) // 監視有冇人禁開關
        } }>開/關</button>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
