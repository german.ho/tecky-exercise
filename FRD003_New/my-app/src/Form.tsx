import React, { useState } from 'react'

const Form = () => {
  const [name, setName] = useState('')
  const [gender, setGender] = useState('男')
  const [mingNgMing, setMingNgMing] = useState('明')

  return (
    <div>
      <div>姓名：
        <input 
          type="text" 
          value={name} 
          onChange={event => setName(event.currentTarget.value)} />
      </div>
      <div>性別： <select multiple value={gender} onChange={event => setGender(event.currentTarget.value)}>
        <option>男</option>
        <option>女</option>
        <option>未知</option>
      </select></div>
      <div>遺言： <textarea /></div>
      <div>明唔明： 
        <label><input type="radio" name="mingNgMing" checked={mingNgMing === '明'} onChange={event => event.currentTarget.checked && setMingNgMing('明')} />明 </label>
        <label><input type="radio" name="mingNgMing" checked={mingNgMing === '唔明'} 
        onChange={event => event.currentTarget.checked && setMingNgMing('唔明')} />唔明</label> </div>
      
      <button onClick={() => {
        alert(JSON.stringify(name, null, 2))
      }}>遞交</button>
    </div>
  )
}

export default Form