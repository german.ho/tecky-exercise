import React from 'react'
import { useFormState } from 'react-use-form-state';

const FormV2 = () => {
  const [formState, { text, textarea, selectMultiple, radio }] = useFormState();
  
  return (
    <div>
      <div>姓名：
        <input { ...text('name') } />
        { formState.touched.name && formState.values.name == '' && <div>喂，大佬啊，我都要你乜名架</div>}
      </div>
      <div>性別： <select { ...selectMultiple('gender') } >
        <option>男</option>
        <option>女</option>
        <option>未知</option>
      </select></div>
      <div>遺言： <textarea {...textarea('comment')} /></div>
      <div>明唔明： 
        <label><input {...radio('mingNgMing', '明')} />明 </label>
        <label><input {...radio('mingNgMing', '唔明')} />唔明</label> </div>
      
      <button onClick={() => {
        fetch('https://tecky.io/', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(formState)
        })
      }}>遞交</button>
    </div>
  )
}

export default FormV2;