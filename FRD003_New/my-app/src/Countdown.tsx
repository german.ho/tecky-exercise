import React, { useState, useEffect } from 'react'

function Countdown() {
  const [remaining, setRemaining] = useState(100);

  // CORRECT
  useEffect(() => {
    // mount
    const timer = setInterval(() => {
      console.log('行緊喇div')
      console.log(remaining);
      setRemaining((oldValue) => {
        return oldValue - 1
      });
    }, 500);

    return () => {
      // unmount
      clearInterval(timer)
    }
  }, [])

  // WRONG
  // useEffect(() => {
  //   setInterval(() => {
  //     setRemaining(remaining - 1);
  //   }, 500);
  // }, [])

  // // WRONG
  // useEffect(() => {
  //   setInterval(() => {
  //     setRemaining(remaining - 1);
  //   }, 500);
  // }, [remaining])

  // // WRONG
  // setInterval(() => {
  //   setRemaining(remaining - 1);
  // }, 500);

  return <div>{remaining}</div>
}

export default Countdown;