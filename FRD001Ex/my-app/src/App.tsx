import React, {useState} from 'react';
import logo from './logo.png';
import './App.scss';

function App() {
    const [name, setName] = useState("Please Click the button")
  return (
    <>
    <div className="App">
       <header>
            <h1>Simple Website</h1>
        </header>
        <section>
            This is a simple website made without React. Try to convert this into React enabled.

            <ol>
                <li>First, you need to use <span className="command">create-react-app</span></li>
                <li>Second, you need to run <span className="command">npm start</span></li>
            </ol>
            {name /* expression */}
            <button onClick={() => {
          setName("Hello Tecky!")
        }}>button</button>
        </section>
        <footer>
            <img src={logo} className="App-logo" alt="tecky-logo"/>
      {/* <header className="App-header"> */}
        {/* <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p> */}
        {/* <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a> */}
        </footer>
        </div>
        </>
  );
}

export default App;
