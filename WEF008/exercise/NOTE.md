# WEF008 Exercise

## Step 1

Create HTML, CSS file

```Bash
touch index.html main.css
```

Import CSS, JS in `index.html`

**CSS:**

```Html
<link
  rel="stylesheet"
  href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous"
/>
<link
  rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css"
/>
<link rel="stylesheet" href="main.css" />
```

**JS:**

```Html

```

## Step 2

Add the container

```Html
<div class="container-fluid">
</div>
```

## Step 3

```Html
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      ...
    <div>
  </div>
</div>
```

## Step 4

Create a widget.

```Html
<div class="widget">
  <h3 class="widget-title">latest media</h3>
  <div class="row">
    ...
  </div>
</div>
```

## Step 5

Add the Card

```Html
<div class="col-sm-6 col-md-3">
  <div class="card">
    <img src="img_avatar.jpg" alt="Avatar" style="width:100%;" />
    <div class="card-container">
      <div class="media-card-content">
        <div>
          <p>
            Hello World<br />
            <span>hello, world</span>
          </p>
          <a href="http://www.google.com" class="card-btn">View set</a>
        </div>
        <div class="card-icons">
          <i class="fa fa-address-book"></i>
        </div>
      </div>
    </div>
  </div>
</div>
```

## Step 6

Create another Row

```Html
<div class="container-fluid">
  <div class="row">
    ...
  </div>
</div>
```

## Step 7

Create the Columns

```Html
<div class="col-3">
</div>
<div class="col-6">
</div>
<div class="col-3">
</div>
```

## Step 8

Upcoming Events

```Html
<div class="widget">
  <h3 class="widget-title">UPCOMING EVENTS</h3>
  ...
</div>
```

```Html
<ul class="event-list">
  <li>
    <div>
      <div style="font-size: 20px;">30</div>
      <div style="font-size: 15px;">JAN</div>
      <div style="font-size: 20px;">2019</div>
    </div>
    <div>
      <p>APAP<br />Thessaloniki, Greece</p>
      <a href="http://www.google.com" class="card-btn">View set</a>
    </div>
  </li>
  ...
</ul>
```

## Step 9

Latest News

```Html
<div class="widget">
  <h3 class="widget-title">LATEST NEWS</h3>
  ...
</div>
```

```Html
<div class="card news-card">
  <div style="position: relative;"> <-- ???
    <img src="cat.jpg" alt="Cat" style="width:100%">
    <div class="post-date">
      <p>31</p>
      <p>DEC</p>
      <p>2018</p>
    </div>
  </div>
  <div class="card-container">
    <div class="card-content">
      <h5>My Topic</h5>
      <p>In condimentum facilisis porta. Sed nec diam eu diam mattis viverra. Nulla fringilla, orci ac euismod
        semper, magna diam porttitor mauris, quis sollicitudin sapien justo in libero. Vestibulum mollis
        mauris enim. Morbi euismod magna ac lorem rutrum elementum. Donec viverra auctor lobortis.
        Pellentesque eu est a nulla placerat dignissim.</p>
    </div>
  </div>
</div>
```
