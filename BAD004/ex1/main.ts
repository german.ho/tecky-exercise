import Knex from 'knex';
import xlsx from 'xlsx';

interface User {
    username: string;
    password: string;
    level: string;
}

interface File {
    name: string;
    content: string;
    isFile: number;
}

interface Category {
    name: string;
}

async function main() {

    const knexConfig = require('./knexfile');
    // console.log(knexConfig[process.env.NODE_ENV || "development"]);
    let knex: Knex | undefined;

    try {
        knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

        // read xlsx file
        const workbook = xlsx.readFile('BAD003-exercise.xlsx');
        const users: User[] = xlsx.utils.sheet_to_json(workbook.Sheets['user']);
        const files: File[] = xlsx.utils.sheet_to_json(workbook.Sheets['file']);
        const categories: Category[] = xlsx.utils.sheet_to_json(workbook.Sheets['category']);

        await knex.raw(/*SQL*/`TRUNCATE table "user" RESTART IDENTITY`);
        await knex.raw(/*SQL*/`TRUNCATE table "file" RESTART IDENTITY`);
        await knex.raw(/*SQL*/`TRUNCATE table "category" RESTART IDENTITY`);

        for (const user of users) {
            await knex.raw(
                /*SQL*/`INSERT INTO "user" (username, password, level) VALUES
                    (?, ?, ?)`, [user.username, user.password, user.level]
            );
        }

        for (const file of files) {
            // console.log(typeof file.content === 'undefined');
            // if (typeof file.content === 'undefined') {
            //     file.content = "";
            // }
            await knex.raw(
                /*SQL*/`INSERT INTO "file" (name, content, isFile) VALUES
                    (?, ?, ?)`, [file.name, file.content || "", file.isFile === 1]
            );
        }

        for (const category of categories) {
            await knex.raw(
                /*SQL*/`INSERT INTO "category" (name) VALUES
                    (?)`, [category.name]
            );
        }

    } catch (err) {
        console.log(err.message);

    } finally {
        if (knex) {
            await knex.destroy();
        }

    }
}

main();
