CREATE DATABASE fileserver;

CREATE TABLE "user" (
    id SERIAL primary key,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    level INTEGER NOT NULL,
    created TIMESTAMP,
    updated TIMESTAMP
);

CREATE TABLE file (
    id SERIAL primary key,
    name VARCHAR(255) NOT NULL,
    isDirectory BOOLEAN NOT NULL,
    content text,
    created TIMESTAMP,
    updated TIMESTAMP
);

CREATE TABLE category (
    id SERIAL primary key,
    name VARCHAR(255) NOT NULL,
    created TIMESTAMP,
    updated TIMESTAMP
);

CREATE TABLE membership (
    id SERIAL primary key,
    name VARCHAR(255) NOT NULL,
    price NUMERIC,
    created TIMESTAMP,
    updated TIMESTAMP
);

ALTER TABLE category ADD COLUMN icon VARCHAR(255);

ALTER TABLE file RENAME COLUMN isDirectory TO isFile;

-- ALTER TABLE file ALTER COLUMN created TYPE TIMESTAMP;

DROP TABLE membership;

ALTER TABLE "user" DROP COLUMN level;
ALTER TABLE "user" ADD COLUMN level VARCHAR(255) NOT NULL;