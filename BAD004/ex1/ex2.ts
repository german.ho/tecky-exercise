import { Client } from 'pg';
import xlsx from 'xlsx';

interface User {
    username: string;
    password: string;
    level: string;
}

interface File {
    name: string;
    content: string;
    isFile: number;
}

interface Category {
    name: string;
}

async function main() {

    const client = new Client({
        user: "jason",
        host: "localhost",
        database: "fileserver",
        password: ""
    });

    await client.connect();

    // read xlsx file
    const workbook = xlsx.readFile('BAD003-exercise.xlsx');
    const users: User[] = xlsx.utils.sheet_to_json(workbook.Sheets['user']);
    const files: File[] = xlsx.utils.sheet_to_json(workbook.Sheets['file']);
    const categories: Category[] = xlsx.utils.sheet_to_json(workbook.Sheets['category']);

    for (const user of users) {
        await client.query(
            /*SQL*/`INSERT INTO "user" (username, password, level) VALUES
                ($1, $2, $3)`, [user.username, user.password, user.level]
        );
    } 

    for (const file of files) {
        await client.query(
            /*SQL*/`INSERT INTO "file" (name, content, isFile) VALUES
                ($1, $2, $3)`, [file.name, file.content, file.isFile === 1]
        );
    }

    for (const category of categories) {
        await client.query(
            /*SQL*/`INSERT INTO "category" (name) VALUES
                ($1)`, [category.name]
        );
    }

    // const res = await client.query('SELECT $1::text as message', ['Hello world!'])
    // console.log(res.rows[0].message) // Hello world!
    await client.end();
}

main();
