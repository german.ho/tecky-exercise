export interface Student {
    id: number;
    name: string;
    level: string;
    date_of_birth: Date;
    teacher_id: number;
}