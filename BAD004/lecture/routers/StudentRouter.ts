import { StudentService } from '../services/StudentService';
import express from 'express';
import { Request, Response } from 'express';

export class StudentRouter {

    constructor(private studentService: StudentService) {}

    router() {
        const router = express.Router();
        router.get("/", this.getStudents);
        return router;
    }

    getStudents = async(req: Request, res: Response) => {
        const students = await this.studentService.getStudents();
        res.json({ students });
    }
}