import Knex from "knex";
import { Student } from '../models/student'

export class StudentService {

    constructor(private knex: Knex) {}

    async getStudents() {
        const result = await this.knex.raw(/*sql*/`SELECT * FROM students`); // plain SQL
        const students: Student[] = result.rows;
        return students;
    }
    
    addStudent(body:any){
        return this.knex.raw(/*sql*/`INSERT INTO students 
           (name,level,date_of_birth) VALUES (?,?,?) RETURNING id`,
           [body.name, body.level,body.dateOfBirth]);
    }
    
    updateStudent(id:number,body:any){
        return this.knex.raw(/*sql*/`UPDATE students
            SET name= ?, level= ?, date_of_birth= ? WHERE id= ?
        `,[body.name,body.level,body.dateOfBirth,id]);
    }
    
    deleteStudent(id:number){
        return this.knex.raw(/*sql*/"DELETE FROM students WHERE id = ?", [id]);
    }
}