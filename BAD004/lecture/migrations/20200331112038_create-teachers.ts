import Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable("teachers");
    if (!hasTable) {
        await knex.schema.createTable("teachers", table => {
            table.increments(); // default column name: id
            table.string("name").notNullable();
            table.date("date_of_birth");
            table.timestamps(false, true);
        });
    }
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTableIfExists("teachers");
}

