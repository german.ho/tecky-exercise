import Knex from 'knex';
import express from 'express';
// import ...

const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

const app = express();
// ...

import { StudentService } from './services/StudentService';
import { StudentRouter } from './routers/StudentRouter';

// async function demoRouter() {
//     const studentService = new StudentService(knex);

//     // Select
//     // const students = await studentService.getStudents();
//     // console.log(students);

//     // Insert
//     // const insertedId = (await studentService.addStudent(
//     //     { name: "Peter", level: 25, dateOfBirth: "1995-05-15" })).rows[0].id;
//     // console.log(insertedId);

//     // Update
//     const result = await studentService.updateStudent(4,
//         { name: "Peter", level: 25, dateOfBirth: new Date() });
//     console.log(result.rowCount);
// }

// demoRouter();

const studentService = new StudentService(knex);
const studentRouter = new StudentRouter(studentService);

app.use("/students", studentRouter.router());

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`listening to PORT ${PORT}`);
});
