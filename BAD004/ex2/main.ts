import Knex from 'knex';
import { UserService } from './services/UserService';

async function main() {

    const knexConfig = require('./knexfile');
    const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

    try {
        const userService = new UserService(knex);
        const userId = await userService.createUser("jason456", "jason", 18);
        console.log(userId);

        const user = await userService.getUserByID(userId);
        console.log(user);

    } catch (err) {
        console.log(err.message);

    } finally {
        await knex.destroy();

    }
}

main();