import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.alterTable("users", table => {
        table.integer("age");
    });
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.alterTable("users", table => {
        table.dropColumn("age");
    });
}

