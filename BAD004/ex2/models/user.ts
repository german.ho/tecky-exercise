// interface Human {
//     abc: string;
// }

// export interface User extends Human {
export interface User {
    id: number;
    username: string;
    password: string;
    age: number;
}