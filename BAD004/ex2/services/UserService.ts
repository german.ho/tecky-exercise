import Knex from 'knex';
import { User } from '../models/user';

export class UserService {

    constructor(private knex: Knex){}

    async getAllUsers() {
        const result = await this.knex.raw(/*SQL*/
            `SELECT * FROM users`);
        return result.rows as User[];
    }

    async createUser(username: string, password: string, age: number) {
        {
            const result = await this.knex.raw(/*SQL*/`
                SELECT * FROM users WHERE username = ?
            `, [username]);
            console.log(result.rowCount);
            if (result.rows.length > 0) {
                throw new Error("duplicate username");
            }
        }
        const result = await this.knex.raw(/*SQL*/`
            INSERT INTO users (username, password, age) VALUES (?, ?, ?) RETURNING id
        `, [username, password, age]);
        return result.rows[0].id;
    }

    async getUserByID (id: number) {
        const result = await this.knex.raw(/*SQL*/
            `SELECT * FROM users WHERE id = ?`, [id]);
        return result.rows[0] as User;
    }
}