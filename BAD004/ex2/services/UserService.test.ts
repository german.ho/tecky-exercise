import Knex from 'knex';

// Assuming you test case is inside `services/ folder`
const knexfile = require('../knexfile');

// Now the connection is a testing connection.
const knex = Knex(knexfile["testing"]);

import { UserService } from './UserService';

describe("UserService", () => {
    let userService: UserService;

    beforeEach(async () => {
        userService = new UserService(knex);
        await knex.raw(/*sql*/`DELETE from users`);
        await knex.raw(/*sql*/`INSERT INTO users (username, password, age) 
         VALUES (?,?,?)`, ["jason", "jason", 18]);
    });

    it("get all Users", async () => {
        const users = await userService.getAllUsers();
        expect(users.length).toBe(1);
    });

    it("create User", async () => {
        await userService.createUser("peter", "peter", 12);
        const users = await userService.getAllUsers();
        expect(users.length).toBe(2);
    });

    it("get User by ID", async () => {
        const users = await userService.getAllUsers();
        const user = await userService.getUserByID(users[0].id);
        expect(user.username).toBe(users[0].username);
    });

    afterAll(() => {
        knex.destroy(); // Important!!
    });
});