// console.log(document.querySelector(".test"));

// document.querySelector(".test").addEventListener("click", function(e) {
// 	this.innerHTML = "<div><div>Hi</div></div>";
// 	console.log("hello");
// });

// document.querySelector(".test").onclick = function () {
// 	console.log("this is jason");
// }

// document.querySelector(".a").addEventListener("click", function(e) {
// 	console.log(this.querySelectorAll(".b"));
// });

// console.log(document.querySelectorAll(".b"));

// const listItems = document.querySelectorAll("#my-list .list-item");
// for (let listItem of listItems) {
// 	listItem.addEventListener("click", function(e) {
// 		console.log(e.target.textContent);
// 	})
// }

document.querySelector('#my-list').addEventListener('click', function(event){
	// console.log(event.target.textContent);
	console.log(event.target);
	console.log(event.target.matches('.list-item'));
	if(event.target && event.target.matches('.list-item')){
		console.log(`${event.target.textContent} has been clicked!`);
	}
});