# WEF006 Document Object Model

The HTML structures resembles a **tree**, therefore the browser actually does parse the HTML and **store them into a tree structure**. The tree structure is what we called Document Object Model or DOM. In order to make our **website interactive**, we need to manipulate the Document Object Model as a result.

## DOM Selector

```Javascript
<div id="item-123" class="items">...</div>
<div id="item-456" class="items"></div>
<p>This is a paragraph</p>
```

- `document.getElementById(id)`: returns the element with id. null is returned if the id does not exist in any element.
- `document.getElementsByTagName(tagName)`: returns an array of elements with the same name.
- `document.getElementsByClassName(className)`: returns an array of elements with the same class name.

- `document.querySelector(cssSelector)`: Select the first element that fulfills the CSS selector supplied.
- `document.querySelectorAll(cssSelector)`: Select all the elements that fulfill the CSS selector supplied.

## DOM Events

DOM Events are sent when something happens in the DOM elements. There are lots of different events existing. Some useful events are:

```Html
<div id="test">
</div>

<script type='text/javascript'>
// Using onclick
document.querySelector('#test').onclick = function(event){
    // The event object contains the information about the event
    console.log("Test is clicked!");
}
//-- OR --

document.querySelector('#test').addEventListener('click',function(event){
    // The event object contains the information about the event
    console.log("Test is clicked");
});
</script>
```

## Document VS this

```Html
<div class="b">
    peter
</div>
<div class="a">
    <div class="b">
        jason
    </div>
</div>
```

```Javascript
document.querySelector(".a").addEventListener("click", function(e) {
	console.log(this.querySelectorAll(".b"));
});

console.log(document.querySelectorAll(".b"));
```
