// Exercise 1
// console.log(document.querySelector('#box-1'));
// document.querySelector('#box-1').addEventListener('click',function(event){
// 	// 1. Get the element itself by event.target or event.currentTarget
// 	// 2. Check if the box is occupied
// 	// 3, Put the X in if the box is not occupied.
// 	if (event.target.textContent === "") {
// 		event.target.innerHTML = "X";
// 	}
// });

// Assume X = 1, O = -1, Empty = 0
const arr = [0, 0, 0, 0, 0, 0, 0, 0, 0];

// for (let i =0; i < 3; i++) {
// 	for (let j =0; j < 3; j++) {
// 		console.log(i * 3 + j);
// 		console.log(j * 3 + i);
//  	}
// }

const WIN_PATTERN = [
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],

  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],

  [0, 4, 8],
  [2, 4, 6]
];
let turnNum = 1;
let isGameEnd = false;

window.onload = function() {
  initNextTurn();
  initBoard();
};

const isCircle = () => turnNum % 2 === 0;
const getOutput = () => (isCircle() ? "O" : "X");

function hasWinner() { // true / false
	// no need WIN_PATTERN
  let dia1 = 0, dia2 = 0;
  for (let i = 0; i < 3; i++) {
    let sumRow = 0, sumCol = 0;
    for (let j = 0; j < 3; j++) {
      sumRow += arr[i * 3 + j];
      sumCol += arr[j * 3 + i];
      if (i === j) {
        dia1 += arr[i * 3 + j];
      }
      if (j === 2 - i) {
        dia2 += arr[i * 3 + j];
      }
    }
		if (Math.abs(sumRow) === 3 || Math.abs(sumCol) === 3) {
			return true;
		}
	}
	if (Math.abs(dia1) === 3 || Math.abs(dia2) === 3) {
		return true;
	}
  return false;
}

function getScoreV2() {
	// no need WIN_PATTERN
  let dia1 = 0, dia2 = 0;
  for (let i = 0; i < 3; i++) {
    let sumRow = 0, sumCol = 0;
    for (let j = 0; j < 3; j++) {
      sumRow += arr[i * 3 + j];
      sumCol += arr[j * 3 + i];
      if (i === j) {
        dia1 += arr[i * 3 + j];
      }
      if (j === 2 - i) {
        dia2 += arr[i * 3 + j];
      }
    }
		if (Math.abs(sumRow) === 3) {
			return sumRow;
		}
		if (Math.abs(sumCol) === 3) {
			return sumCol;
		}
	}
	if (Math.abs(dia1) === 3) {
		return dia1;
	}
	if (Math.abs(dia2) === 3) {
		return dia2;
	}
  return null;
}

function getScore() {
  for (const pattern of WIN_PATTERN) {
    // [0, 3, 6] -> (((0 + arr[0]) + arr[3]) + arr[6])
    const score = pattern.reduce((acc, cur) => acc + arr[cur], 0);
    if (Math.abs(score) === 3) {
      return score;
    }
  }
  return null;
}

function initNextTurn() {
  if (turnNum === 10) {
    console.log("DRAW, HAHAHA");
    return;
  }
  const output = getOutput();
  document.querySelector(".current-turn").innerHTML = `${output} Turn`;

  const idx = isCircle() ? [1, 0] : [0, 1];
  const turnBoxes = document.querySelectorAll(".turn div");
  turnBoxes[idx[0]].setAttribute("class", "current");
  turnBoxes[idx[1]].removeAttribute("class");
}

function initBoard() {
  const boxes = document.querySelectorAll(".board div");
  for (let i = 0; i < boxes.length; i++) {
    // for (let box of boxes) {
    boxes[i].addEventListener("click", function(event) {
      if (isGameEnd) {
        return;
      }
      if (event.target.textContent === "") {
        event.target.innerHTML = getOutput();
        arr[i] = isCircle() ? -1 : 1;
				// const score = getScore();
				// const score = getScoreV2();
        // if (score !== null) {
        if (hasWinner()) {
          // someone win!
          isGameEnd = true;
          // const winner = score === 3 ? "Player X" : "Player O";
          // can I use getOutput() instead of winner?
          console.log(`Player ${getOutput()} WIN!!!`);
          return;
        }
        turnNum++;
        initNextTurn();
      }
    });
  }
}
