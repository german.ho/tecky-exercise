# WEF005 Exercise

## step 1

```Html
<div class="wrap-box">
</div>
```

## step 2

```Html
<div class="head">
  <div class="difficulty">
    <select>
      <option>Medium</option>
      <option>Easy</option>
    </select>
  </div>
  <div>
    <i class="fas fa-share-alt"></i>
  </div>
</div>
```

We need to install the `font-awesome` before we use it.

```Html
<head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css">
</head>
```

## step 3

```Html
<div class="turn">
  <div class="current">
    X
  </div>
  <div>
    O
  </div>
</div>
```

## step 4

```Html
<div class="turn">
  <div class="current">
    X
  </div>
  <div>
    O
  </div>
</div>
<div class="current-turn">
  X Turn
</div>
```

## step 5

```Html
<div class="board-background">
  <div class="board">
    <div class="top left"></div>
    <div class="top"></div>
    <div class="top right"></div>

    <div class="left"></div>
    <div></div>
    <div class="right"></div>

    <div class="bottom left"></div>
    <div class="bottom"></div>
    <div class="bottom right"></div>
  </div>
</div>
```
