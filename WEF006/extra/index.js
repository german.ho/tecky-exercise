// function foo() {
// 	let a = 100;
// 	const arr = [1, 2, 3, 4];
// 	const newArr = arr.map(num => num + a);
// 	console.log("this is foo");
// 	console.log(newArr);
// }

// function bar() {
// 	let a = 100;
// 	const arr = [1, 2, 3, 4];
// 	const newArr = arr.map(jason);
// 	console.log("this is bar");
// 	console.log(newArr);
// }

// foo();
// bar();

// const createFooBar = () =>
// 	Object.freeze({
// 	val: 100,
// 	arr: [1, 2, 3, 4],
// 	foo: function () {
// 		console.log(this.val);
// 	},
// 	bar: function () {
// 		return this.arr.map(function(num) {
// 			return num + this.val;
// 		}, this);
// 	}
// });

const createFooBar = () => {
	const data = { val: 100, arr: [1, 2, 3, 4], };
	return Object.freeze({
		foo: () => {
			console.log(data.val);
		},
		bar: () => data.arr.map(num => num + data.val)
	})
};

const foobar = createFooBar();
foobar.foo(); // <-- 100
console.log(foobar.bar()); // <-- ???
