// Encapsulation
// Inheritance
// Polymorphism

// 只會有 public properties （但一定無內容）
// 同埋 public methods （都一樣一定無內容）
interface KnowHowToEat {
    // favouriteFoods: string[]; // public properties
    eat(food: number): void; // public methods
  }
  
  interface HaveHp {
    getHp(): number;
  }
  
  // 定義類別
  // 類
  abstract class Cat implements KnowHowToEat, HaveHp {
    // properties 屬性
    // class properties 類別屬性
    public static population: number = 10000;
    // instance properties 個體屬性
    protected hp: number; // 客廁
    private mood: number; // 套廁
    public 罐罐only: boolean = false; // 公廁
  
    // constructor 建構器
    public constructor(hp: number = 100, mood: number = 50) {
      this.hp = hp;
      this.mood = mood;
    }
    
    // methods 行為
    // class methods 類別行為
    public static extinct() {
      this.population = 0;
    }
    // instance method 個體行為
  
    public eat(food: number) {
      let weather = Math.random();
      if (weather > 0.5) {
        this.hp = this.hp + food * 2;
      } else {
        this.hp = this.hp + food;
      }
    }
    // getter/setter
    public getHp() {
      return this.hp;
    }
    public getMood() {
      return this.mood;
    }
  
    // Abstract methods
    public abstract getBreed(): string; //類似 interface
  }
  
  class 波斯貓 extends Cat {
    public eat(food: number) {
      super.eat(food)
      this.hp = this.hp + 1;
      console.log('Meow....')
    }
    public getBreed() {
      return '波斯種'
    }
  }
  class 比卡超 extends Cat {
    public eat(food: number) {
      super.eat(food)
      console.log('Erup....')
    }
    public 十萬伏特() {
      return '⚡️⚡️⚡️'
    }
    public getBreed() {
      return '比卡比'
    }
  }
  
  class Dog implements KnowHowToEat {
    public eat(food: number) {
      console.log('汪汪汪')
    }
  }
  
  class 電子貓 implements KnowHowToEat {
    public eat(food: number) {
      console.log('喵喵喵')
    }
  }
  
  //  vvvvv Object
//   let 填海華 = new Cat();
  
  let 維尼 = new 波斯貓();
  //              ^^^ Class
  維尼.eat(10)
  console.log(維尼.getHp())
  console.log(維尼.getMood())
  
  // Cat.extinct();
  // console.log(Cat.population)
  
  let 牛丸 = new 比卡超(150, 60);
  牛丸.eat(10)
  console.log(牛丸.getHp())
  console.log(牛丸.十萬伏特())
  
  let 林鄭  = new Dog();
  
  function hotelEat(pet: KnowHowToEat) {
    pet.eat(0.1);
  }
  
  hotelEat(維尼)
  hotelEat(牛丸)
  hotelEat(林鄭)
  