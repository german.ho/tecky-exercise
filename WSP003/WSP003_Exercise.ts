/*Exercise 1 */
class Player{
    private strength:number;
    private name:string;
    private exp: number = 0
    constructor(strength:number,name:string){
        this.strength  = strength;
        this.name = name;
    }


    attack(monster:Monster){
        while (monster.getHp() > 0){
        if (Math.random() < 0.2){
        monster.injure(this.strength * 2)
        this.gainExperience(15)
        console.log(`Player ${this.name} attacks a monster (HP: ${monster.getHp()}) [CRITICAL]`) 
        } else{
        monster.injure(this.strength)
        this.gainExperience(10)
        console.log(`Player ${this.name} attacks a monster (HP: ${monster.getHp()})`)
    }
    }
    console.log(`Player ${this.name} wins! (Exp: ${this.exp})`)
}


    gainExperience(exp:number){
        this.exp = this.exp + exp;
    }

}


class Monster{
    private hp: number = 100;
    private reborn: number = 1
    public injure(strength: number){
        this.hp = Math.max(this.hp - strength, 0)
        if (this.hp <= 0 && this.reborn >= 1){
            this.reborn -= 1
            this.hp = 100
            console.log("I am reborn again!")
        }
    } 
    public getHp(){
        return this.hp;
    }
}


// Invocations of the class and its methods
const player = new Player(20,'Peter');
const 喵喵怪 = new Monster();
player.attack(喵喵怪);
// English counterpart: Player attacks monster








/* Exercise 2 */

interface Attack{
    damage:number
 }
 
 class BowAndArrow implements Attack{
      //Bow and Arrow Attack here
      public damage: number;
      
      public constructor(damage: number){
          this.damage = damage
      }
 }
 
 class ThrowingSpear implements Attack{
    // Throwing Spear Attack here

    public damage: number;
      
    public constructor(damage: number){
        this.damage = damage; 
      }
 }
 
 class Sword implements Attack{
    // Throwing Sword here

    public damage: number;
      
    public constructor(damage: number){
        this.damage = damage; 
      }
 }
 
 class MagicSpells implements Attack{
    // Throwing MagicSpells here

    public damage: number;
      
    public constructor(damage: number){
        this.damage = damage; 
      }
 }

 class Slings implements Attack{
    // Throwing Slings here

    public damage: number;
      
    public constructor(damage: number){
        this.damage = damage; 
      }
 }

 class Minions implements Attack{
    // Throwing Minions here

    public damage: number;
      
    public constructor(damage: number){
        this.damage = damage; 
      }
 }

 interface Player{
     attack(monster:Monster): void;
     switchAttack(): void;
     gainExperience(exp:number): void;
 }
 
 class Amazon implements Player{
     private primary: Attack;
     private secondary: Attack;
     private usePrimaryAttack: boolean;
     constructor(){
         this.primary = new BowAndArrow(30);
         this.secondary = new ThrowingSpear(40);
         this.usePrimaryAttack = true;
         // TODO: set the default value of usePrimaryAttack
      }
 
      attack(monster:Monster):void{
          if(this.usePrimaryAttack){
            monster.attack(this.primary.damage)
          }else{
            this.secondary.damage
          }
      }
 
      switchAttack(){
          if (this.usePrimaryAttack){
              this.usePrimaryAttack = false
          } else {
              this.usePrimaryAttack = true
          }
      }
      
      gainExperience(exp: number){

      }
      //.. The remaining methods.
 }
 
 interface Monster{
     injure(strength: number): void;
 }











 
 /* Exercise 2 - After Refactoring */

//  interface Attack{
//     damage:number
//  }
 
//  class BowAndArrow implements Attack{
//       //Bow and Arrow Attack here
//       public damage: number;
      
//       public constructor(damage: number){
//           this.damage: damage; 
//       }
//  }
 
//  class ThrowingSpear implements Attack{
//     // Throwing Spear Attack here

//     public damage: number;
      
//     public constructor(damage: number){
//         this.damage: damage; 
//       }
//  }
 
//  class Sword implements Attack{
//     // Throwing Sword here

//     public damage: number;
      
//     public constructor(damage: number){
//         this.damage: damage; 
//       }
//  }
 
//  class MagicSpells implements Attack{
//     // Throwing MagicSpells here

//     public damage: number;
      
//     public constructor(damage: number){
//         this.damage: damage; 
//       }
//  }

//  class Slings implements Attack{
//     // Throwing Slings here

//     public damage: number;
      
//     public constructor(damage: number){
//         this.damage: damage; 
//       }
//  }

//  class Minions implements Attack{
//     // Throwing Minions here

//     public damage: number;
      
//     public constructor(damage: number){
//         this.damage: damage; 
//       }
//  }

//  interface IPlayer{
//      attack(monster:Monster): void;
//      switchAttack(): void;
//      gainExperience(exp:number): void;
//  }
 
//  abstract class Player implements IPlayer{
//     protected primary: Attack;
//     protected secondary: Attack;
//     protected usePrimaryAttack: boolean;

//     constructor(primary: Attack, secondary: Attack){
//         super()
//         this.primary = primary;
//         this.secondary = secondary;
//         this.UsePrimaryAttack: true;
    
//     public attack(monster:Monster):void{
//         if(this.usePrimaryAttack){
//           monster.attack(this.primary.damage)
//         }else{
//           this.secondary.damage
//         }
//     }

//     public switchAttack(){
//         if (this.usePrimaryAttack){
//             this.usePrimaryAttack = false
//         } else {
//             this.usePrimaryAttack = true
//         }
//     }
    
//     public gainExperience(exp: number){

//     } 
     
//  }

//  class Amazon extends Player{
    
//      constructor(primary: BowAndArrow, secondary: ThrowingSpear){
//          super(primary, secondary)
//          // TODO: set the default value of usePrimaryAttack
//       }
 
//       //.. The remaining methods.
//  }
 
//  const amazon = new Amazon(new BowAndArrow (30), new ThrowingSpear(40))

//  interface Monster{
//      injure(strength: number): void;
//  }


