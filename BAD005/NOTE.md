# BAD005

- teachers
PK: id
FK: N/A

- students
PK: id
FK: teacher_id

## Install KING

```Bash
yarn init -y
yarn add typescript ts-node @types/node
yarn add knex @types/knex pg @types/pg
touch tsconfig.json
yarn knex init -x ts
yarn add dotenv @types/dotenv
```

## JOIN

```SQL
SELECT * FROM students INNER JOIN teachers ON students.teacher_id = teachers.id;
SELECT * FROM students LEFT JOIN teachers ON students.teacher_id = teachers.id;
SELECT * FROM students RIGHT JOIN teachers ON students.teacher_id = teachers.id;
SELECT * FROM students FULL JOIN teachers ON students.teacher_id = teachers.id;

SELECT teachers.name, students.name FROM teachers LEFT JOIN students ON teachers.id = students.teacher_id;
```

**order** - PK: id
**order_details** - FK: order_id

INSERT INTO order (...) VALUES (...) RETURNING id;
INSERT INTO order_details (order_id, ...) VALUES (id, ...);


## Aggregations

- top 100 record, then select content including 'Etiam justo'

```SQL
WITH tmp AS (SELECT * FROM file LIMIT 100)
SELECT count(*) FROM tmp WHERE content LIKE '%Etiam justo%';
```

- number of student for every level

```SQL
SELECT level, COUNT(*) FROM students GROUP BY level;
```

- sum of level group by teacher (sum of level > 45)

```SQL
SELECT teachers.name, SUM(students.level::int) FROM teachers 
INNER JOIN students ON teachers.id = students.teacher_id 
GROUP BY teachers.name HAVING SUM(students.level::int) > 45;
```

## Transaction

- BEGIN;

```SQL
INSERT INTO students (name, level, date_of_birth, teacher_id) VALUES ('TEST', 25, '2020-06-16', 2);

INSERT INTO students (name, level, date_of_birth, teacher_id) VALUES ('TEST ABC', 25, '2020-06-16', 2);
INSERT INTO students (name, level, date_of_birth, teacher_id) VALUES ('TEST DEF', 25, '2020-06-16', 2);

INSERT INTO students (name, level, date_of_birth, teacher_id) VALUES ('TEST 123', 25, '2020-06-16', 2);
INSERT INTO students (name, level, date_of_birth, teacher_id) VALUES ('TEST 456', 25, '2020-06-16', 2);
```

- COMMIT;

- ROLLBACK;
