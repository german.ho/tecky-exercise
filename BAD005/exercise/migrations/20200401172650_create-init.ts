import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
	await knex.schema.createTable("users", table => {
		table.increments();
		table.string("username").notNullable().unique();
		table.string("password").notNullable();
		table.string("level");
		table.timestamps(false, true);
	});

	await knex.schema.createTable("categories", table => {
		table.increments();
		table.string("name").notNullable();
		table.timestamps(false, true);
	});

	await knex.schema.createTable("files", table => {
		table.increments();
		table.string("name");
		table.text("content");
		table.boolean("is_file");
		table.integer("category_id").unsigned();
		table.foreign("category_id").references("categories.id");
		table.integer("owner_id").unsigned();
		table.foreign("owner_id").references("users.id");
		table.timestamps(false, true);
	});
}


export async function down(knex: Knex): Promise<any> {
	await knex.schema.dropTable("files");
	await knex.schema.dropTable("categories");
	await knex.schema.dropTable("users");
}

