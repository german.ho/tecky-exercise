const SCORE_MIN = 0.5;

function getScore(input: string, username: string) {
	let count = 0;
	for (let i = 0; i < username.length; i++) {
		if (input[i] === username[i]) {
			count++;
		}
	}
	return parseFloat((count / username.length).toFixed(2));
}

function createGetCorrectName(usernames: string[]) {
	return function(inputName: string) {
		const scores: {name: string, score: number}[] = [];
		for (const username of usernames) {
			if (username === inputName) {
				return inputName;
			} else {
				const score = getScore(inputName.toLowerCase(), username.toLowerCase());
				scores.push({ name: username, score });
			}
		}
		scores.sort((a, b) => b.score - a.score);
		console.log(scores);
		if (scores[0].score >= SCORE_MIN) {
			return scores[0].name;
		}
		throw new Error("Fail score!!!")
	}
}

const tests = ['michael', 'alex', 'gordon'];
const input = "alexgordon";

const testFunc = createGetCorrectName(tests);
console.log(testFunc(input));