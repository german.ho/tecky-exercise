import xlsx from "xlsx";
import Knex from "knex";

interface User {
  Username: string;
  Password: string;
  Level: string;
}

interface File {
  Name: string;
  Content: string;
  "is File": number;
  Category: string;
  Owner: string;
}

interface Category {
  Name: string;
}

// Method 1
// const OWNER_MAP = Object.freeze({
//   gordan: "gordon",
//   alexs: "alex",
//   admiin: "admin",
//   ales: "alex",
//   micheal: "michael"
// });

const SCORE_MIN = 0.5;

function getScore(input: string, username: string) {
	let count = 0;
	for (let i = 0; i < username.length; i++) {
		if (input[i] === username[i]) {
			count++;
		}
	}
	return parseFloat((count / username.length).toFixed(2));
}

function createGetCorrectName(usernames: string[]) {
	return function(inputName: string) {
		const scores: {name: string, score: number}[] = [];
		for (const username of usernames) {
			if (username === inputName) {
				return inputName;
			} else {
				const score = getScore(inputName.toLowerCase(), username.toLowerCase());
				scores.push({ name: username, score });
			}
		}
		scores.sort((a, b) => b.score - a.score);
		if (scores[0].score >= SCORE_MIN) {
			return scores[0].name;
		}
		throw new Error("Fail score!!!")
	}
}

async function main() {
  const knexConfig = require("./knexfile");
  const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

  try {
    await knex.raw(/*SQL*/ `DELETE FROM "files"`);
    await knex.raw(/*SQL*/ `DELETE FROM "users"`);
    await knex.raw(/*SQL*/ `DELETE FROM "categories"`);

    // read xlsx file
    const workbook = xlsx.readFile("BAD005-exercise.xlsx");
    const users: User[] = xlsx.utils.sheet_to_json(workbook.Sheets["user"]);
    const files: File[] = xlsx.utils.sheet_to_json(workbook.Sheets["file"]);
    const categories: Category[] = xlsx.utils.sheet_to_json(
      workbook.Sheets["category"]
    );

    // console.log(users[0]);
    // console.log(files[0]);
    // console.log(categories[0]);

    const trx = await knex.transaction();

    try {
      const userMap = {};
      for (const user of users) {
        const result = await trx.raw(
          /*SQL*/ `INSERT INTO "users" (username, password, level) VALUES 
                (?, ?, ?) RETURNING id`,
          [user.Username, user.Password, user.Level]
        );
        const userId = result.rows[0].id;
        userMap[user.Username] = userId;
      }
      console.log(userMap);

      const categoryMap = {};
      for (const category of categories) {
        const result = await trx.raw(
          /*SQL*/ `INSERT INTO "categories" (name) VALUES
                (?) RETURNING id`,
          [category.Name]
        );
        const categoryId = result.rows[0].id;
        categoryMap[category.Name] = categoryId;
      }
      console.log(categoryMap);

			const getCorrectName = createGetCorrectName(Object.keys(userMap));
      for (const file of files) {
				// Method 1
				// if (file.Owner in OWNER_MAP) {
				// 	file.Owner = OWNER_MAP[file.Owner];
				// }
				// console.log(file.Owner, userMap[file.Owner]);

				// Method 2
				const newOwner = getCorrectName(file.Owner);
        await trx.raw(
          /*SQL*/ `INSERT INTO "files" (name, content, is_file, category_id, owner_id) VALUES
									(?, ?, ?, ?, ?)`,
          [
            file.Name,
            file.Content || "",
            file["is File"] === 1,
            categoryMap[file.Category],
						// userMap[file.Owner]
						userMap[newOwner]
          ]
        );
      }

      console.log("[info] transaction commit");
      await trx.commit();
    } catch (err) {
      console.log(err.message);
      console.log("[info] transaction rollback");
      await trx.rollback();
    }

    // const mapping = {};
    // for (const file of files) {
    // 	if (file.Owner in mapping) {
    // 		mapping[file.Owner] += 1;
    // 	} else {
    // 		mapping[file.Owner] = 1;
    // 	}
    // }

    // console.log(mapping);
  } catch (err) {
    console.log(err.message);
  } finally {
    await knex.destroy();
  }
}

main();
