import Knex from "knex";

async function main() {
    const knexConfig = require("./knexfile");
    const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

    try {
        {
            const result = await knex.raw(/*SQL*/`
                SELECT users.id, users.username, COUNT(files.id) FROM users 
                LEFT JOIN files ON users.id = files.owner_id 
                GROUP BY users.id`);
            console.log("How many files does each user have?");
            console.log(result.rows);
        }

        {
            const result = await knex.raw(/*SQL*/`
                SELECT categories.id, categories.name, COUNT(files.id) FROM categories 
                LEFT JOIN files ON categories.id = files.category_id 
                GROUP BY categories.id;`);
            console.log("How does the files distribute over different categories?");
            console.log(result.rows);
        }

        {
            const result = await knex.raw(/*SQL*/`
                SELECT COUNT(*) FROM files 
                INNER JOIN categories ON categories.id = files.category_id 
                INNER JOIN users ON users.id = files.owner_id 
                WHERE categories.name = ? AND users.username = ?`, ['Important', 'alex']);
            console.log("How many files of the category important does alex have ?");
            console.log(result.rows);
        }

        {
            const result = await knex.raw(/*SQL*/`
                SELECT users.id, users.username, COUNT(files.id) FROM users 
                LEFT JOIN files ON users.id = files.owner_id 
                GROUP BY users.id HAVING COUNT(files.id) > 750`);
            console.log("How many users has more than 750 files ?");
            console.log(result.rowCount);
        }

    } catch (err) {
        console.error(err.message);

    } finally {
        await knex.destroy();
    }
}

main();