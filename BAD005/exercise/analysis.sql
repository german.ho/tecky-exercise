
-- How many files does each user have?
SELECT users.id, users.username, COUNT(files.id) FROM users 
LEFT JOIN files ON users.id = files.owner_id 
GROUP BY users.id;

-- How does the files distribute over different categories?
SELECT categories.id, categories.name, COUNT(files.id) FROM categories 
LEFT JOIN files ON categories.id = files.category_id 
GROUP BY categories.id;

-- How many files of the category important does alex have ?
SELECT COUNT(*) FROM files 
INNER JOIN categories ON categories.id = files.category_id 
INNER JOIN users ON users.id = files.owner_id 
WHERE categories.name = 'Important' AND users.username = 'alex';

-- How many users has more than 800 files ?
WITH tmp AS (SELECT users.id, users.username, COUNT(files.id) FROM users 
LEFT JOIN files ON users.id = files.owner_id 
GROUP BY users.id HAVING COUNT(files.id) > 750) 
SELECT COUNT(*) FROM tmp;