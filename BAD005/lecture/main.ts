import Knex from 'knex';
const knexConfig = require('./knexfile');

// async function main() {
//     const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
//     const result = await knex.raw(/*SQL*/`
//         SELECT teachers.id as teacher_id, teachers.name as teacher_name, 
//         students.name as student_name , students.id as student_id
//         FROM students INNER JOIN teachers ON students.teacher_id = teachers.id`);
//     // console.log(result.rows);

//     // Method 1
//     const teacherMap = {};
//     for (const row of result.rows) {
//         const { student_id, student_name } = row;
//         const student = { id: student_id, name: student_name };
//         if (teacherMap[row.teacher_id]) {
//             teacherMap[row.teacher_id].students.push(student);
//         } else {
//             teacherMap[row.teacher_id] = { 
//                 id: row.teacher_id,
//                 name: row.teacher_name,
//                 students: [ student ]
//             }
//         }
//     }
//     const teachers = Object.values(teacherMap);
//     console.log(teachers);

//     // Method 2
//     // let groupedTeachers = {};
//     // for (let item of result.rows) {
//     //     const { student_name, student_id, ...others } = item;
//     //     const student = { student_name, student_id }
//     //     if (groupedTeachers[item.teacher_name]) {
//     //         groupedTeachers[item.teacher_name].students.push(student);
//     //     } else {
//     //         groupedTeachers[item.teacher_name] = { ...others, students: [student] };
//     //     }
//     // }
//     // const teachers = Object.values(groupedTeachers);
//     // console.log(JSON.stringify(teachers));
//     await knex.destroy();
// }

async function main2() {
    const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
    // knex.transaction(async (trx)=>{
    //     await trx.raw(/*sql*/'DELETE FROM students');
    //     await trx.raw(/*sql*/'DELETE FROM teachers');
    //     const result = await trx.raw(/*sql*/`INSERT INTO teachers (name,date_of_birth) 
    //        VALUES (?,?),(?,?) RETURNING id`,['Bob','1970-01-01','Herman','1971-05-05']);
    //     const [bob,herman] = result.rows;
    //     return await trx.raw(/*sql*/`INSERT INTO students (name,level,date_of_birth,teacher_id)
    //        VALUES (?,?,?,?),(?,?,?,?),(?,?,?,?),(?,?,?,?),(?,?,?,?),(?,?,?,?)
    //     `,[ 'Peter',25,'1995-05-15',bob.id,
    //         'John',20,'1985-06-16',bob.id,
    //         'Simon',15,'1987-07-17',null,
    //         'Andy',5,'2005-05-15',bob.id,
    //         'Cesar',10,'1999-06-16',herman.id,
    //         'Danny',30,'1997-07-01',herman.id
    //     ]);
    // });
    const trx = await knex.transaction(); // BEGIN
    try {
        const result = await trx.raw(/*sql*/`INSERT INTO teachers (name,date_of_birth) 
            VALUES (?,?),(?,?) RETURNING id`, ['Bob 123', '1970-01-01', 'Herman 456', '1971-05-05']);
        const [bob, herman] = result.rows;
        await trx.raw(/*sql*/`INSERT INTO students (name,level,date_of_birth,teacher_id)
            VALUES (?,?,?,?),(?,?,?,?),(?,?,?,?),(?,?,?,?),(?,?,?,?),(?,?,?,?)
            `, ['Peter', 25, '1995-05-15', bob.id,
            'John', 20, '1985-06-16', bob.id,
            'Simon', 15, '1987-07-17', null,
            'Andy', 5, '2005-05-15', bob.id,
            'Cesar', 10, '1999-06-16', herman.id,
            'Danny', 30, '1997-07-01', herman.id
        ]);
        await trx.commit(); // COMMIT
    } catch (err) {
        console.log(err);
        await trx.rollback(); // ROLLBACK
    }
    await knex.destroy();
}

// main();
main2();

//    teacher  student 
// 1. jason peter
// 2. jason mary

// init teacherMap = {};
// loop over rows

// 1. jason peter
// teacherMap["jason"] = { id, name, students: [peter] }

// 2. jason mary
// teacherMap["jason"].students.push(mary)