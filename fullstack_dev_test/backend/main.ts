import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { UserService } from './services/UserService';
import { UserRouter } from './routers/UserRouter';
import Knex from 'knex';
import dotenv from 'dotenv';

dotenv.config();

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cors({
    origin: [
      'http://localhost:3000',
      'http://dummyapi.io/data/api/user?limit=50',
    ],
    credentials: true
  }))
  
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);

const userService = new UserService(knex);
const userRouter = new UserRouter(userService);

app.use('/api', userRouter.router());

const PORT = process.env.PORT || 8001;

app.listen(PORT, () => {
  console.log(`[info] Listening to Port: ${PORT}`);
})
