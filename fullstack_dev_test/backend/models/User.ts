export interface User {
    id: string;
    userId: string;
    title: string;
    firstName: string;
    lastName: string;
    email: string;
    picture: string;
  }
  