import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable('users', (table) => {
        table.increments();
        table.string('user_id').notNullable()
        table.string('title');
        table.string('first_name');
        table.string('last_name');
        table.string('email');
        table.string('picture');
    })}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('users');
}
