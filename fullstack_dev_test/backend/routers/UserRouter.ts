import express from 'express';
import { UserService } from '../services/UserService';


export class UserRouter {

    constructor(private userService: UserService) { }
  
    router() {
      const router = express.Router();
      router.get('/users', this.getUsers);
    //   router.post('/users/post', this.postUsers);
      router.delete('/users/:id', this.deleteUser);
      return router;
    }

    getUsers = async (req: express.Request, res: express.Response) => {
        try {
            const users = await this.userService.getUsers();
            res.status(200).json( users );
        } catch (e) {

            console.error(e.message);
            res.status(500).json({ message: 'Internal server error' })
        }
    }

    // postUsers = async (req: express.Request, res: express.Response) => {
    //     try {
    //         const users = await this.userService.getUsers();
    //         const completed  = await this.userService.postUsers(users)
    //         res.status(200).json( completed );

    //     } catch (e) {
    //         console.error(e.message);
    //         res.status(500).json({ message: 'Internal server error' })
    //     }
    //   }


    deleteUser = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req.params.id
            if (!userId){
              res.status(401).json({message: 'Unauthorized'});
              return;      
            }

            await this.userService.deleteUser(userId);

            res.status(200).json({ message: 'success' })

        } catch (e) {
            console.error(e.message);
            res.status(500).json({ message: 'Internal server error' });
        }
    }

    }