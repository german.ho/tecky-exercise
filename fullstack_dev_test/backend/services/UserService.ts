import Knex from "knex";
import fetch from 'node-fetch'
import { User } from "../models/User";


export class UserService {
    constructor(private knex: Knex) {
    }

    public getUsers = async () => {
        const res = await fetch(`https://dummyapi.io/data/api/user?limit=50`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'app-id': 'MGYVjzxh6533dd43Hozs'
            },
        })
         const userlist = await res.json();
         const updatedUserList: User[] = []
         userlist.data.map((user: User) => {
            if(!Singleton.getInstance().deletedUserId.includes(user.id)) {
                updatedUserList.push(user)
            }
         })
         return {
            userlist: updatedUserList,
            deleteduserlist: Singleton.getInstance().deletedUserId
         } ;

         //not in use, just place it here
         const memes = (await this.knex.raw(/* sql */`SELECT * FROM users`)).rows;
         console.log(memes)
    }

    // public postUsers = async (users: any) => {
    //     // console.log(users.data[0].email)
    //      let idxArray = []
    //      console.log(users.data.length)

    //      for (let i = 0 ; i < users.data.length; i++){ //use let i instead of for (let of/in)

    //      const result = await this.knex.raw(
    //         /* SQL */`
    //         INSERT INTO users (user_id, title, first_name, last_name, email, picture) VALUES (?, ?, ?, ?, ?, ?) RETURNING id
    //         `, 
    //         [users.data[i].id, users.data[i].title, users.data[i].firstName, users.data[i].lastName, users.data[i].email, users.data[i].picture]
    //     )
    //         idxArray.push(result.rows[0])
 
    //     }
    //         return idxArray
    // }

    public deleteUser = async (userId: string) => {

        Singleton.getInstance().deletedUserId.push(userId)

    }
}

class Singleton {
    private static instance: Singleton;
    public deletedUserId: string[]

    /**
     * The Singleton's constructor should always be private to prevent direct
     * construction calls with the `new` operator.
     */
    private constructor() { }

    /**
     * The static method that controls the access to the singleton instance.
     *
     * This implementation let you subclass the Singleton class while keeping
     * just one instance of each subclass around.
     */
    public static getInstance(): Singleton {
        if (!Singleton.instance) {
            Singleton.instance = new Singleton();
            Singleton.instance.deletedUserId = []
        }

        return Singleton.instance;
    }
}