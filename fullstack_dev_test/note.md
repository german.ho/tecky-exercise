# Fullstack Developer Test

> This is how I get access to the localhost of frontend (React) & backend (Node.js).

## React (Frontend)

> I will use "yarn install" to install all the dependencies listed within package.json in the local node_modules folder.

- bash
- cd frontend
- yarn start
- .env has been added into .gitignore. You can refer to .env.sample, and add the REACT_APP_BACKEND_URL to your .env.

## Node.js (Backend)

> I will use "yarn install" to install all the dependencies listed within package.json in the local node_modules folder.

- bash
- cd backend
- npx ts-node main.ts
