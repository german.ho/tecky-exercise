import { ThunkDispatch } from '../../store';
import { loadUsers, loadingUsers } from './action';

export function fetchUsers() {
    return async (dispatch: ThunkDispatch) => {
      dispatch(loadingUsers())
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/users`)
      const json = await res.json();
      dispatch(loadUsers(json.userlist))
      console.log(json.userlist)
    }
  }
  
  export function deleteUsers(userId: string){
    return async (dispatch: ThunkDispatch) => {
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/users/${userId}`,{
        method: 'DELETE',
        credentials: 'include',
      })
      const json = await res.json();
      dispatch(fetchUsers())
    }
  }