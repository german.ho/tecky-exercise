import { UserState, User } from "./reducer"

export function loadingUsers() {
    return {
        type: '@@USERS/LOADING_USERS' as '@@USERS/LOADING_USERS'
    }
}

export function loadUsers(users: User[]) {
    return {
        type: '@@USERS/LOAD_USERS' as '@@USERS/LOAD_USERS',
        users
    }
}

export type UserActions =
ReturnType<typeof loadUsers> |
ReturnType<typeof loadingUsers> 
