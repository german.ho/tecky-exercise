import produce from "immer";
import { UserActions } from "./action";


export interface User {
    id: string;
    title: string;
    firstName: string;
    lastName: string;
    email: string;
    picture: string;
}

export interface UserState {
    users: User[],
    isLoading: boolean
}

const initialState: UserState = {
    users: [],
    isLoading: false
}


export const UserReducer = (state: UserState = initialState, action: UserActions ): UserState => { 

        switch (action.type) {
            case '@@USERS/LOAD_USERS':
                return {
                    ...state,
                    users: action.users,
                    isLoading: false
                }
            case '@@USERS/LOADING_USERS':
                return {
                    ...state,
                    isLoading: true
                }
            
        }
        return state;
    }

    