import React, { useEffect, useState } from 'react';
import { RootState } from '../store';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUsers, deleteUsers } from '../redux/user/thunk';
import { withStyles, Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import LinearProgress from '@material-ui/core/LinearProgress';

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }),
)(TableCell);

const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }),
)(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
  progressBar: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0
  }
});

export function User() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const users = useSelector((state: RootState) => state.users.users)
  const isLoading = useSelector((state: RootState) => state.users.isLoading)

  useEffect(() => {
    dispatch(fetchUsers())
  }, [])

  return (
    <TableContainer component={Paper}>
      {
        (isLoading)
        ? <LinearProgress className={classes.progressBar} />
        : null
      }
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
          <StyledTableCell align="right">id</StyledTableCell>
            <StyledTableCell align="right">Title</StyledTableCell>
            <StyledTableCell align="right">First Name</StyledTableCell>
            <StyledTableCell align="right">Last Name</StyledTableCell>
            <StyledTableCell align="right">Email</StyledTableCell>
            <StyledTableCell align="right">Picture</StyledTableCell>
            <StyledTableCell align="right">Button</StyledTableCell>
          </TableRow>
        </TableHead>

        <TableBody>
          {users?.map((user) => (
            <StyledTableRow key={user.id}>
              <StyledTableCell align="right">{user.id}</StyledTableCell>
              <StyledTableCell align="right">{user.title}</StyledTableCell>
              <StyledTableCell align="right">{user.firstName}</StyledTableCell>
              <StyledTableCell align="right">{user.lastName}</StyledTableCell>
              <StyledTableCell align="right">{user.email}</StyledTableCell>
              <StyledTableCell align="right"><img src={user.picture} alt="picture" width="40" height="40"></img></StyledTableCell>
              <StyledTableCell align="right"><button onClick={() => dispatch(deleteUsers(user.id))}>Delete</button></StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}


