import React from 'react';
import Board, {important} from './Board';
import './App.css';

function App() {
  return (
    <div className="game">
      <div className="game-board">
        <Board />
      </div>
      <div className="game-info">
        <div>{important}</div>
      </div>
    </div>
  );
}

export default App;
