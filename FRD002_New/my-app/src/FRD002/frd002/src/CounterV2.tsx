import React, { useState } from "react";

interface IProps {
  initNum: number;
}

const CounterV2: React.FC<IProps> = (props) => {
  const [num, setNum] = useState(props.initNum);
  return (
    <div>
      {props.children}
      <h1>{num}</h1>
      <button onClick={() => setNum(num + 1)}>Click Me!!!</button>
    </div>
  );
};

export default CounterV2;
