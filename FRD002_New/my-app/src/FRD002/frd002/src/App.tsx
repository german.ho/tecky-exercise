import React, { useState } from "react";
import "./App.css";

// import {Counter} from './Counter';
// import CounterV2 from './CounterV2';
import CounterV3 from "./CounterV3";

function App() {
  const [numArr, setNumArr] = useState([10, 15, 20]);
  return (
    <div className="App">
      {/* <Counter intiNum={10}/>
      <CounterV2 initNum={15}>
        <h1>Jason is Handsome</h1>
      </CounterV2> */}
      {/* <CounterV3 num={num} setNum={setNum}/>
      <CounterV3 num={num} setNum={setNum}/> */}
      {numArr.map((num, idx) => (
        <CounterV3
          key={`num_${idx}`}
          num={num}
          setNum={(num: number) => {
            const newNumArr = numArr.slice();
            newNumArr[idx] = num;
            setNumArr(newNumArr);
          }}
        />
      ))}
    </div>
  );
}

export default App;
