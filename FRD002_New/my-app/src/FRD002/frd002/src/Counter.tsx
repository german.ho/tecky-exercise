import React, { Component } from "react";

interface IProps {
	intiNum: number;
}

interface IState {
  num: number;
}

export class Counter extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      num: props.intiNum,
    };
  }

  render() {
    return (
      <div>
		    {/* <h1>INIT: {this.props.intiNum}</h1> */}
        <h1>{this.state.num}</h1>
        <button onClick={() => this.setState({ num: this.state.num + 1 })}>
          Click Me
        </button>
      </div>
    );
  }
}
