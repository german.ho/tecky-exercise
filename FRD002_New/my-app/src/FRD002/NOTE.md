# FRD002

## Components

- Class
- Functional (Traditional)
- Functional (React-hooks)

### Class

- props
- state
- life cycle

### Functional (Traditional)

- props

### Functional (React-hooks)

- props
- state (`useState()`)
- life cycle (`useEffect()` to perform some functions)

Class > FRH > FT
