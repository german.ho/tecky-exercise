import React from "react";
import {Button} from 'react-bootstrap';

interface IProps {
  num: number;
  setNum: (num: number) => void;
}

const CounterV3: React.FC<IProps> = (props) => {
//   const [isJasonHandsome, setIsJasonHandsome] = useState(false);
  console.log(`This number is ${props.num}`);
  return (
    <div>
      {props.children}
      <h1>{props.num}</h1>
      <Button variant="primary" onClick={() => props.setNum(props.num + 1)}>Click Me!!!</Button>
      {/* <button onClick={() => setIsJasonHandsome(true)}>
        Click Me and Jason is Handsome
      </button> */}
    </div>
  );
};

export default CounterV3;
