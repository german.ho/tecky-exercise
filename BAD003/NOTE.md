# BAD003

## Basic Command

**login into DB (system user):**

```Bash
psql
```

**show database list:**

```Text
\l
```

**change database;**

```Text
\c <database-name>
```

**list of relations:**

```Text
\d
```

**check table structure:**

```Text
\d+ <table-name>
```

## SQL

### Create database

**Syntax:**

```Text
CREATE DATABASE <database-name>;
```

**Example:**

```SQL
CREATE DATABASE tecky;
```

**Result:**

```Text
CREATE DATABASE
```

### Drop database

**Syntax:**

```Text
DROP DATABASE <database-name>;
```

**Example:**

```SQL
DROP DATABASE tecky;
```

**Result:**

```Text
DROP DATABASE
```

### Create Table

**Syntax:**

```Text
CREATE TABLE <table-name> (
  <columns....>
);
```

**Example:**

```SQL
CREATE TABLE students(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    level INTEGER NOT NULL,
    date_of_birth date
);
```

**Result:**

```Text
CREATE TABLE
```

### Drop Table

**Syntax:**

```Text
DROP TABLE <table-name>;
```

**Example:**

```SQL
DROP TABLE users;
```

**Result:**

```Text
DROP TABLE
```

### ALTER

**Syntax:**

```Text
// Alter Table Name:
ALTER TABLE <Old Table Name> RENAME TO <New Table Name>;

// Modify Column:
ALTER TABLE students ALTER COLUMN <Column Name> ...;
ALTER TABLE students RENAME COLUMN <Old Name> TO <New Name>;

// Add / Remove Column
// Remove Column:
ALTER TABLE students DROP COLUMN <Column Name>;

// Add Column:
ALTER TABLE students ADD COLUMN <Column Name>...;
```

**Example:**

```SQL
ALTER TABLE students ALTER COLUMN level TYPE VARCHAR(255);

ALTER TABLE students DROP COLUMN level;
ALTER TABLE students ADD COLUMN level INTEGER NOT NULL;
```

### Insert Record

**Syntax:**

```Text
INSERT INTO <table-name> (<columns>...) VALUES (<values>...);
```

**Example:**

```SQL
INSERT INTO students (name,level,date_of_birth) VALUES ('Peter',25,'1995-05-15');
INSERT INTO students (name,level,date_of_birth) VALUES ('John',10,'1985-06-16');
INSERT INTO students (name,level,date_of_birth) VALUES ('Simon',20,'1987-07-17');

INSERT INTO students (name,level,date_of_birth) VALUES ('Mary',25,'1995-05-15'), ('May',10,'1985-06-16'), ('Michelle',20,'1987-07-17');
```

## Retrieve Record

**Syntax:**

```Text
SELECT * FROM students;
SELECT * FROM students WHERE ... ;
SELECT * FROM students WHERE ... AND ... ;
SELECT * FROM students WHERE ... OR ... ;
```

**Example:**

```SQL
SELECT * FROM students;
```

**Result:**

```Text
 id |   name   | date_of_birth | level
----+----------+---------------+-------
  1 | Peter    | 1995-05-15    |    25
  2 | John     | 1985-06-16    |    10
  3 | Simon    | 1987-07-17    |    20
  4 | Mary     | 1995-05-15    |    25
  5 | May      | 1985-06-16    |    10
  6 | Michelle | 1987-07-17    |    20
(6 rows)
```

### Update

**Syntax:**

```Text
UPDATE <Table Name> SET <Column Name> =  <Value>;
UPDATE <Table Name> SET <Column Name> =  <Value> WHERE <Condition>;
```

**Example:**

```SQL
UPDATE students SET level = 18 WHERE id = 2;
```

**Result:**

```Text
UPDATE 1
```

### Delete Record

**Syntax:**

```Text
DELETE FROM <table-name>;
DELETE FROM <table-name> WHERE <Condition>;
```

**Example:**

```SQL
DELETE FROM students WHERE level > 20;
```

## Remark

/etc/postgresql/10/main/postgresql.conf

```Text
fsync = off
data_sync_retry = true
```

### Restart psql Service

```Bash
sudo service postgresql restart
sudo service postgresql status
```
