# Exercise

## Exercise 1

Using raw SQL statements, create a database named fileserver and below tables:

- [x] user

The user table should consist of below fields:

    id
    username
    password
    level
    created
    updated

- [x] file

The file table should consist of below fields:

    id
    name
    isDirectory
    content
    created
    updated

- [x] category

The category table should consist of below fields:

    id
    name
    created
    updated

- [x] membership

The membership table should consist of below fields:

    id
    name
    price
    created
    updated

- [x] others

After you created all the tables above, please do the following:

    Add a field icon to category
    Rename field isDirectory to isFile
    Drop the table membership

## Exercise 2

**INSTALL KING:**

```Bash
yarn init -y
yarn add typescript ts-node @types/node
touch tsconfig.json index.js ex2.ts

yarn add pg @types/pg
yarn add xlsx @types/xlsx
```

