# Beauty of Programming

## Array

Array is a **contiguous memory block** allocated by the operating system to your program. As a result, you can access every element by just referring the index.

- Fixed Length
- Data Type

## Linked List

Like arrays, Linked List is a linear data structure. Unlike arrays, linked list elements are not stored at a contiguous location; the elements are linked using pointers.

### Singly Linked List

```Javascript
// @ts-check

/**
 * @typedef MyNode
 * @property {number} val
 * @property {MyNode} next
 */

/**
*
* @param {number} val
* @returns {MyNode}
*/
const createNode = (val) => ({ val, next: null });
const createLinkedList = () => {
  /** @type { MyNode | null } */
  let head = null;
  return Object.freeze({
    /**
     * @param {MyNode} node
     */
    insert: (node) => {
      if (head === null) {
        head = node;
      } else {
        let curNode = head;
        while (curNode.next !== null) {
          curNode = curNode.next;
        }
        curNode.next = node; // lastNode
      }
    },
    /**
     * @param {number} val
     */
    delete: (val) => {
      let curNode = head;
      let prevNode = null;
      let isFound = false;
      while (curNode.next !== null) {
        if (curNode.val === val) {
          isFound = true;
          break;
        }
        prevNode = curNode;
        curNode = curNode.next;
      }
      // console.log(prevNode.val);
      // console.log(curNode.val);
      if (isFound) {
        if (prevNode === null) {
          head = curNode.next;
        } else {
          prevNode.next = curNode.next;
        }
        return curNode.val;
      } else {
        return null;
      }
    },
    displayAll: () => {
      console.log("Display ALLLLLLL");
      let curNode = head;
      while (curNode.next !== null) { // <-- what if list is empty
        console.log(curNode.val)
        curNode = curNode.next;
      }
      console.log(curNode.val); // lastNode;
    },
    head: () => {
      // console.log(head.val);
      return head;
    }
  });
}

const linkedList = createLinkedList();

linkedList.insert({ val: 10, next: null });
linkedList.head();
linkedList.insert({ val: 15, next: null });
linkedList.insert({ val: 25, next: null });
linkedList.insert({ val: 13, next: null });
linkedList.displayAll();

console.log(linkedList.delete(10));
linkedList.displayAll();

const linkedList2 = createLinkedList();
linkedList2.insert(createNode(101));
linkedList2.insert(createNode(11));
linkedList.insert(linkedList2.head());
linkedList.displayAll();
```

### Doubly Linked List

```Javascript
//@ts-check

/**
 * @typedef MyNode
 * @property {number} val
 * @property {MyNode} prev
 * @property {MyNode} next
 */

/**
 * @typedef DoublyLinkedList
 * @property {MyNode} head
 * @property {MyNode} tail
 */

/**
*
* @param {number} val
* @returns {MyNode}
*/
const createNode = (val) => ({ val, prev: null, next: null });
const createDoublyLinkedList = () => {

  const doublyLinkedList = { head: null, tail: null };
  return Object.freeze({
    head: () => {
      console.log(doublyLinkedList.head);
      return doublyLinkedList.head;
    },
    /**
     * @param {MyNode} node
     */
    insert: (node) => {
      // let node = createNode(val);
      let lastNode = doublyLinkedList.tail;
      if(lastNode === null) {
        doublyLinkedList.head = node;
        doublyLinkedList.tail = node;
        return;
      }
      lastNode.next = node;
      node.prev = lastNode;
      doublyLinkedList.tail = node;
    },
    /**
     * @param {number} val
     */
    delete: (val) => {
      let curNode = doublyLinkedList.head;
      // let prevNode = null;
      let isFound = false;
      if (curNode === null) {
        // empty
        return null;
      }
      while (curNode.next !== null) {
        if (curNode.val === val) {
          isFound = true;
          break;
        }
        // prevNode = curNode;
        curNode = curNode.next;
      }
      if (isFound) {
        if (curNode.prev === null) { // delete first Node
          doublyLinkedList.head = curNode.next;
          doublyLinkedList.head.prev = null;
        } else if (curNode.next === null) { // delete last node
          doublyLinkedList.tail = curNode.prev;
          doublyLinkedList.tail.next = null;
        } else {
          curNode.next.prev = curNode.prev;
          curNode.prev.next = curNode.next;
        }
        return curNode.val;
      } else {
        return null;
      }
    },
    travelFor: () => {
      console.log('Forward start');
      let current = doublyLinkedList.head;
      if (current !== null) {
        while (current.next !== null) {
          console.log(current.val);
          current = current.next;
        }
        console.log(current.val); // Last
      } else {
        console.log('list is empty');
      }
    },
    travelBack: () => {
      console.log('Backward start');
      let current = doublyLinkedList.tail;
      if (current !== null) {
        while (current.prev !== null) {
          console.log(current.val);
          current = current.prev;
        }
        console.log(current.val);
      } else {
        console.log('list is empty');
      }
    }
  });
}

const doublyLinkedList = createDoublyLinkedList();
doublyLinkedList.insert(createNode(10));
doublyLinkedList.insert(createNode(15));
doublyLinkedList.insert(createNode(25));
doublyLinkedList.insert(createNode(13));
doublyLinkedList.head();

// doublyLinkedList.travelFor();
// doublyLinkedList.travelBack();

doublyLinkedList.delete(25);
doublyLinkedList.travelFor();
doublyLinkedList.travelBack();
```

## Stack

Stack is sometimes called a **Last In First Out**. Because the most recently pushed elements are the first one to be poped out. Here is a diagram to demonstrate how stack's two main operations push and pop works:

We can use Linked List to form the Stack structure. Or we can use JS array to form the structure.

```Javascript

const createStack = () => {
  let arr = [];
  return Object.freeze({
    push: (val) => {
      arr.push(val);
    },
    pop: () => {
      return arr.pop();
    },
    count: () => arr.length,
    top: () => {
      if (arr.length > 0) {
        return arr[arr.length-1];
      }
      return null;
    },
    empty: () => {
      arr = [];
    }
  });
}

const stack = createStack();
stack.push(10);
stack.push(15);
console.log(stack.top());
console.log(`deleted ${stack.pop()}`);
console.log(stack.top());
stack.empty();
console.log(stack.count());
```

## Queue

Queue is called a **First in first out** instead. Because the most recently enqueued elements are the last ones to be dequeued. Here is a diagram to demonstrate how queues’ two main operations enqueue and dequeue works:

```Javascript
const createQueue = () => {
  let arr = [];
  return Object.freeze({
    enqueue: (val) => {
      arr.push(val);
    },
    dequeue: () => {
      return arr.shift();
    },
    queueFront: () => {
      if (arr.length > 0) {
        return arr[0];
      }
      return null;
    },
    queueRear: () => {
      if (arr.length > 0) {
        return arr[arr.length-1];
      }
      return null;
    }
  });
}

const queue = createQueue();
queue.enqueue(10);
queue.enqueue(15);
queue.enqueue(25);
console.log(`${queue.queueFront()}, ${queue.queueRear()}`);

queue.dequeue();
console.log(`${queue.queueFront()}, ${queue.queueRear()}`);

queue.dequeue();
console.log(`${queue.queueFront()}, ${queue.queueRear()}`);
```

## Tree

Tree is a pretty **common data structures** which simulates a hierarchical tree structure. There are lots of examples of tree structures. Your files are stored in a tree structures with the outermost folder or drive contains all of the folders which in turn contain more sub-folders or files.

A tree always contains a **Root node** which is the entry point of the whole data structure. Each node can have **child nodes**. Each nodes can carry more than one attributes of data.

### Binary Tree

```Javascript
//@ts-check

/**
 *
 * @typedef TreeNode
 * @property {number} val
 * @property {TreeNode} left
 * @property {TreeNode} right
 */


const createNode = (val) => ({ val, left: null, right: null });

/**
 *
 * @param {number} num
 */
const createBinaryTree = (num) => {
  let rootNode = createNode(num);
  return Object.freeze({
    root: () => rootNode,
    insert: (val) => {
      let node = createNode(val);
      let isFound = false;
      let curNode = rootNode;
      while (!isFound) {
        let key = "left";
        if (node.val > curNode.val) {
          key = "right";
        } else if (node.val === curNode.val) {
          console.log("found duplicated val!!!")
          return;
        }
        if (curNode[key] !== null) {
          curNode = curNode[key];
        } else {
          isFound = true;
          curNode[key] = node;
        }
      }
    },
    inOrder: () => {
      /**
       *
       * @param {TreeNode} node
       */
      const display = (node) => {
        if (node !== null) {
          display(node.left);
          console.log(node.val);
          display(node.right);
        }
      }
      display(rootNode);
    },
    preOrder: () => {
      /**
       *
       * @param {TreeNode} node
       */
      const display = (node) => {
        if (node !== null) {
          console.log(node.val);
          display(node.left);
          display(node.right);
        }
      }
      display(rootNode);
    },
  });
}

const binaryTree = createBinaryTree(8);
binaryTree.insert(3);
binaryTree.insert(1);
binaryTree.insert(6);
binaryTree.insert(4);
binaryTree.insert(7);
binaryTree.insert(7);
binaryTree.insert(10);
binaryTree.insert(14);
binaryTree.insert(13);
console.log(binaryTree.root());

binaryTree.preOrder();
// binaryTree.inOrder();
```

## Sort

### Bubble Sort

Bubble-sort is one of the simplest comparison sort. It is called Bubblesort because the smaller elements seem to bubble to the right. It is basically comparing two adjacent elements and swap them if they are out of order. Repeat the whole process on each pair until the elements are sorted. This is one of the possible implementation of bubble sort fetched from Wikipedia.

```Javascript
function bubbleSort(arr) {
	const sortedArr = arr.slice(0); // clone
  let swapped = true;
  while(swapped){
    swapped = false;
    for (let i = 1; i < sortedArr.length; ++i) {
      if (sortedArr[i - 1] > sortedArr[i]) {
        [sortedArr[i], sortedArr[i - 1]] = [sortedArr[i - 1], sortedArr[i]];
        swapped = true;
      }
    }
  }
  return sortedArr;
}
```

### QuickSort

In quicksort, we rely on choosing an element called pivot. The pivot divides the element into two group: the partition with elements smaller than the pivot and the partition with elements greater than the pivot. After grouping all the elements into their respective partition . We repeat the partition again on each partition until we finished sorting the whole list.

```Text
[|2|, 4, 9, 8, 7, 1]
[1], 2 , [4, 9, 8, 7]
1, 2, [ |4|, 9, 8, 7]
1, 2, [], 4, [|9|, 8, 7]
1, 2, 4, [|8|, 7], 9, []
1, 2, 4, 7, 8, 9
```

```Javascript
function quicksortBasic(array) {
  if(array.length < 2) {
    return array;
  }

  const pivot = array[0];
  const lesser = [];
  const greater = [];

  for(let i = 1; i < array.length; i++) {
    if(array[i] < pivot) {
      lesser.push(array[i]);
    } else {
      greater.push(array[i]);
    }
  }

  return quicksortBasic(lesser).concat(pivot, quicksortBasic(greater));
}
```

## Search

Binary Search

```Javascript
search: (data) => {
  const searchNode = (node, value) => {
    if (node == null) {
      return null;
    } else {
      if (value < node.data) {
        return searchNode(node.left, value);
      } else if (value > node.data) {
        return searchNode(node.right, value);
      } else {
        return value;
      }
    }
  }
  return searchNode(bst.root, data);
},
```
