//@ts-check

/**
 *
 * @typedef TreeNode
 * @property {number} val
 * @property {TreeNode} left
 * @property {TreeNode} right
 */


const createNode = (val) => ({ val, left: null, right: null });

/**
 *
 * @param {number} num
 */
const createBinaryTree = (num) => {
  let rootNode = createNode(num);
  return Object.freeze({
    root: () => rootNode,
    insert: (val) => {
      let node = createNode(val);
      let isFound = false;
      let curNode = rootNode;
      while (!isFound) {
        let key = "left";
        if (node.val > curNode.val) {
          key = "right";
        } else if (node.val === curNode.val) {
          console.log("found duplicated val!!!")
          return;
        }
        if (curNode[key] !== null) {
          curNode = curNode[key];
        } else {
          isFound = true;
          curNode[key] = node;
        }
      }
    },
    inOrder: () => {
      /**
       *
       * @param {TreeNode} node
       */
      const display = (node) => {
        if (node !== null) {
          display(node.left);
          console.log(node.val);
          display(node.right);
        }
      }
      display(rootNode);
    },
    preOrder: () => {
      /**
       *
       * @param {TreeNode} node
       */
      const display = (node) => {
        if (node !== null) {
          console.log(node.val);
          display(node.left);
          display(node.right);
        }
      }
      display(rootNode);
		},
		/**
		 * @param {number} val
		 */
		search: (val) => {
			/**
			 *
			 * @param {TreeNode} node
			 * @param {number} value
			 */
			const searchNode = (node, value) => {
				if (node === null) {
					return null;
				} else {
					if (value < node.val) {
						return searchNode(node.left, value);
					} else if (value > node.val) {
						return searchNode(node.right, value);
					} else {
						return value;
					}
				}
			}
			return searchNode(rootNode, val);
		},
  });
}

const binaryTree = createBinaryTree(8);
binaryTree.insert(3);
binaryTree.insert(1);
binaryTree.insert(6);
binaryTree.insert(4);
binaryTree.insert(7);
binaryTree.insert(7);
binaryTree.insert(10);
binaryTree.insert(14);
binaryTree.insert(13);
console.log(binaryTree.root());

// binaryTree.preOrder();
// binaryTree.inOrder();

console.log(binaryTree.search(16));