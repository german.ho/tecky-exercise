import React, { useState } from 'react';
import { useFormState } from 'react-use-form-state';
import { useDispatch, useSelector } from 'react-redux';
import { createMemo } from './memos/thunk';
import { RootState } from './store';

export const CreateMemo = () => {
  const [formState, { textarea }] = useFormState()
  const submitting = useSelector((state: RootState) => state.memos.submitting)
  const [photo, setPhoto] = useState<undefined | File>(undefined);

  const dispatch = useDispatch();

  return (
    <form onSubmit={async event => {
      event.preventDefault();

      dispatch(createMemo(formState.values.content, photo));
      formState.reset();
    }}>
      <textarea { ...textarea('content') }></textarea>
      <input type="file" accept="image/*" onChange={event => setPhoto(event.currentTarget.files?.[0])} />
      <input disabled={submitting} type="submit" value="貼" />
    </form>
  )
}