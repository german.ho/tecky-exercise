import React, { useState, useEffect, useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from './store';
import { loadMemo, editMemo } from './memos/action';
import { CreateMemo } from './CreateMemo';
import { LoginForm } from './LoginForm';
import { Role } from './user/reducer';
import { useFormState } from 'react-use-form-state';
import { fetchMemo, createMemo } from './memos/thunk';

export const Home = () => {
  const [formState, { textarea }] = useFormState();
  const submitting = useSelector((state: RootState) => state.memos.submitting);
  const role = useSelector((state: RootState) => state.user.role);
  const memos = useSelector((state: RootState) => state.memos.memos)
  const [editing, setEditing] = useState<null | number>(null);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMemo());
  }, [dispatch])

  return (
    <>
      <button onClick={() => {
        dispatch(createMemo("光復香港～～"))
      }}>特快光復香港</button>
      <div className={`memos ${submitting ? "submitting" : ""}`}>
        {memos.map((memo, i) => (
          <div>
            <div className="content">
              {editing === i ? <textarea {...textarea('content')} onBlur={async () => {
                await fetch('http://localhost:8080/memos/' + i, {
                  method: 'PUT',
                  headers: {
                    'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                    content: formState.values.content
                  }),
                  credentials: "include"
                })
                dispatch(editMemo(i, formState.values.content))
                setEditing(null);
              }}></textarea> : memo.content}
            </div>
            { memo.image != null && <img src={"http://localhost:8080/" + memo.image} />}
            { role == Role.ADMIN && (
              <>
                <button onClick={() => {
                  formState.setField('content', memo.content)
                  setEditing(i);
                }} className="edit action-button"><i className="fas fa-edit"></i></button>
                <button onClick={async () => {
                  await fetch('http://localhost:8080/memos/' + i, {
                    method: 'DELETE',
                    credentials: "include"
                  });

                  dispatch(fetchMemo());
                }} className="trash action-button"><i className="fas fa-car-crash"></i></button>
              </>
            )}
          </div>
        ))}
      </div>

      <CreateMemo />
    
      <LoginForm />
    
      <form id="upload-image-form">
        <input type="file" name="photo" />
      </form>
    </>
  )
}