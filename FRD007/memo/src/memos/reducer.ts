import { MemosActions } from "./action";

export interface Memo {
  content: string;
  image: string;
}

export interface MemosState {
  memos: Memo[],
  submitting: boolean,
}

const initialState: MemosState = {
  memos: [],
  submitting: false
};

export const memoReducer = (oldState: MemosState = initialState, action: MemosActions): MemosState => {
  switch (action.type) {
    case "@@MEMOS/LOADED_MEMO":
      return {
        ...oldState,
        memos: action.memos
      }
    case "@@MEMOS/EDIT_MEMO":
      const newMemos = oldState.memos.slice();
      newMemos[action.i].content = action.content
      return {
        ...oldState,
        memos: newMemos
      }
    case "@@MEMOS/SUBMITTING":
      return {
        ...oldState,
        submitting: action.isSubmitting
      }
  }
  
  return oldState;
}