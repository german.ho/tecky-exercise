import { ThunkDispatch, RootState } from "../store";
import { loadMemo, setSubmitting } from "./action";

// Thunk Action
export function fetchMemo() {
  return async (dispatch: ThunkDispatch) => {
    const fetchRes = await fetch('http://localhost:8080/memos', {
      credentials: "include"
    }); // GET + 'memos'
    const memos = await fetchRes.json();

    dispatch(loadMemo(memos));
  }
}

export function createMemo(content: string, photo?: File) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    if (getState().memos.submitting) {
      return;
    }

    dispatch(setSubmitting(true))
    setTimeout(() => {
      dispatch(setSubmitting(false))
    }, 5000);

    const formData = new FormData();
    formData.append('content', content);
    if (photo != null) {
      formData.append('photo', photo)
    }
  
    await fetch('http://localhost:8080/memos', {
      method: 'POST',
      body: formData  
    })  

    dispatch(fetchMemo());
  }
}