import { Role } from "./reducer";

export function changeRole(role: Role) {
  return {
    type: '@@USER/CHANGE_ROLE',
    role
  }
}

export type UserActions = ReturnType<typeof changeRole>;