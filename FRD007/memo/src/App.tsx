import React, { useEffect } from 'react';
import { Home } from './Home';
import { Switch, Route } from 'react-router';
import { useDispatch } from 'react-redux';
import { changeRole } from './user/action';
import { Role } from './user/reducer';

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    async function getCurrentUser() {
      const res = await fetch('http://localhost:8080/currentUser', {
        credentials: "include"
      })
      console.log('fetch 的 res', res)
      console.log('fetch 的 res.body', res.body)
    
      const result = await res.json();
      console.log('res.json() 處理後的 result', result)
    
      if (result.role === 'admin') {
        dispatch(changeRole(Role.ADMIN))
      }
    }
    
    getCurrentUser();
  }, [])

  return (
    <div className="App">
      <Switch>
        <Route path="/" exact><Home /></Route>
        <Route>Not Found</Route>
      </Switch>
    </div>
  );
}

export default App;
