import React from 'react';
import { useFormState } from 'react-use-form-state';
import { useDispatch, useSelector } from 'react-redux';
import { Role } from './user/reducer';
import { changeRole } from './user/action';
import { RootState } from './store';

export const LoginForm = () => {
  const [formState, { text, password }] = useFormState();
  const dispatch = useDispatch()
  const role = useSelector((state: RootState) => state.user.role)

  return (
    <>
      <form onSubmit={async event => {
        event.preventDefault();
        const res = await fetch('http://localhost:8080/login', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded', // bodyParser.urlencoded()
            // 'Content-Type': 'application/json' // bodyParser.json()
          },
          body: 'username=' + encodeURIComponent(formState.values.username) + '&password=' + encodeURIComponent(formState.values.password),
          credentials: "include"
          // body: JSON.stringify({username, password})
        })
        const result = await res.json();
      
        if (result.success) {
          dispatch(changeRole(Role.ADMIN));
        } else {
          alert('扮晒蟹，你唔係 Admin!!!!');
        }
      }}>
        <h1>管理員專用</h1>
        <input { ...text("username") } />
        <input { ...password("password") } />
        <input type="submit" value="登入" />
      </form>
    
      { role == Role.ADMIN && (
        <button onClick={async () => {
          await fetch('http://localhost:8080/logout', {
            method: 'POST',
            credentials: "include"
          })
          dispatch(changeRole(Role.GUEST));
        }}>登出</button>
      ) }
    </>
  )
}