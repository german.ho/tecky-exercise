import React from 'react';
import './App.scss';
import Profile from './Profile';
import TodosRedux from './TodosRedux';
import Nav from './Nav';
import { Route, Switch } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Nav />
      <Switch>
        <Route path="/" exact>你好</Route>
        <Route path="/profile"><Profile /></Route>
        <Route path="/準備做嘢清單/:id"><TodosRedux /></Route>
        <Route path="/準備做嘢清單/"><TodosRedux /></Route>
        <Route>404 找不到喎</Route>
      </Switch>
    </div>
  );
}

export default App;
