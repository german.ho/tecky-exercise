import { ThunkDispatch } from "../store";
import { loadTodos } from "./actions";

// Thunk Action Creator
export function fetchTodo(projectId: string) {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/todos?projectId=` + projectId);
    const json = await res.json();

    dispatch(loadTodos(projectId, json));
  }
}