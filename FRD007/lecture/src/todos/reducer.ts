import { TodosActions } from "./actions"

export interface Todo {
  id: number;
  item: string;
  completed: boolean;
  projectId: number;
}

export interface TodoState {
  todos: {
    [id: string]: Todo
  },
  todosByProjectId: {
    [id: string]: number[]
  }
}

// immutability

const initialState: TodoState = {
  todos: {},
  todosByProjectId: {}
}

export const todosReducer = /* reducer */ (oldState = initialState, action: TodosActions) => {
  switch (action.type) {
    case 'LOADED_TODOS':
      {
        const newTodos = {...oldState.todos};

        for (let todo of action.todos) {
          newTodos[todo.id] = todo
        }

        const newTodosByProjectId = {...oldState.todosByProjectId}
        newTodosByProjectId[action.projectId] = action.todos.map(todo => todo.id)

        return {
          ...oldState,
          todos: newTodos,
          todosByProjectId: newTodosByProjectId
        };
      }
    case 'COMPLETE_TODO':
      {
        const newTodos = {...oldState.todos};
        newTodos[action.i].completed = action.completed
        return {
          ...oldState,
          todos: newTodos,
        };
        // return {
        //   ...oldState,
        //   todos: {
        //     ...oldState.todos,
        //     [action.i]: {
        //       ...oldState.todos[action.i],
        //       completed: action.completed
        //     }
        //   },
        // };
      }
  }
  return oldState
}