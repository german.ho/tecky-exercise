import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import { todosReducer, TodoState } from './todos/reducer';
import { userReducer, UserState } from './user/reducer';
import { projectsReducer, Project } from './projects/reducer';
import { createBrowserHistory } from 'history';
import { RouterState, connectRouter, routerMiddleware } from 'connected-react-router';
import thunk, { ThunkDispatch as OldThunkDispatch } from 'redux-thunk';
import { UserActions } from './user/action';
import { TodosActions } from './todos/actions';

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

export type RootAction = UserActions | TodosActions;

export type ThunkDispatch = OldThunkDispatch<RootState, null, RootAction>

export const history = createBrowserHistory();

export interface RootState {
  todos: TodoState,
  user: UserState,
  projects: Project[],
  router: RouterState
}

const reducer = combineReducers<RootState>({
  todos: todosReducer,
  user: userReducer,
  projects: projectsReducer,
  router: connectRouter(history)
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(reducer,
  composeEnhancers(
    applyMiddleware(routerMiddleware(history)),
    applyMiddleware(thunk)
  ));