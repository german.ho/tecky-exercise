export function setName(name: string) {
  return {
    type: '@@USER/SET_NAME' as '@@USER/SET_NAME',
    name
  }
}

export type UserActions = ReturnType<typeof setName>;