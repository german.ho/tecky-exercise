import React, { useEffect, useCallback } from 'react'
import { useFormState } from 'react-use-form-state';
import { useSelector, useDispatch } from 'react-redux';
import { loadTodos, completeTodo } from './todos/actions';
import { RootState } from './store';
import useReactRouter from 'use-react-router';
import { push } from 'connected-react-router';
import { fetchTodo } from './todos/thunk';

function TodosRedux() {
  const router = useReactRouter<{id: string}>();
  const projectId = router.match.params.id;
  const [formState, { text }] = useFormState();
  const todoIds = useSelector((rootState: RootState) => projectId ? rootState.todos.todosByProjectId[projectId] : null)
  const todos = useSelector((rootState: RootState) => todoIds?.map(id => rootState.todos.todos[id]))
  const dispatch = useDispatch();
  const projects = useSelector((state: RootState) => state.projects)
  
  async function onTodoClick(i: number, event: React.MouseEvent<HTMLInputElement, MouseEvent>) {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/todos/` + i, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        completed: event.currentTarget.checked ? "true" : "false"
      })
    });
    const json = await res.json();

    dispatch(completeTodo(i, json.completed))
  }

  useEffect(() => {
    dispatch(fetchTodo(projectId))
  }, [projectId])

  if (projectId === '' || projectId == null) {
    return (<>
      <h2>Todos</h2>
      <select value={projectId} onChange={event => dispatch(push(`/準備做嘢清單/${event.currentTarget.value}`))}>
        <option></option>
        {projects.map(project => <option key={project.id} value={project.id}>{project.name}</option>)}
      </select>

      請選擇 project
    </>);
  }

  return (
    <>
      <h2>Todos</h2>
      <select value={projectId} onChange={event => dispatch(push(`/準備做嘢清單/${event.currentTarget.value}`))}>
        <option></option>
        {projects.map(project => <option key={project.id} value={project.id}>{project.name}</option>)}
      </select>

      <form onSubmit={async (event) => {
        event.preventDefault();

        await fetch('http://localhost:8000/todos', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            ...formState.values,
            projectId: projectId
          })
        })

        dispatch(fetchTodo(projectId))
      }}>
        <input {...text('item')} />
        <input type="submit" value="加！" />
      </form>

      <div>
        {todos && todos.map(todo => <div key={todo.id}><input type="checkbox" readOnly checked={todo.completed} 
           onClick={onTodoClick.bind(null, todo.id)} /> {todo.item}</div>)}
      </div>
    </>
  )
}

export default TodosRedux