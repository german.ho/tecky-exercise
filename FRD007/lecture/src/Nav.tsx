import React from 'react'
import { connect } from 'react-redux'
import { Todo } from './todos/reducer'
import { RootState } from './store';
import { NavLink } from 'react-router-dom';

// function Nav(props: {
//   onPageChange: (newPage:string) => void;
// }) {
//   const todos = useSelector((rootState: Todo[]) => rootState)

//   return (
//     <div>
//       <button onClick={() => props.onPageChange('profile')}>Profile</button>
//       <button onClick={() => props.onPageChange('todos')}>Todos ({todos.filter(todo => todo.completed).length}/{todos.length})</button>
//     </div>

//   )
// }

interface Props {
  todos: Todo[];
  username: string | null;
}

class Nav extends React.Component<Props> {
  render() {
    return (
      <div className="nav">
        <NavLink activeClassName="active" to="/profile">Profile</NavLink>
        <div></div>
        <NavLink activeClassName="active" to="/準備做嘢清單">Todos ({this.props.todos.filter(todo => todo.completed).length}/{this.props.todos.length})</NavLink>
        <div>早晨{this.props.username}，你好</div>
      </div>
    );
  }
}

export default connect(/* mapStateToProps */ (rootState: RootState) => {
  return {
    todos: Object.values(rootState.todos.todos),
    username: rootState.user.name
  }
})(Nav)