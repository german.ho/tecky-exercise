import React, { useEffect } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom'
import useReactRouter from 'use-react-router'
// # Function

export const funcComp = () => {
  // mount + unmount
  useEffect(() => {
    // fetch

    return () => {
      // clearTimeout
      // clearInterval
    }
  }, []);
  
  // mount + update (跳頁) + unmount
  const router = useReactRouter<{id: string}>();
  const projectId = router.match.params.id;

  useEffect(() => {
    // further fetch

    return () => {
      // clearTimeout
      // clearInterval
    }
  }, [projectId])
}














// # Class

class ClassComp extends React.Component<RouteComponentProps<{id: string}>> {
  // mount
  componentDidMount() {
    // fetch
  }  

  // update (跳頁)
  componentDidUpdate(prevProps) {
    if (prevProps.match.params.id !== this.props.match.params.id) {
      // further fetch
    }
  }

  // unmount
  componentWillMount() {
    // clearTimeout
    // clearInterval
  }
}

const ConnectedClassComp = withRouter(ClassComp)