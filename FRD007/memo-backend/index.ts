import express from 'express'
import bodyParser from 'body-parser'
import path from 'path'
import multer from 'multer'
import jsonfile from 'jsonfile'
import expressSession from 'express-session';
import cors from 'cors';

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, 'uploads'));
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
})

const upload = multer({storage: storage})

const app = express()

app.use(cors({
  origin: [
    "http://localhost:3000"
  ],
  credentials: true
}))

// 增強 express 的功能
// Middleware (未講)
app.use(/* 放加強器 */ express.static(path.join(__dirname, 'public')) )
app.use(/* 放加強器 */ express.static(path.join(__dirname, 'uploads')) )
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(expressSession({
  secret: 'Tecky Academy teaches typescript',
  resave:true,
  saveUninitialized:true
}));

// a=123&b=456
// req.body = {a: '123', b: '456'}

// app.get('/', async (req, res) => {
//   // 嘔 json file 俾 res
//   try { // 搵 runtime error
//     const memos = await jsonfile.readFile('./memos.json')
  
//     const header = `<!DOCTYPE html>
//     <html lang="en">
//     <head>
//       <meta charset="UTF-8">
//       <meta name="viewport" content="width=device-width, initial-scale=1.0">
//       <title>網上陳百牆</title>
//       <link href="index.css" rel="stylesheet">
//     </head>
//     <body>
//       <div class="memos">`;

//     let memoHTML = '';
//     for (const memo of memos) {
//       memoHTML += `<div>${memo.content}`;
//       if (memo.image != null) {
//         memoHTML += `<img src="${memo.image}">`
//       }
//       memoHTML += `</div>`
//     }

//     const footer = `
//       </div>
//       <form action="/memos" method="POST" enctype="multipart/form-data">
//         <textarea name="content"></textarea>
//         <input type="file" name="photo">
//         <input type="submit" value="貼">
//       </form>
//     </body>
//     </html>`

//     res.header('Content-Type', 'text/html')
//     res.end(header + memoHTML + footer);
//   } catch (e) {
//     console.error(e);
//     res.status(500).json({result: false});
//   }
// })

app.get('/currentUser', async (req, res) => {
  if (req.session && req.session.isAdmin) {
    res.json({role: 'admin'})
  } else {
    res.json({role: 'guest'})
  }
});

app.get('/memos', async (req, res) => {
  try {
    const memos = await jsonfile.readFile('./memos.json');

    res.json(memos);
  } catch (e) {
    console.error(e);
    res.status(500).json({success: false})
  }
})

app.post('/login', (req, res) => {
  if (req.body.username === 'alex' && req.body.password === '123456') {
    if (req.session) {
      req.session.isAdmin = true;
    }
    res.json({success: true})
  } else {
    res.json({success: false})
  }
})

app.put('/memos/:id', async (req, res) => {
  try {
    if (!(req.session && req.session.isAdmin)) {
      res.status(401).json({success: false, message: 'unauthorized'})
      return;
    }
    const memos = await jsonfile.readFile('./memos.json')

    memos[req.params.id].content = req.body.content

    await jsonfile.writeFile('./memos.json', memos);

    res.json({success: true})
  } catch (e) {
    console.error(e);
    res.status(500).json({success: false});
  }
})

app.put('/memos/:id/image', upload.single('photo'), async (req, res) => {
  try {
    if (!(req.session && req.session.isAdmin)) {
      res.status(401).json({success: false, message: 'unauthorized'})
      return;
    }
    const memos = await jsonfile.readFile('./memos.json')

    memos[req.params.id].image = req.file.filename;

    await jsonfile.writeFile('./memos.json', memos);

    res.json({success: true})
  } catch (e) {
    console.error(e);
    res.status(500).json({success: false});
  }
})

app.delete('/memos/:id/image', async (req, res) => {
  try {
    if (!(req.session && req.session.isAdmin)) {
      res.status(401).json({success: false, message: 'unauthorized'})
      return;
    }
    const memos = await jsonfile.readFile('./memos.json')

    memos[req.params.id].image = null;

    await jsonfile.writeFile('./memos.json', memos);

    res.json({success: true})
  } catch (e) {
    console.error(e);
    res.status(500).json({success: false});
  }
})

app.delete('/memos/:id', async (req, res) => {
  try {
    if (!(req.session && req.session.isAdmin)) {
      res.status(401).json({success: false, message: 'unauthorized'})
      return;
    }
    const memos = await jsonfile.readFile('./memos.json')

    memos.splice(req.params.id, 1)

    await jsonfile.writeFile('./memos.json', memos);

    res.json({success: true})
  } catch (e) {
    console.error(e);
    res.status(500).json({success: false});
  }
})

app.post('/logout', (req, res) => {
  if (req.session) {
    req.session.destroy(() => {

    });
  }
  res.redirect('/')
})

app.post('/memos', upload.single('photo'), async (req, res) => {
  // 寫入 json file 
  try {
    if (req.body.content == null || req.body.content == '') {
      if (!(req.session && req.session.isAdmin)) {
        res.status(400).json({success: false, message: '無字天書唔接受!'})
        return;
      }
    }

    if (req.session) {
      if (req.session.submission == null) {
        req.session.submission = 0;
      }
      req.session.submission = req.session.submission + 1;

      if (req.session.submission > 5) {
        res.status(400).json({success: false, message: '每人最多寫五次!'})
        return;
      }
    }

    try {
      const memos = await jsonfile.readFile('./memos.json')
    
      memos.push({
        content: req.body.content,
        image: req.file == null ? null : req.file.filename
      })
    
      await jsonfile.writeFile('./memos.json', memos);
    } catch {
      await jsonfile.writeFile('./memos.json', [{
        content: req.body.content,
        image: req.file == null ? null : req.file.filename
      }]);
    }

    res.redirect('/')
    // res.json({msg: "test"});
  } catch (e) {
    console.error(e);
    res.status(500).json({success: false});
  }
})

app.listen(8080, () => {
  console.log('Listening on port 8080')
})