console.log("abc".replace("a", "jason"))

let numInStr = "123"
console.log('value': ${numInStr}, type: ${typeof numInStr})

console.log ("value:", numInStr, "type:", (typeof numInStr))

let num = parseInt(numInStr)
console.log(num, (typeof num))

let a = "abc"// string
let b = 100 // number
let c = a + b

console.log (c)
console.log(typeof c) // -> became string 

console.log("a,b,c,d".split (",")) //--> becomes array 

### Object
const person = {name: "jason", age: 18}
console.log (person) // whole object
console.log (person.name) // jason


let name = person.name// jason, and assign another variable called name - create another box

console.log(person["age"]) // another way, shows 18 

person.name = "Mary"
console.log (person) //change to Mary

name = "Peter" // independent, no change to the object 

const apple = {
    color: "red"
}

apple.color = "yellow"
console.log(apple) // change to yellow

const apple = Object.freeze({
    color: "red"
})

apple.color = "yellow"
console.log(apple) // still red 

const person = {
    name: "Jason"
    age: 18
}

person.hobby = "coding" // add coding in the object
delete person.age // delete age 
// now we have learnt how to access key-value, update value, add key-value, delete key-value, object.freeze


//array - represent a sequence of data
const arr = [1,2,3,4]
const arr = ["a", "b"]
const arr = [ {}, {} {}]

const arr = [10, 11, 12, 13]
console.log(array.length) // show many items
console.log(array[0]) // extract figure
console.log(array.length - 1) // extract last item
array[0] = 100 // change 10 to 100 
arr.push (15) // add 15 to the back
let removedItem = arr.pop (15) 
console.log(arr, removed Item) // separate 15 from the array 

array.unshift (0)
console.log (arr) // to add new element to the front


let removedItem1 = array.shift()
console.log (arr, removedItem1)

//null and undefined - no value
//null: deliberately 
//undefinited: not yet declared

// let a = null 
//console.log (a) -> null value 

//control structure
- //repetition (Loop)
- // condition (if ... else)
- // error handling (try.. catch)

//##if... else...
if (condition 1) {
    statement 1
} else if (condition 2){
    statement 2
    else if (condition 3){
        statement 3
    } else {
        statement N+1
    }
}


//Te
=== // same value and same datatype
== // equals to 
!== // the opposite of ===
!= // the opposite of equals to 
>
<
>=
<=


// switch (name)

//loop 

