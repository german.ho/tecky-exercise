import React from 'react'
import { useSelector } from 'react-redux'
import { Todo, store } from './store'


function Nav(props: {
  onPageChange: (newPage:string) => void;
}) {
  const todos = useSelector((rootState: Todo[]) => rootState)

  return (
    <div>
      <button onClick={() => props.onPageChange('profile')}>Profile</button>
      <button onClick={() => props.onPageChange('todos')}>Todos ({todos.length})</button>
    </div>

  )
}


export default Nav