import React, { useState, useEffect } from 'react'
import { useFormState } from 'react-use-form-state';
import { store } from './naiveStore'

interface Todo {
  item: string
}

function Todos() {
  const [formState, { text }] = useFormState();
  const [todos, setTodos] = useState<Todo[]>([]);

  async function load() {
    const res = await fetch('http://localhost:8000/todos');
    const json = await res.json();

    setTodos(json);
    store.todos = json;
  }

  useEffect(() => {
    load();
  }, [])

  return (
    <>
      <form onSubmit={async (event) => {
        event.preventDefault();

        await fetch('http://localhost:8000/todos', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(formState.values)
        })

        await load();
      }}>
        <input {...text('item')} />
        <input type="submit" value="加！" />
      </form>

      <div>
        {todos.map(todo => <div>{todo.item}</div>)}
      </div>
    </>
  )
}

export default Todos