import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import { useFormState } from 'react-use-form-state'
import Profile from './Profile';
import TodosRedux from './TodosRedux';
import Nav from './Nav';

function App() {
  const [page, setPage] = useState('profile')
  const [name, setName] = useState('')

  return (
    <div className="App">
      <div>早晨{name}，你好</div>
      <Nav onPageChange={(newPage) => {
        setPage(newPage)
      }} />

      {page === 'profile' && <Profile onNameChange={(newName) => {
        setName(newName)
      }} />}
      {page === 'todos' && <TodosRedux /> }
    </div>
  );
}

export default App;
