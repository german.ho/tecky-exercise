import React, { useState, useEffect } from 'react'
import { useFormState } from 'react-use-form-state';
import { useSelector, useDispatch } from 'react-redux';
import { Todo } from './store';

function TodosRedux() {
  const [formState, { text }] = useFormState();
  const todos = useSelector((rootState: Todo[]) => rootState)
  const dispatch = useDispatch();

  async function load() {
    const res = await fetch('http://localhost:8000/todos');
    const json = await res.json();

    dispatch({
      type: 'LOADED_TODOS',
      todos: json
    });

    // setTodos(json);
    // store.todos = json;
  }

  useEffect(() => {
    load();
  }, [])

  return (
    <>
      <form onSubmit={async (event) => {
        event.preventDefault();

        await fetch('http://localhost:8000/todos', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(formState.values)
        })

        await load();
      }}>
        <input {...text('item')} />
        <input type="submit" value="加！" />
      </form>

      <div>
        {todos.map(todo => <div>{todo.item}</div>)}
      </div>
    </>
  )
}

export default TodosRedux