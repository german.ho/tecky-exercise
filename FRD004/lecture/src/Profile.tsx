import React, { useState, useEffect } from 'react'
import { useFormState } from 'react-use-form-state'

function Profile(props: {
  onNameChange: (name: string) => void
}) {
  const [formState, { text, raw }] = useFormState();
  const [file, setFile] = useState<File | undefined>(undefined);

  async function load() {
    const res = await fetch('http://localhost:8000/user');
    const json = await res.json();

    props.onNameChange(json.name)
  }

  useEffect(() => {
    load();
  }, [])

  return (
    <>
      <form onSubmit={async (event) => {
        event.preventDefault();

        const formData = new FormData();
        formData.append('name', formState.values.name)
        if (file) {
          formData.append('photo', file);
        }

        await fetch('http://localhost:8000/user', {
          method: 'POST',
          body: formData
        })
      }}>
        <div>名：<input {...text('name')} /></div>
        <div>相：<input type="file" onChange={event => setFile(event.currentTarget.files?.[0])} /></div>
        <div><input type="submit" value="更新" /></div>
      </form>
    </>
  )
}

export default Profile