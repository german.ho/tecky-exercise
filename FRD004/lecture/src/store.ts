import { createStore } from 'redux'

export interface Todo {
  item: string
}
// immutability

const initialState = [
  {
    item: '123',
  },
  {
    item: '456'
  }
]

export const store = createStore(/* reducer */ (oldState = initialState, action: any) => {
  if (action.type === 'LOADED_TODOS') {
    return action.todos
  }
  return oldState
});