import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import multer from 'multer'

const app = express()

app.use(cors({
  origin: [
    'http://localhost:3000',
    'http://whatthefook.ga'
  ]
}))
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

interface Todo {
  item: string;
}

const todos: Todo[] = []
const user = {
  name: '',
  profile: ''
}

const upload = multer({
  dest: 'uploads/'
})

app.use('/uploads', express.static('uploads'))

app.get('/todos', (req, res) => {
  res.json(todos)
})

app.post('/todos', (req, res) => {
  todos.push({
    item: req.body.item
  })

  res.json({success: true})
})

app.get('/user', (req, res) => {
  res.json(user)
});

app.post('/user', upload.single('photo'), (req, res) => {
  user.name = req.body.name
  user.profile = req.file.filename

  res.json({success: true})
})

const port = process.env.PORT || 8000
app.listen(port, () => {
  console.log('Listening on port ' + port)
})