import React, { useState } from "react";
import Square from "./Square";
import "./Board.css";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "./store";

const Board = () => {
  let player = 'X';
  let 格仔 = useSelector((rootState: RootState) => rootState.squares)
  let oIsNext = useSelector((rootState: RootState) => rootState.oIsNext)

  const winningConditions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ]

  if (oIsNext) {
    player = 'O'
  }

  let winner: string | null = null;
  let wonCondition: number[] | null = null;
  for (const winningCondition of winningConditions) {
    if (格仔[winningCondition[0]] != null &&
      格仔[winningCondition[0]] === 格仔[winningCondition[1]] &&
      格仔[winningCondition[1]] === 格仔[winningCondition[2]]) {
        winner = 格仔[winningCondition[0]];
        wonCondition = winningCondition;
      }
  }

  const dispatch = useDispatch();

  const renderSquare = (i: number, isWin?: boolean) => {
    return <Square on被人㩒咗={() => {
      if (格仔[i] !== null) {
        return;
      }
      if (winner != null) {
        return;
      }
      const 新格仔 = 格仔.slice();
      新格仔[i] = player;

      dispatch({
        type: 'TIC_TAE_TOE_CLICKED',
        square: i
      })

      // set格仔(新格仔);
      // setTurn(turn + 1)
    }} isWin={isWin ? true : false} player={player} value={格仔[i]} key={`square_${i}`} />;
  };

  const status: string = "Next player: " + player;

  return (
    <div>
      <div className="status">{status}</div>
      { winner && <div className="status">{winner} 勝出了，完～</div>}
      { 格仔.indexOf(null) == -1 && <div className="status">SUPER 打和～</div>}
      {[0, 3, 6].map((i, idx) => (
        <div className="board-row" data-testid="row" key={`board-row-${idx}`}>
          {[0, 1, 2].map((j) => renderSquare(i + j, (wonCondition?.indexOf(i + j) ?? -1) > -1))}
        </div>
      ))}
    </div>
  );
};

export default Board;
export const important = "Important! Jason is Handsome";
