import { createStore } from 'redux'

export interface RootState {
  squares: (string | null)[],
  oIsNext: boolean
}

const initialState: RootState = {
  squares: [null,null,null,null,null,null,null,null,null],
  oIsNext: true
}

export const store = createStore((oldState: RootState = initialState, action: any) => {
  if (action.type === 'RESET') {
    return initialState;
  }
  if (action.type === 'TIC_TAE_TOE_CLICKED') {
    const squares = oldState.squares.slice()
    squares[action.square] = oldState.oIsNext ? 'O' : 'X'

    return {
      ...oldState,
      squares: squares,
      oIsNext: !oldState.oIsNext
    };
  }
  return initialState;
})