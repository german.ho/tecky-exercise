import React, { useState } from 'react';
import Board, {important} from './Board';
import './App.css';
import { Provider } from 'react-redux';
import { store } from './store';

function App() {
  const [page, setPage] = useState('board')
  return (
    <Provider store={store}>
      <div className="game">
        <div>
          <button onClick={() => setPage('board')}>Board</button>
          <button onClick={() => setPage('aboutUs')}>邊個整架？</button>
        </div>
        <div className="game-board">
          { page === 'board' && <Board /> }
          { page === 'aboutUs' && <div>Proudly Presented by Jason</div> }
        </div>
        <div className="game-info">
          <div>{important}</div>
        </div>
      </div>
    </Provider>
  );
}

export default App;
