import React, { useState } from "react";
import "./Square.css";

interface ISquareProps {
  value: string | null;
  player: string;
  isWin: boolean;
  on被人㩒咗: () => void;
}

const Square: React.FC<ISquareProps> = (props) => {
  return (
    <button className={`square` + (props.isWin ? ' won' : '')} data-testid="square" onClick={() => {
      props.on被人㩒咗()
    }}>
      {props.value}
    </button>
  );
};

export default Square;
