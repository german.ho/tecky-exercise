# WEF010

## Jewels and Stones.

```Text
/**
 * @param {string} J
 * @param {string} S
 * @return {number}
 */
var numJewelsInStones = function(J, S) {
  let count = 0;
  const jArr = J.split("");
  for (let s of S) {
      if (jArr.includes(s)) {
          count += 1;
      }
  }
  return count;
};

/**
 * @param {string} J
 * @param {string} S
 * @return {number}
 */
var numJewelsInStones = function(J, S) {
    let count = 0;
    const mapping = {};
    for (let s of S) {
        if (s in mapping) {
            mapping[s] += 1;
        } else {
            mapping[s] = 1;
        }
    }
    for (let j of J) {
        if (j in mapping) {
            count += mapping[j];   
        }
    }
    return count;
};
```

## Square

```Text
const mergeSort = (arr) => {
    if (arr.length <= 1) {
        arr[0] = arr[0] * arr[0];
        return arr;
    }
    const middle = Math.floor(arr.length / 2);
    const leftArr = arr.slice(0, middle);
    const rightArr = arr.slice(middle);
    return merge( mergeSort(leftArr), mergeSort(rightArr) );
}

const merge = (leftArr, rightArr) => {
    const result = [];
    let leftIdx = 0;
    let rightIdx = 0;
    while (leftIdx < leftArr.length && rightIdx < rightArr.length) {
        if (leftArr[leftIdx] < rightArr[rightIdx]) {
            result.push(leftArr[leftIdx]);
            leftIdx += 1;
        } else {
            result.push(rightArr[rightIdx]);
            rightIdx += 1;
        }
    }
    return result.concat(leftArr.slice(leftIdx), rightArr.slice(rightIdx));
}

/**
 * @param {number[]} A
 * @return {number[]}
 */
var sortedSquares = function(A) {
    // return A.map(a => a * a).sort((a, b) => a - b);
    // console.log(merge([1, 3], [2, 4]));
    return mergeSort(A);
};
```

## Two Sum

```Text
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
    for (let i = 0; i < nums.length; i++)  {
        for (let j = i + 1; j < nums.length; j++)  {
            if (nums[i] + nums[j] === target) {
                return [i, j];
            }
        }
    }
};

var twoSum = function(nums, target) {
    const mapping = {};
    for (let i = 0; i < nums.length; i++) {
        let remain = target - nums[i]; // 7
        if (remain in mapping) {
            return [mapping[remain], i];
        }
        mapping[nums[i]] = i;
    }
    return [-1, -1];
};
```

## Find Common Char

```Text
/**
 * @param {string[]} A
 * @return {string[]}
 */
var commonChars = function(A) {
  let res = [...A[0]];
  for (let i = 1; i < A.length; i++) {
    res = res.filter(c => {
      const l = A[i].length;
      A[i] = A[i].replace(c, "");
      return l > A[i].length;
    });
  }
  return res;
};

// /**
//  * @param {string[]} A
//  * @return {string[]}
//  */
// var commonChars = function(A) {
//     const mapping = A.map(str => {
//       const tmp = {};
//       for (let s of str) {
//           if (s in tmp) {
//               tmp[s] += 1;
//           } else {
//               tmp[s] = 1;
//           }
//       }
//       return tmp;
//     });
//     const result = [];
//     for (let str in mapping[0]) {
//         let count = mapping[0][str];
//         for (let i = 1; i < mapping.length; i++) {
//             if (!(str in mapping[i])) {
//                 count = 0;
//                 break;
//             }
//             // count = (count <= mapping[i][str]) ? count : mapping[i][str];
//             count = Math.min(count, mapping[i][str]);
//         }
//         for (let j = 0; j < count; j++) {
//             result.push(str);
//         }
//     }
//     return result;
// };

```

