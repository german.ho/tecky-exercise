import React from 'react'
import { connect } from 'react-redux'
import { Todo } from './todos/reducer'
import { RootState } from './store';


// function Nav(props: {
//   onPageChange: (newPage:string) => void;
// }) {
//   const todos = useSelector((rootState: Todo[]) => rootState)

//   return (
//     <div>
//       <button onClick={() => props.onPageChange('profile')}>Profile</button>
//       <button onClick={() => props.onPageChange('todos')}>Todos ({todos.filter(todo => todo.completed).length}/{todos.length})</button>
//     </div>

//   )
// }

interface Props {
  onPageChange: (newPage:string) => void;
  todos: Todo[];
}

class Nav extends React.Component<Props> {
  render() {
    return (
      <div>
        <button onClick={() => this.props.onPageChange('profile')}>Profile</button>
        <button onClick={() => this.props.onPageChange('todos')}>Todos ({this.props.todos.filter(todo => todo.completed).length}/{this.props.todos.length})</button>
      </div>
    );
  }
}

export default connect(/* mapStateToProps */ (rootState: RootState) => {
  return {
    todos: Object.values(rootState.todos.todos)
  }
})(Nav)