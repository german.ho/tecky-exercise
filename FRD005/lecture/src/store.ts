import { createStore, combineReducers } from 'redux'
import { todosReducer, TodoState } from './todos/reducer';
import { userReducer, UserState } from './user/reducer';
import { projectsReducer, Project } from './projects/reducer';

declare global {
  /* tslint:disable:interface-name */
  interface Window {
      __REDUX_DEVTOOLS_EXTENSION__: any
  }
}

export interface RootState {
  todos: TodoState,
  user: UserState,
  projects: Project[]
}

const reducer = combineReducers<RootState>({
  todos: todosReducer,
  user: userReducer,
  projects: projectsReducer
})

export const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());