import { Todo } from "./reducer";

// action creator
export function loadTodos(projectId: string, todos: Todo[]) {
  return {
    type: 'LOADED_TODOS' as 'LOADED_TODOS',
    todos: todos,
    projectId: projectId
  }
}

export function completeTodo(i: number, completed: boolean) {
  return {
    type: 'COMPLETE_TODO' as 'COMPLETE_TODO',
    i: i,
    completed: completed
  }
}

// action types
export type TodosActions = ReturnType<typeof loadTodos> | ReturnType<typeof completeTodo>;