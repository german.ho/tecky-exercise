import { UserActions } from "./action";

export interface UserState {
  name: string | null;
}

const initialState: UserState = {
  name: null
}

export const userReducer = (oldState: UserState = initialState, action: UserActions): UserState => {
  switch (action.type) {
    case '@@USER/SET_NAME':
      return {
        ...oldState,
        name: action.name
      };
  }
  return oldState;
}