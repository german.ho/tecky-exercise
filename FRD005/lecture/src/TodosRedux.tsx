import React, { useEffect, useCallback } from 'react'
import { useFormState } from 'react-use-form-state';
import { useSelector, useDispatch } from 'react-redux';
import { loadTodos, completeTodo } from './todos/actions';
import { RootState } from './store';

function TodosRedux(props: {
  projectId: string | null;
}) {
  const [formState, { text }] = useFormState();
  const todoIds = useSelector((rootState: RootState) => props.projectId ? rootState.todos.todosByProjectId[props.projectId] : null)
  const todos = useSelector((rootState: RootState) => todoIds?.map(id => rootState.todos.todos[id]))
  const dispatch = useDispatch();

  const load = useCallback(async function () {
    if (props.projectId == null) {
      return;
    }

    const res = await fetch('http://localhost:8000/todos?projectId=' + props.projectId);
    const json = await res.json();

    dispatch(loadTodos(props.projectId, json));
  }, [dispatch, props.projectId]);

  async function onTodoClick(i: number, event: React.MouseEvent<HTMLInputElement, MouseEvent>) {
    const res = await fetch('http://localhost:8000/todos/' + i, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        completed: event.currentTarget.checked ? "true" : "false"
      })
    });
    const json = await res.json();

    dispatch(completeTodo(i, json.completed))
  }

  useEffect(() => {
    load();
  }, [load, props.projectId])

  if (props.projectId === '' || props.projectId == null) {
    return (<>請選擇 project</>);
  }

  return (
    <>
      <form onSubmit={async (event) => {
        event.preventDefault();

        await fetch('http://localhost:8000/todos', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            ...formState.values,
            projectId: props.projectId
          })
        })

        await load();
      }}>
        <input {...text('item')} />
        <input type="submit" value="加！" />
      </form>

      <div>
        {todos && todos.map(todo => <div key={todo.id}><input type="checkbox" readOnly checked={todo.completed} 
           onClick={onTodoClick.bind(null, todo.id)} /> {todo.item}</div>)}
      </div>
    </>
  )
}

export default TodosRedux