import React, { useState } from 'react';
import './App.css';
import Profile from './Profile';
import TodosRedux from './TodosRedux';
import Nav from './Nav';
import { useSelector } from 'react-redux';
import { RootState } from './store';

function App() {
  const [page, setPage] = useState('profile')
  const [activeProject, setActiveProject] = useState<string | null>(null)
  const name = useSelector((state: RootState) => state.user.name)
  const projects = useSelector((state: RootState) => state.projects)

  return (
    <div className="App">
      <div>早晨{name}，你好</div>
      <select onChange={event => setActiveProject(event.currentTarget.value)}>
        <option></option>
        {projects.map(project => <option key={project.id} value={project.id}>{project.name}</option>)}
      </select>
      <Nav onPageChange={(newPage) => {
        setPage(newPage)
      }} />

      {page === 'profile' && <Profile />}
      {page === 'todos' && <TodosRedux projectId={activeProject} /> }
    </div>
  );
}

export default App;
