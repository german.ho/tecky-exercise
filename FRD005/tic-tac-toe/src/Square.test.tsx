import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Square from './Square';

test('Square should get the value and show the value', () => {
  const screen = render(<Square value="TEST" player="123" on被人㩒咗={() => {}} />)
  const element = screen.getByText("TEST")

  expect(element).toBeInTheDocument()
})


test('Square should fire the click callback', () => {
  const mockFn = jest.fn();
  const screen = render(<Square value="TEST" player="123" on被人㩒咗={mockFn} />)
  const element = screen.getByText("TEST")

  fireEvent.click(element)

  expect(mockFn).toBeCalledTimes(1)
})
