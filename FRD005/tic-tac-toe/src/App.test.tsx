import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import App from './App';
import { store } from './store';

beforeEach(() => {
  store.dispatch({
    type: 'RESET'
  })
})

test('renders nine square', () => {
  const screen = render(<App />);
  const squareElements = screen.getAllByTestId("square");
  for (const squareElement of squareElements) {
    expect(squareElement).toBeInTheDocument();
  }
  expect(squareElements.length).toBe(9);
});

test('click one square got O', () => {
  const screen = render(<App />);
  const squareElement = screen.getAllByTestId("square")[0];
  fireEvent.click(squareElement);

  const xElement = screen.getByText('O')
  expect(squareElement).toBe(xElement);
})

test('click one square got O then next player should be X', () => {
  const screen = render(<App />);
  const squareElement = screen.getAllByTestId("square")[0];
  fireEvent.click(squareElement);

  const xElement = screen.getByText(/Next Player: X/i)
  expect(xElement).toBeInTheDocument()
})

test('click one square got O then go about us then go back to board', () => {
  const screen = render(<App />);
  const squareElement = screen.getAllByTestId("square")[0];
  fireEvent.click(squareElement);
  const aboutUsElement = screen.getByText(/邊個整/);
  fireEvent.click(aboutUsElement);
  const boardElement = screen.getByText(/Board/);
  fireEvent.click(boardElement);

  const xElement = screen.getByText('O')
  expect(squareElement).toEqual(xElement);
})

test('click one square got O then click another got X', () => {
  const screen = render(<App />);
  const squareElement1 = screen.getAllByTestId("square")[0];
  fireEvent.click(squareElement1);
  const squareElement2 = screen.getAllByTestId("square")[1];
  fireEvent.click(squareElement2);

  const xElement = screen.getByText('X')
  expect(squareElement2).toBe(xElement);
})

test('click one square got O then click again do not X', () => {
  const screen = render(<App />);
  const squareElement1 = screen.getAllByTestId("square")[0];
  fireEvent.click(squareElement1);
  const squareElement2 = screen.getAllByTestId("square")[0];
  fireEvent.click(squareElement2);

  const xElement = screen.getByText('O')
  expect(squareElement2).toBe(xElement);
})

test('click one winner', () => {
  const screen = render(<App />);
  {
    const squareElement = screen.getAllByTestId("square")[0];
    fireEvent.click(squareElement);
  }
  {
    const squareElement = screen.getAllByTestId("square")[1];
    fireEvent.click(squareElement);
  }
  {
    const squareElement = screen.getAllByTestId("square")[3];
    fireEvent.click(squareElement);
  }
  {
    const squareElement = screen.getAllByTestId("square")[5];
    fireEvent.click(squareElement);
  }
  {
    const squareElement = screen.getAllByTestId("square")[6];
    fireEvent.click(squareElement);
  }

  const winElement = screen.getByText(/O 勝出了，完～/)
  expect(winElement).toBeInTheDocument();
})

test('click one winner with css class', () => {
  const screen = render(<App />);
  {
    const squareElement = screen.getAllByTestId("square")[0];
    fireEvent.click(squareElement);
  }
  {
    const squareElement = screen.getAllByTestId("square")[1];
    fireEvent.click(squareElement);
  }
  {
    const squareElement = screen.getAllByTestId("square")[3];
    fireEvent.click(squareElement);
  }
  {
    const squareElement = screen.getAllByTestId("square")[5];
    fireEvent.click(squareElement);
  }
  {
    const squareElement = screen.getAllByTestId("square")[6];
    fireEvent.click(squareElement);
  }

  {
    const wonElement = screen.getAllByTestId("square")[0];
    expect(wonElement).toHaveClass('won');
  }
  {
    const wonElement = screen.getAllByTestId("square")[3];
    expect(wonElement).toHaveClass('won');
  }
  {
    const wonElement = screen.getAllByTestId("square")[6];
    expect(wonElement).toHaveClass('won');
  }
})

test('click 平手', () => {
  const screen = render(<App />);
  const squareElements = screen.getAllByTestId("square");
  fireEvent.click(squareElements[0]);
  fireEvent.click(squareElements[1]);
  fireEvent.click(squareElements[2]);
  fireEvent.click(squareElements[4]);
  fireEvent.click(squareElements[3]);
  fireEvent.click(squareElements[5]);
  fireEvent.click(squareElements[7]);
  fireEvent.click(squareElements[6]);
  fireEvent.click(squareElements[8]);
  
  const drawElement = screen.getByText(/SUPER 打和/)
  expect(drawElement).toBeInTheDocument();
})
