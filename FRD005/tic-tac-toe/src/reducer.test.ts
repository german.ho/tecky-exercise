import { reducer } from "./store"

test('test reducer return initial state', () => {
  const state = reducer(undefined, {
    type: '@@INIT'
  });

  expect(state).toEqual({
    squares: [null, null, null, null, null, null, null, null, null,],
    oIsNext: true
  })
})

test('test reducer make a O on first click', () => {
  const state = reducer({
    squares: [null, null, null, null, null, null, null, null, null,],
    oIsNext: true
  }, {
    type: 'TIC_TAE_TOE_CLICKED',
    square: 2
  });

  expect(state).toEqual({
    squares: [null, null, 'O', null, null, null, null, null, null,],
    oIsNext: false
  })
})